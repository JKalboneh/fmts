﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;

namespace FmtsProj.Controllers
{
    public class TripsController : ControllerBase
    {
        private BMOSContext db = new BMOSContext();

        // GET: Trips
        [SessionExpireFilter]
        public ActionResult Index()
        {
            var trips = db.Trips.Include(t => t.BookSchedule).Include(t => t.Vehicle);
            return View(trips.ToList());
        }

        // GET: Trips/Details/5
        [SessionExpireFilter]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Find(id);
            if (trip == null)
            {
                return HttpNotFound();
            }
            return View(trip);
        }

        // GET: Trips/Create
        [SessionExpireFilter]
        public ActionResult Create()
        {

            ViewBag.BookID = new SelectList(db.Books, "BookID", "BookID");
            ViewBag.VehicleID = new SelectList(db.Vehicles, "VehicleID", "VehicleBrand");
            
            return View(new Trip());
        }

        // POST: Trips/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create([Bind(Include = "TripID,VehicleID,TripType,Cost,StartDate,EndDate,StartTime,EndTime,UpdatedAt,StartLocName,EndLocName,CreatedAt,RepeatEvery,OccursOn,Rec_Type,BookID")] Trip trip)
        {
            if (ModelState.IsValid)
            {
                trip.UpdatedAt = null;
                trip.CreatedAt = DateTime.Now;
               db.Trips.Add(trip);
                db.SaveChanges(CurrentUser.UserID);
                return RedirectToAction("Index");
            }

            ViewBag.BookID = new SelectList(db.Books, "BookID", "BookID", trip.BookID);
            ViewBag.VehicleID = new SelectList(db.Vehicles, "VehicleID", "VehicleBrand", trip.VehicleID);
            return View(trip);
        }

        // GET: Trips/Edit/5
        [SessionExpireFilter]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Find(id);
            if (trip == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookID = new SelectList(db.Books, "BookID", "BookID", trip.BookID);
            ViewBag.VehicleID = new SelectList(db.Vehicles, "VehicleID", "VehicleBrand", trip.VehicleID);
            return View(trip);
        }

        // POST: Trips/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Edit([Bind(Include = "TripID,VehicleID,TripType,Cost,StartDate,EndDate,StartTime,EndTime,UpdatedAt,StartLocName,EndLocName,CreatedAt,RepeatEvery,OccursOn,Rec_Type,BookID")] Trip trip)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trip).State = EntityState.Modified;
                trip.UpdatedAt = System.DateTime.UtcNow;
                db.SaveChanges(CurrentUser.UserID);
                return RedirectToAction("Index");
            }
            ViewBag.BookID = new SelectList(db.Books, "BookID", "BookID", trip.BookID);
            ViewBag.VehicleID = new SelectList(db.Vehicles, "VehicleID", "VehicleBrand", trip.VehicleID);
            return View(trip);
        }

        // GET: Trips/Delete/5
        [SessionExpireFilter]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Find(id);
            if (trip == null)
            {
                return HttpNotFound();
            }
            return View(trip);
        }

        // POST: Trips/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult DeleteConfirmed(int id)
        {
            Trip trip = db.Trips.Find(id);
            db.Trips.Remove(trip);
            db.SaveChanges(CurrentUser.UserID);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using FmtsServices;
using FmtsServices.Model;

namespace FmtsProj.Controllers
{
    public class UnassignedBooksController : Controller
    {
        private BMOSContext db = new BMOSContext();

        // GET: UnassignedBooks
        public ActionResult index(string paraObject)
        {
            
            ViewBag.FilterObject = paraObject;
            return View();
        }
        //public JsonResult FindUnassignedBookGrid(DateTime? paraDateTime, DateTime? paraToTime, int? paraClientId, int? paraDriverId, int? paraVehicaleId)
        //{
        //    TripsService tripsService = new TripsService();
        //    List<BookSchedule> aBookSchedule= tripsService.GetUnassignedBooks(paraDateTime, paraToTime, paraClientId, paraDriverId, paraVehicaleId);
        //    return  Json(aBookSchedule, JsonRequestBehavior.AllowGet); //Json(aBookSchedule);//bookSchedules.ToList()
        //}

        public JsonResult FindUnassignedBookGrid(FindUnassignedBookFilterModel model)
        {
            TripsService aTripsService = new TripsService();
            int count = 0;

            var list = aTripsService.GetUnassignedBooks(model, out count);
            return Json(new { data = list, itemsCount = count }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

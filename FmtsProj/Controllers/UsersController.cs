﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using FmtsServices.Model;
using FmtsServices;

namespace FmtsProj.Controllers
{
    public class UsersController : ControllerBase
    {
        private BMOSContext db = new BMOSContext();

        
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        
        [SessionExpireFilter]
        public ActionResult Create()
        {
            UserModel model = new UserModel();
            ViewBag.RoleList = new SelectList(db.Roles, "RoleID", "RoleName");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create(UserModel model)
        {
            if (ModelState.IsValid)
            {
                UsersService usersService = new UsersService();
                model.ByUserID = CurrentUser.UserID;
                usersService.AddUser(model);
                return RedirectToAction("Index");
            }
            ViewBag.RoleList = new SelectList(db.Roles, "RoleID", "RoleName");

            return View(model);
        }

        [SessionExpireFilter]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsersService usersService = new UsersService();
            ViewBag.RoleList = new SelectList(db.Roles, "RoleID", "RoleName");
            var model = usersService.FindByID(id.GetValueOrDefault());
            
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Edit(UserModel model)
        {
            ViewBag.RoleList = new SelectList(db.Roles, "RoleID", "RoleName");
            if (ModelState.IsValid)
            {
                UsersService usersService = new UsersService();
                model.ByUserID = CurrentUser.UserID;
                usersService.UpdateUser(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [SessionExpireFilter]
        public ActionResult FindUsersGrid(UserFilterModel model)
        {
            UsersService usersService = new UsersService();
            int count = 0;

            var list = usersService.FindUsers(model, out count);
            return Json(new { data = list, itemsCount = count }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult DeleteUser(int userId)
        {
            UsersService usersService = new UsersService();

            usersService.DeleteUser(userId, CurrentUser.UserID);
            return Json("Done", JsonRequestBehavior.AllowGet);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;

namespace FmtsProj.Controllers
{
    public class RouteTripsController : ControllerBase
    {
        private BMOSContext db = new BMOSContext();

        // GET: RouteTrips
        public ActionResult Index()
        {
            var routeTrips = db.RouteTrips.Include(r => r.Route).Include(r => r.Trip);
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "ClientName");

            return View(routeTrips.ToList());
        }

        // GET: RouteTrips/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RouteTrip routeTrip = db.RouteTrips.Find(id);
            if (routeTrip == null)
            {
                return HttpNotFound();
            }
            return View(routeTrip);
        }

        // GET: RouteTrips/Create
        public ActionResult Create()
        {
            ViewBag.RouteID = new SelectList(db.Routes, "RouteID", "RouteID");
            ViewBag.TripID = new SelectList(db.Trips, "TripID", "TripName");
            return View();
        }

        // POST: RouteTrips/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RouteTripID,RouteID,TripID")] RouteTrip routeTrip)
        {
            if (ModelState.IsValid)
            {
                db.RouteTrips.Add(routeTrip);
                db.SaveChanges(CurrentUser.UserID);
                return RedirectToAction("Index");
            }

            ViewBag.RouteID = new SelectList(db.Routes, "RouteID", "RouteID", routeTrip.RouteID);
            ViewBag.TripID = new SelectList(db.Trips, "TripID", "TripName", routeTrip.TripID);
            return View(routeTrip);
        }

        // GET: RouteTrips/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RouteTrip routeTrip = db.RouteTrips.Find(id);
            if (routeTrip == null)
            {
                return HttpNotFound();
            }
            ViewBag.RouteID = new SelectList(db.Routes, "RouteID", "RouteID", routeTrip.RouteID);
            ViewBag.TripID = new SelectList(db.Trips, "TripID", "TripName", routeTrip.TripID);
            return View(routeTrip);
        }

        // POST: RouteTrips/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RouteTripID,RouteID,TripID")] RouteTrip routeTrip)
        {
            if (ModelState.IsValid)
            {
                db.Entry(routeTrip).State = EntityState.Modified;
                db.SaveChanges(CurrentUser.UserID);
                return RedirectToAction("Index");
            }
            ViewBag.RouteID = new SelectList(db.Routes, "RouteID", "RouteID", routeTrip.RouteID);
            ViewBag.TripID = new SelectList(db.Trips, "TripID", "TripName", routeTrip.TripID);
            return View(routeTrip);
        }

        // GET: RouteTrips/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RouteTrip routeTrip = db.RouteTrips.Find(id);
            if (routeTrip == null)
            {
                return HttpNotFound();
            }
            return View(routeTrip);
        }

        // POST: RouteTrips/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RouteTrip routeTrip = db.RouteTrips.Find(id);
            db.RouteTrips.Remove(routeTrip);
            db.SaveChanges(CurrentUser.UserID);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

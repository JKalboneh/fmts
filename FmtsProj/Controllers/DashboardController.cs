﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using FmtsEntity;
using System.Globalization;
using FmtsServices;
using FmtsServices.Model;

namespace FmtsProj.Controllers
{
    public class DashboardController : ControllerBase
    {
        private BMOSContext db = new BMOSContext();

        [SessionExpireFilter]
        public ActionResult Index()
        {
            DateTime currentDatime = DateTime.Now;
            ViewBag.FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0).ToString("dd-MM-yyyyy HH:mm");
            ViewBag.ToDate = DateTime.Today.AddDays(1).AddSeconds(-1).ToString("dd-MM-yyyyy HH:mm");
            return View();

        }
        public JsonResult GetNotifications()
        {
            try
            {
                NotificationsService aNotificationsService = new NotificationsService();
                return Json(aNotificationsService.GetNotifications(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        public JsonResult FindNotifications(NotificationsFilterModel model)
        {
            try
            {
                
                NotificationsService aNotificationsService = new NotificationsService();
                NotificationsModel aNotificationsModel = aNotificationsService.FindNotifications(model);
                return Json(new { data = aNotificationsModel.items, itemsCount = aNotificationsModel.counter}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        public ActionResult Notifications(string inputfind)
        {
            try
            {

                ViewBag.Filter = inputfind;
                return View();
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult GetDriverDashboardInfo(string fromDatetime, string toDatetime, int? driverId, int? clientId)
        {
            try
            {
                DateTime aFromDatetime = DateTime.ParseExact(fromDatetime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture); 
                DateTime aToDatetime = DateTime.ParseExact(toDatetime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                DriversService driversService = new DriversService();
                var model = driversService.GetDriverDashboard(driverId, clientId, aFromDatetime, aToDatetime);

                return Json(model,JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult GetVehicleDashboardInfo(string fromDatetime, string toDatetime, int? driverId, int? clientId, int? vehicleId)
        {
            try
            {
                DateTime aFromDatetime = DateTime.ParseExact(fromDatetime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                DateTime aToDatetime = DateTime.ParseExact(toDatetime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                VehiclesService vehiclesService = new VehiclesService();
                var model = vehiclesService.GetVehicleDashboard(driverId, clientId, vehicleId, aFromDatetime, aToDatetime);

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult GetBookDashboardInfo(string fromDatetime, string toDatetime, int? driverId, int? clientId, int? vehicleId)
        {
            try
            {
                DateTime aFromDatetime = DateTime.ParseExact(fromDatetime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                DateTime aToDatetime = DateTime.ParseExact(toDatetime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                BooksService bookService = new BooksService();
                var list = bookService.GetBookDashBoardModel(clientId,aFromDatetime, aToDatetime, driverId, vehicleId);

                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult GetUnassignedTripsDashboardInfo(FindUnassignedBookFilterModel model)
        {
            try
            {
                TripsService tripsService = new TripsService();
                var count = tripsService.GetUnassignedTripsCount(model);

                return Json(count, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult TripDetials(int? Bookid)
        {
            if (Bookid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Trip> trip = db.Trips.Where(tr => tr.BookID.Equals(Bookid.Value)).ToList();
            if (trip == null)
            {
                return HttpNotFound();
            }
            return View("TripDetials", trip);

        }

        //auto AutoComplete
        public JsonResult AutoComplete(string filterstr, string mode)
        {

            if (mode == "inputVehicleID")
            {
                var client = db.Vehicles.ToList().Where(v => v.VehicleNum.StartsWith(filterstr, true, CultureInfo.CurrentCulture)).Select(a => new { label = a.VehicleNum, val = a.ID });
                return Json(client, JsonRequestBehavior.AllowGet);

            }
            if (mode == "inputDriversNum")
            {
                var client = db.Drivers.ToList().Where(d => d.FullName.StartsWith(filterstr, true, CultureInfo.CurrentCulture)).Select(a => new { label = a.FullName, val = a.DriverID });
                return Json(client, JsonRequestBehavior.AllowGet);

            }
            if (mode == "inputClient")
            {
                var client = db.Clients.ToList().Where(c => c.ClientName.StartsWith(filterstr, true, CultureInfo.CurrentCulture)).Select(a => new { label = a.ClientName, val = a.ClientID });
                return Json(client.ToList(), JsonRequestBehavior.AllowGet);

            }
            return Json(true);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

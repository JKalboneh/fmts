﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;

namespace FmtsProj.Controllers
{
    public class DriverSchedulesController : ControllerBase
    {
        private BMOSContext db = new BMOSContext();

        // GET: DriverSchedules
        public ActionResult Index()
        {
            var driverSchedules = db.DriverSchedules.Include(d => d.Driver);
            return View(driverSchedules.ToList());
        }
        // GET: DriverSchedules/Details/5
        public ActionResult WorkingHourDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DriverSchedule driverSchedule = db.DriverSchedules.Where(ds=>ds.DriverID== id.Value).Single();
            if (driverSchedule == null)
            {
                return HttpNotFound();
            }
            return View(driverSchedule);
        }

        // GET: DriverSchedules/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DriverSchedule driverSchedule = db.DriverSchedules.Find(id);
            if (driverSchedule == null)
            {
                return HttpNotFound();
            }
            return View(driverSchedule);
        }

        // GET: DriverSchedules/Create
        public ActionResult Create()
        {
            ViewBag.DriverID = new SelectList(db.Drivers, "DriverID", "FullName");
            return View();
        }

        // POST: DriverSchedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create([Bind(Include = "ID,DriverID,WorkingDays,WorkingHourFrom,WorkingHourTo")] DriverSchedule driverSchedule)
        {
            if (ModelState.IsValid)
            {
                db.DriverSchedules.Add(driverSchedule);
                db.SaveChanges(CurrentUser.UserID);
                return RedirectToAction("Index");
            }

            ViewBag.DriverID = new SelectList(db.Drivers, "DriverID", "FullName", driverSchedule.DriverID);
            return View(driverSchedule);
        }

        // GET: DriverSchedules/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DriverSchedule driverSchedule = db.DriverSchedules.Find(id);
            if (driverSchedule == null)
            {
                return HttpNotFound();
            }
            ViewBag.DriverID = new SelectList(db.Drivers, "DriverID", "FullName", driverSchedule.DriverID);
            return View(driverSchedule);
        }

        // POST: DriverSchedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Edit([Bind(Include = "ID,DriverID,WorkingDays,WorkingHourFrom,WorkingHourTo")] DriverSchedule driverSchedule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(driverSchedule).State = EntityState.Modified;
                db.SaveChanges(CurrentUser.UserID);
                return RedirectToAction("Index");
            }
            ViewBag.DriverID = new SelectList(db.Drivers, "DriverID", "FullName", driverSchedule.DriverID);
            return View(driverSchedule);
        }

        // GET: DriverSchedules/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DriverSchedule driverSchedule = db.DriverSchedules.Find(id);
            if (driverSchedule == null)
            {
                return HttpNotFound();
            }
            return View(driverSchedule);
        }

        // POST: DriverSchedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DriverSchedule driverSchedule = db.DriverSchedules.Find(id);
            db.DriverSchedules.Remove(driverSchedule);
            db.SaveChanges(CurrentUser.UserID);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

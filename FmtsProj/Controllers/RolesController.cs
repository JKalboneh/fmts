﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using FmtsServices;
using FmtsServices.Model;

namespace FmtsProj.Controllers
{
    public class RolesController : ControllerBase
    {
        // GET: Roles
        public ActionResult Index()
        {
            return View();
        }


        // GET: Roles/Create
        public ActionResult Create()
        {
            RoleModel model = new RoleModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create(RoleModel role)
        {
            if (ModelState.IsValid)
            {
                RolesService rolesService = new RolesService();
                role.ByUserID = CurrentUser.UserID;
                rolesService.SaveRole(role);
                return RedirectToAction("Index");
            }
            return View(role);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RolesService rolesService = new RolesService();
            var model = rolesService.FindByID(id.GetValueOrDefault());
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Edit(RoleModel model)
        {
            if (ModelState.IsValid)
            {
                RolesService rolesService = new RolesService();
                model.ByUserID = CurrentUser.UserID;
                rolesService.UpdateRole(model);

                return RedirectToAction("Index");
            }
            return View(model);
        }


        public ActionResult FindRolesGrid(RoleFilterModel model)
        {
            RolesService rolesService = new RolesService();
            int count = 0;

            var list = rolesService.FindRoles(model, out count);
            return Json(new { data = list, itemsCount = count }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateRole(RoleModel roleModel)
        {
            RolesService roleService = new RolesService();
            roleModel.ByUserID = CurrentUser.UserID;
            roleService.UpdateRole(roleModel);

            var driver = roleService.FindByID(roleModel.RoleID);
            return Json(new { driver = driver }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult DeleteRole(int roleId)
        {
            RolesService roleService = new RolesService();
            roleService.DeleteRole(roleId, CurrentUser.UserID);
            return Json("Done", JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult GetRolePermissions(int roleId)
        {
            PermissionService permissionService = new PermissionService();
            var list = permissionService.GetByRoleID(roleId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult SaveRolePermissions(int roleId, string permissionIds)
        {
            PermissionService permissionService = new PermissionService();
            permissionService.UpdateRolePermissions(roleId, permissionIds, CurrentUser.UserID);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        

    }
}

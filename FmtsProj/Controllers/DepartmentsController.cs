﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using FmtsServices;
using FmtsServices.Model;

namespace FmtsProj.Controllers
{
    public class DepartmentsController : ControllerBase
    {
        private BMOSContext db = new BMOSContext();
        [SessionExpireFilter]
        public ActionResult Index()
        {
            ViewBag.CustomerName  = new SelectList(db.Customers, "CustomerID", "Name");
            

            return View();
        }

        [SessionExpireFilter]
        public ActionResult Create()
        {
            ViewBag.CustomerName = new SelectList(db.Customers, "CustomerID", "Name");
            DepartmentModel model = new DepartmentModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult CreatePopup(DepartmentModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DepartmentsService departmentsService = new DepartmentsService();
                    model.ByUserID = CurrentUser.UserID;
                    departmentsService.AddDepartment(model);
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create(DepartmentModel model)
        {
            if (ModelState.IsValid)
            {
                ViewBag.CustomerName = new SelectList(db.Customers, "CustomerID", "Name");
                DepartmentsService departmentsService = new DepartmentsService();
                model.ByUserID = CurrentUser.UserID;
                departmentsService.AddDepartment(model);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [SessionExpireFilter]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.CustomerName = new SelectList(db.Customers, "CustomerID", "Name");
            DepartmentsService DepartmentsService = new DepartmentsService();
            var model = DepartmentsService.FindByID(id.GetValueOrDefault());
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Edit(DepartmentModel model)
        {
            if (ModelState.IsValid)
            {
                ViewBag.CustomerName = new SelectList(db.Customers, "CustomerID", "Name");
                DepartmentsService DepartmentsService = new DepartmentsService();
                model.ByUserID = CurrentUser.UserID;
                DepartmentsService.UpdateDepartment(model);

                return RedirectToAction("Index");
            }
            return View(model);
        }
        [SessionExpireFilter]
        public ActionResult FindDepartmentsGrid(DepartmentFilterModel model)
        {
            DepartmentsService DepartmentsService = new DepartmentsService();
            int count = 0;

            var list = DepartmentsService.FindDepartments(model, out count);
            return Json(new { data = list, itemsCount = count }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult FindDepartment(string q, int? page = 1, int? pageSize = 15,int ?parCustomerid=0)
        {
            DepartmentsService departmentsService = new DepartmentsService();
            int count = 0;
            var list = departmentsService.FindDepartments(q, out count, page.Value, pageSize.Value,parCustomerid.Value);
            var selectList = list.Select(a => new { id = a.DepartmentID, text = a.Name }).ToList();
            return Json(new { items = selectList, count = count }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult DeleteDepartment(int DepartmentId)
        {
            DepartmentsService DepartmentsService = new DepartmentsService();

            DepartmentsService.DeleteDepartment(DepartmentId, CurrentUser.UserID);
            return Json("Done", JsonRequestBehavior.AllowGet);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FmtsServices.Model;
using FmtsServices;
using System.Web.Security;
using System.Threading;

namespace FmtsProj.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            ClearSession();

            LoginModel model = new LoginModel();
            model.RememberMe = true;
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Login(LoginModel model, string ReturnUrl = "")
        {
            HttpContext ctx = System.Web.HttpContext.Current;
            ctx.Session["User"] = null;
            TempData["LoginMessage"] = "";
            var usersService = new UsersService();
            var user = usersService.LoginUser(model);
            if (user != null)
            {
                ctx.Session["User"] = user;
                return RedirectToAction("Index", "Dashboard");
            }
            ModelState.Remove("Password");
            TempData["LoginMessage"] = "The login faild";
            return RedirectToAction("Index", "Login");
        }

        public ActionResult Logout()
        {
            ClearSession();

            return RedirectToAction("Index", "Login");
        }

        private void ClearSession()
        {
            HttpContext ctx = System.Web.HttpContext.Current;
            ctx.Session["User"] = null; //it's my session variable
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            FormsAuthentication.SignOut();

            this.Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            this.Response.Cache.SetNoStore();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using FmtsServices;
using FmtsServices.Model;

namespace FmtsProj.Controllers
{
    public class CustomersController : ControllerBase
    {

        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult Create()
        {
           CustomerModel model = new CustomerModel();
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create(CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                CustomersService CustomersService = new CustomersService();
                model.ByUserID = CurrentUser.UserID;
                CustomersService.AddCustomer(model);
                return RedirectToAction("Index");
            }

            return View(model);
        }
        [HttpPost]
        public JsonResult CreatePopup(CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                CustomersService CustomersService = new CustomersService();
                model.ByUserID = CurrentUser.UserID;
                CustomersService.AddCustomer(model);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomersService CustomersService = new CustomersService();
            var model = CustomersService.FindByID(id.GetValueOrDefault());
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Edit(CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                CustomersService CustomersService = new CustomersService();
                model.ByUserID = CurrentUser.UserID;
                CustomersService.UpdateCustomer(model);

                return RedirectToAction("Index");
            }
            return View(model);
        }

        [SessionExpireFilter]
        public ActionResult FindCustomers(string q, int? page = 1, int? pageSize = 15, int? parClientid=0)
        {
            CustomersService CustomersServiceService = new CustomersService();
            int count = 0;
            var list = CustomersServiceService.FindCustomers(q, out count, page.Value, pageSize.Value, parClientid.Value);
            var selectList = list.Select(a => new { id = a.CustomerID, text = a.Name }).ToList();
            return Json(new { items = selectList, count = count }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult FindCustomersGrid(CustomerFilterModel model)
        {
            CustomersService CustomersService = new CustomersService();
            int count = 0;

            var list = CustomersService.FindCustomers(model, out count);
            return Json(new { data = list, itemsCount = count }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult DeleteCustomer(int customerId)
        {
            CustomersService CustomersService = new CustomersService();

            CustomersService.DeleteCustomer(customerId,CurrentUser.UserID);
            return Json("Done", JsonRequestBehavior.AllowGet);
        }
    }
}

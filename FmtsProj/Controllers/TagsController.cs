﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using FmtsServices;
using FmtsServices.Model;

namespace FmtsProj.Controllers
{
    public class TagsController : ControllerBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create(TagModel model)
        {
            if (ModelState.IsValid)
            {
                TagsService tagsService = new TagsService();
                model.ByUserID = CurrentUser.UserID;
                tagsService.AddTag(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [SessionExpireFilter]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TagsService tagsService = new TagsService();
            var model = tagsService.FindByID(id.GetValueOrDefault());
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TagModel model)
        {
            if (ModelState.IsValid)
            {
                TagsService tagsService = new TagsService();
                model.ByUserID = CurrentUser.UserID;
                tagsService.UpdateTag(model);

                return RedirectToAction("Index");
            }
            return View(model);
        }

        [SessionExpireFilter]
        public ActionResult DeleteTag(int tagId)
        {
            TagsService tagsService = new TagsService();
            tagsService.DeleteTag(tagId, CurrentUser.UserID);
            return Json("Done", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FindTags(TagFilterModel model)
        {
            TagsService tagssService = new TagsService();
            int count = 0;
            var list = tagssService.FindTags(model, out count);
            return Json(new { items = list, count = count }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FindTagsList(string q, int? page = 1, int? pageSize = 15, int? parAddFlage = 0)
        {
            TagsService driversService = new TagsService();
            int count = 0;
            var list = driversService.FindTags(q, out count, page.Value, pageSize.Value);

            var selectList = list.Select(a => new { id = a.TagID, text = a.Name}).ToList();
            return Json(new { items = selectList, count = count }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult FindTagsGrid(TagFilterModel model)
        {
            TagsService tagsService = new TagsService();
            int count = 0;

            var list = tagsService.FindTags(model, out count);
            return Json(new { data = list, itemsCount = count }, JsonRequestBehavior.AllowGet);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using FmtsServices;
using FmtsServices.Model;

namespace FmtsProj.Controllers
{
    public class LocationsController : ControllerBase
    {

        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult Create()
        {
            LocationModel model = new LocationModel();
            return View(model);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create(LocationModel model)
        {
            if (ModelState.IsValid)
            {
                LocationsService locationsService = new LocationsService();
                model.ByUserID = CurrentUser.UserID;
                locationsService.AddLocation(model);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [SessionExpireFilter]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LocationsService locationsService = new LocationsService();
            var model = locationsService.FindByID(id.GetValueOrDefault());
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Edit(LocationModel model)
        {
            if (ModelState.IsValid)
            {
                LocationsService locationsService = new LocationsService();
                model.ByUserID = CurrentUser.UserID;
                locationsService.UpdateLocation(model);

                return RedirectToAction("Index");
            }
            return View(model);
        }

        [SessionExpireFilter]
        public ActionResult FindLocations(string q, int? page = 1, int? pageSize = 15)
        {
            LocationsService locationsServiceService = new LocationsService();
            int count = 0;
            var list = locationsServiceService.FindLocations(q, out count, page.Value, pageSize.Value);
            var selectList = list.Select(a => new { id = a.LocationID, text = a.Name }).ToList();
            return Json(new { items = selectList, count = count }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult FindLocationsGrid(LocationFilterModel model)
        {
            LocationsService locationsService = new LocationsService();
            int count = 0;

            var list = locationsService.FindLocations(model, out count);
            return Json(new { data = list, itemsCount = count }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult DeleteLocation(int locationId)
        {
            LocationsService locationsService = new LocationsService();
            locationsService.DeleteLocation(locationId, CurrentUser.UserID);
            return Json("Done", JsonRequestBehavior.AllowGet);
        }

    }
}

﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FmtsProj.Controllers
{
    public class ControllerBase : Controller
    {
        public UserModel CurrentUser = null;
        public ControllerBase()
        {
            HttpContext ctx = System.Web.HttpContext.Current;
            if (ctx.Session["User"] != null)
            {
                CurrentUser = (UserModel)ctx.Session["User"];
            }
            else
            {
                ctx.Session.Abandon();
                ctx.Session.Clear();
                ctx.Response.Redirect("~/Login/Logout");
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using FmtsServices.Model;
using FmtsServices;

namespace FmtsProj.Controllers
{
    public class PermissionsController : ControllerBase
    {
        private BMOSContext db = new BMOSContext();

        
        [SessionExpireFilter]
        public ActionResult AllPermissions()
        {
            PermissionService permissionService = new PermissionService();
            var list = permissionService.GetGroupsModelList();
            return PartialView("_Permissions", list);
        }
        
    }
}

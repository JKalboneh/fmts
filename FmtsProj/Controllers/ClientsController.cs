﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using FmtsServices;
using FmtsServices.Model;

namespace FmtsProj.Controllers
{
    public class ClientsController : ControllerBase
    {
        private BMOSContext db = new BMOSContext();

        // GET: Clients
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult Create()
        {
            ClientModel model = new ClientModel();
            return View(model);
        }

        [SessionExpireFilter]
        public ActionResult PartialCreate()
        {
            return PartialView("Partial");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create(ClientModel model)
        {
            if (ModelState.IsValid)
            {
                ClientsService clientsService = new ClientsService();
                model.ByUserID = CurrentUser.UserID;
                clientsService.AddClient(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }


        [HttpPost]
        public JsonResult AddNewClient(ClientModel model)
        {
            if (ModelState.IsValid)
            {
                ClientsService clientsService = new ClientsService();
                model.ByUserID = CurrentUser.UserID;
                clientsService.AddClient(model);
                return Json(model, JsonRequestBehavior.AllowGet);// Json(client);
            }
            return Json("Error");
        }

        [SessionExpireFilter]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClientsService clientsService = new ClientsService();
            var model = clientsService.FindByID(id.GetValueOrDefault());
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Edit(ClientModel model)
        {
            if (ModelState.IsValid)
            {
                ClientsService clientsService = new ClientsService();
                model.ByUserID = CurrentUser.UserID;
                clientsService.UpdateClient(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }
        [SessionExpireFilter]
        public ActionResult DeleteClient(int clientId)
        {
            ClientsService clientsService = new ClientsService();
            clientsService.DeleteClient(clientId, CurrentUser.UserID);
            return Json("Done", JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult FindClientsGrid(ClientsFilterModel model)
        {
           ClientsService clientsService = new ClientsService();
            int count = 0;
            var list = clientsService.FindClinets(model, out count);
            return Json(new { data = list, itemsCount = count }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult FindClients(string q, int? page = 1, int? pageSize = 15,int? parAddFlage=0)
        {
            ClientsService vehiclesService = new ClientsService();
            int count = 0;
            var list = vehiclesService.FindClients(q, out count, page.Value, pageSize.Value);
            var selectList = list.Select(a => new { id = a.ClientID, text = a.ClientName }).ToList();
            return Json(new { items = selectList, count = count }, JsonRequestBehavior.AllowGet);
        }

    }
}

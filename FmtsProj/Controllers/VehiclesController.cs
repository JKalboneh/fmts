﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using FmtsServices;
using FmtsServices.Model;

namespace FmtsProj.Controllers
{
    public class VehiclesController : ControllerBase
    {
        private BMOSContext db = new BMOSContext();

        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult Create()
        {
            ViewBag.OwnerID = new SelectList(db.Owners, "ID", "OwnerName");

            ViewBag.TypeList  = new List<SelectListItem>();
            ViewBag.TypeList.Add(new SelectListItem { Value = "1", Text = "InSource" });
            ViewBag.TypeList.Add(new SelectListItem { Value = "2", Text = "Outsource" });

            VehiclesModel model = new VehiclesModel();
            return View(model);
        }
        [HttpPost]
        public JsonResult AddNewVehicale(VehiclesModel model)
        {
            JsonResult result = new JsonResult();
                try
                {
                    if (ModelState.IsValid)
                    {

                        VehiclesService vehiclesService = new VehiclesService();
                        model.CreatedBy = CurrentUser.UserID;
                        vehiclesService.AddVehicle(model);
                        result = Json(model, JsonRequestBehavior.AllowGet);// Json(client);
                    }
                }
                catch(Exception ex)
                {
                    result = Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
            return result;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create(VehiclesModel model)
        {
            if (ModelState.IsValid)
            {
                ViewBag.OwnerID = new SelectList(db.Owners, "ID", "OwnerName");

                ViewBag.TypeList = new List<SelectListItem>();
                ViewBag.TypeList.Add(new SelectListItem { Value = "1", Text = "InSource" });
                ViewBag.TypeList.Add(new SelectListItem { Value = "2", Text = "Outsource" });

                VehiclesService vehiclesService = new VehiclesService();
                model.CreatedBy = CurrentUser.UserID;
                vehiclesService.AddVehicle(model);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [SessionExpireFilter]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.OwnerID = new SelectList(db.Owners, "ID", "OwnerName");
            ViewBag.OwnerID = new SelectList(db.Owners, "ID", "OwnerName");

            ViewBag.TypeList = new List<SelectListItem>();
            ViewBag.TypeList.Add(new SelectListItem { Value = "1", Text = "InSource" });
            ViewBag.TypeList.Add(new SelectListItem { Value = "2", Text = "Outsource" });

            VehiclesService vehiclesService = new VehiclesService();
            var model = vehiclesService.FindByID(id.GetValueOrDefault());
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Edit(VehiclesModel model)
        {
            ViewBag.OwnerID = new SelectList(db.Owners, "ID", "OwnerName");

            ViewBag.TypeList = new List<SelectListItem>();
            ViewBag.TypeList.Add(new SelectListItem { Value = "1", Text = "InSource" });
            ViewBag.TypeList.Add(new SelectListItem { Value = "2", Text = "Outsource" });

            if (ModelState.IsValid)
            {
                VehiclesService vehiclesService = new VehiclesService();
                model.ByUserID = CurrentUser.UserID;
                vehiclesService.UpdateVehicle(model);

                return RedirectToAction("Index");
            }
            return View(model);
        }

        [SessionExpireFilter]
        public ActionResult DeleteVehicle(int vehicleId)
        {
            VehiclesService vehiclesService = new VehiclesService();

            vehiclesService.DeleteVehicle(vehicleId, CurrentUser.UserID);
            return Json("Done", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FindVehicles(string q, int? page = 1, int? pageSize = 15,string parTagsList="",int parDriverID=0, string parStatDate="",string parEndDate="", int parCurrentStopDBId=0)
        {
            VehiclesService vehiclesService = new VehiclesService();
            int count = 0;
            var list = vehiclesService.FindVehicles(q, out count, page.Value, pageSize.Value, parTagsList, parDriverID , parStatDate , parEndDate , parCurrentStopDBId);
            var selectList = list.Select(a => new {
                id = a.ID,
                text = a.VehicleNum,
                SeatCount = a.SeatCount,
                Make = a.Make,
                BusyInSameTrip = a.BusyInSameTrip,
                DriverTagsMissmatch= a.DriverTagsMissmatch,
                VehicalOutTime= a.VehicalOutTime}).ToList();
            //if (parAddFlage != 0)
            //    selectList.Insert(0, new { id = 0, text = "All" });
            return Json(new { items = selectList, count = count }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FindVehiclesGrid(VehicleFilterModel model)
        {
            VehiclesService vehiclesService = new VehiclesService();
            int count = 0;

            var list = vehiclesService.FindVehicles(model, out count);
            return Json(new { data = list, itemsCount = count }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateVehicleDrivers(string drivers, int vehicleId)
        {
            VehicleAssignDriversService vehicleAssignDriversService = new VehicleAssignDriversService();
            vehicleAssignDriversService.UpdateByDriverIds(drivers, vehicleId, CurrentUser.UserID);
            VehiclesService vehiclesService = new VehiclesService();
            var vehicle = vehiclesService.FindByID(vehicleId);
            return Json(new { vehicle = vehicle }, JsonRequestBehavior.AllowGet);
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using System.Security.Cryptography;
using System.Web.Script.Serialization;
using FmtsServices.Model;
using System.Web.Routing;
using System.Globalization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Text;
using iTextSharp.text.html.simpleparser;
using System.Web.UI;
using System.Collections;
using FmtsServices;

namespace FmtsProj.Controllers
{
    public class BooksController : ControllerBase
    {
        private BMOSContext db = new BMOSContext();

        // GET: BookSchedules
        [SessionExpireFilter]
        public ActionResult Index()
        {
            var books = db.BookSchedules.Include(b => b.Client).Include(b => b.User);
            return View(books.ToList());
        }

        // GET: BookSchedules/Details/5
        [SessionExpireFilter]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookSchedule book = db.BookSchedules.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }
        public string Get8Digits()
        {
            var bytes = new byte[4];
            var rng = RandomNumberGenerator.Create();
            rng.GetBytes(bytes);
            uint random = BitConverter.ToUInt32(bytes, 0) % 100000000;
            return String.Format("{0:D8}", random);
        }

        [SessionExpireFilter]
        public ActionResult AddBook(BookModel model)
        {
            try
            {
                Book aBook = new Book();
                aBook.BookRef = Get8Digits();
                BookSchedule aBookSchedule = new BookSchedule();
                aBookSchedule.ClientID = model.ClientID.id.Value;
                //book.Client = db.Clients.FirstOrDefault(a => a.ClientID == model.ClientID);
                aBookSchedule.Refnum = Get8Digits();
                aBookSchedule.CreatedBy = CurrentUser.UserID;
                aBookSchedule.StartDatetime = DateTime.ParseExact(model.StartDatetime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                aBookSchedule.StartLoc = model.StartLoc.id;
                aBookSchedule.Status = "Active";
                if (model.DepartmentID != null && model.DepartmentID.id > 0)
                    aBookSchedule.DepartmentID = model.DepartmentID.id;

                if (model.CustomerID != null && model.CustomerID.id > 0)
                    aBookSchedule.CustomerID = model.CustomerID.id;
                aBookSchedule.ContactType = model.ContactType.id.ToString();
                aBookSchedule.RecType = model.RecType == 1 ? true : false;
                aBookSchedule.TillDate = DateTime.ParseExact(model.inputTillDay, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                if (model.SchedDays != null && model.SchedDays.GetLength(0) > 0)
                {
                    foreach (var item in model.SchedDays)
                    {
                        if (item == "Sat")
                            aBookSchedule.Sat = true;
                        if (item == "Sun")
                            aBookSchedule.Sun = true;
                        if (item == "Mon")
                            aBookSchedule.Mon = true;
                        if (item == "Tue")
                            aBookSchedule.Tue = true;
                        if (item == "Wed")
                            aBookSchedule.Wed = true;
                        if (item == "Thu")
                            aBookSchedule.Thu = true;
                        if (item == "Fri")
                            aBookSchedule.Fri = true;
                    }
                }
                aBookSchedule.TotalCost = model.TotalCost;
                if (model.TripList != null)
                {
                    foreach (var objTrip in model.TripList)
                    {
                        Trip trip = new Trip();
                        trip.BookID = aBookSchedule.ID;
                        if (objTrip.OverWriteLocID != null && objTrip.OverWriteLocID.id > 0)
                            trip.OverWriteLocID = objTrip.OverWriteLocID.id;
                        trip.EndLocid = objTrip.EndLocid.id;
                        trip.StartDate = DateTime.ParseExact(objTrip.StartDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                        trip.EndDate = DateTime.ParseExact(objTrip.EndDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                        trip.SalikFees = objTrip.SalikFees;
                        trip.DriverFees = objTrip.DriverFees;
                        trip.CarFuelCost = objTrip.CarFuelCost;
                        trip.ParkingFees = objTrip.ParkingFees;
                        trip.Consumption = objTrip.Consumption;
                        trip.DriverTrip = Convert.ToByte(objTrip.DriverTrip);
                        trip.Rec_Type = objTrip.Rec_Type;
                        trip.RepeatEvery = objTrip.RepeatEvery;
                        trip.Other = objTrip.Other;
                        trip.CreatedAt = DateTime.Now;
                        if (objTrip.VehicleID != null && objTrip.VehicleID.id > 0)
                            trip.VehicleID = objTrip.VehicleID.id;

                        aBookSchedule.Trips.Add(trip);
                        if (objTrip.DriverID != null && objTrip.DriverID.id > 0)
                        {
                            DriverTrip driverTrip = new DriverTrip();
                            driverTrip.TripID = trip.TripID;
                            driverTrip.DriverID = objTrip.DriverID.id.Value;
                            driverTrip.CreatedBy = CurrentUser.UserID;
                            trip.DriverTrips.Add(driverTrip);
                        }
                    }
                }
                aBook.BookSchedules.Add(aBookSchedule);
                db.Books.Add(aBook);
                db.SaveChanges(CurrentUser.UserID);
                BookResult aBookResult = new BookResult();
                aBookResult.BookRefNum = aBook.BookRef;// get the ref from sch which it is the parent
                aBookResult.IDBook = aBook.ID;

                aBookResult.SchRefnum = aBookSchedule.Refnum;// get the ref from book which it is the edit ref num
                aBookResult.IDBookSch = aBookSchedule.ID;
                return Json(aBookResult); //RedirectToAction("Index","Clients");
                                          //           return RedirectToAction("Edit", new RouteValueDictionary(
                                          //new { controller = "Books", action = "Edit", parBookScheduleId = aBookSchedule.ID }));

            }
            catch (Exception ex)
            {
                return Json("An Error Has occoured");
            }

        }
        public ActionResult Create(int? bookid)
        {
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "ClientName");
            ViewBag.CreatedBy = new SelectList(db.Users, "ID", "UserName");
            ViewBag.Customers = new SelectList(db.Customers, "ID", "LocName");
            ViewBag.Drivers = new SelectList(db.Drivers, "DriverID", "FullName");
            ViewBag.Vehicles = new SelectList(db.Vehicles, "ID", "VehicleNum");
            BookSchedule aBookSchedule = new BookSchedule();

            aBookSchedule.Trips = new List<Trip>();
            Trip trip = new Trip();
            trip.DriverTrips = new List<DriverTrip>();
            trip.DriverTrips.Add(new DriverTrip());
            aBookSchedule.Trips.Add(trip);
            if (!bookid.HasValue)
                return View("Create", aBookSchedule);
            else
                return RedirectToAction("Edit", new RouteValueDictionary(
                    new { controller = "Books", action = "Edit", parBookId = bookid }));
        }
        private BookModel GetBookInfoaAsObjectModel(int? parBookScheduleID)
        {
            try
            {
                if (!parBookScheduleID.HasValue)
                    return null;
                BookSchedule aBookSchedules = db.BookSchedules.Single(bk => bk.ID == parBookScheduleID.Value);
                BookModel aBookModel = new BookModel();

                aBookModel.bookHistory = db.BookSchedules.Where(bk => bk.BookID == aBookSchedules.BookID && bk.ID != aBookSchedules.ID).Select(b => new BookSchedulesHisModel
                {
                    BookHistoryID = b.ID,
                    BookRefNumID = b.Refnum,
                    Status = b.Status.Trim()
                }).ToList();

                aBookModel.BookID = aBookSchedules.BookID.Value;
                aBookModel.BookRef = aBookSchedules.Book.BookRef;
                aBookModel.ScheduleID = aBookSchedules.ID;
                aBookModel.SchedulesRefNum = aBookSchedules.Refnum;
                if (aBookSchedules.CustomerID.HasValue)
                {
                    aBookModel.CustomerID.id = aBookSchedules.CustomerID.Value;
                    aBookModel.CustomerID.text = aBookSchedules.Customer.Name;
                }
                if (aBookSchedules.DepartmentID.HasValue)
                {
                    aBookModel.DepartmentID.id = aBookSchedules.DepartmentID.Value;
                    aBookModel.DepartmentID.text = aBookSchedules.Department.Name;
                }
                aBookModel.ContactType.id = int.Parse(aBookSchedules.ContactType);
                aBookModel.ContactType.text = aBookSchedules.ContactType == "1" ? "Long Term" : "Short Term";
                aBookModel.ClientID.id = aBookSchedules.ClientID;
                aBookModel.ClientID.text = aBookSchedules.Client.ClientName;
                aBookModel.Status = aBookSchedules.Status.Trim();
                aBookModel.RecType = (short)(aBookSchedules.RecType == true ? 1 : 0);
                List<string> days = new List<string>();
                if (aBookSchedules.Sat.HasValue && aBookSchedules.Sat.Value)
                    days.Add("Sat");
                if (aBookSchedules.Sun.HasValue && aBookSchedules.Sun.Value)
                    days.Add("Sun");
                if (aBookSchedules.Mon.HasValue && aBookSchedules.Mon.Value)
                    days.Add("Mon");
                if (aBookSchedules.Thu.HasValue && aBookSchedules.Thu.Value)
                    days.Add("Thu");
                if (aBookSchedules.Wed.HasValue && aBookSchedules.Wed.Value)
                    days.Add("Wed");
                if (aBookSchedules.Tue.HasValue && aBookSchedules.Tue.Value)
                    days.Add("Tue");
                if (aBookSchedules.Fri.HasValue && aBookSchedules.Fri.Value)
                    days.Add("Fri");

                aBookModel.SchedDays = days.ToArray();
                if (aBookSchedules.StartDatetime.HasValue)
                {
                    aBookModel.StartDatetime = aBookSchedules.StartDatetime.Value.ToString("dd/MM/yyyy HH:mm");
                }
                aBookModel.StartLoc.id = aBookSchedules.StartLoc.Value;
                aBookModel.StartLoc.text = aBookSchedules.Location.Name;
                if (aBookSchedules.TillDate.HasValue)
                {
                    aBookModel.inputTillDay = aBookSchedules.TillDate.Value.ToString("dd/MM/yyyy HH:mm");
                    aBookModel.TillDateStr = aBookSchedules.TillDate.Value.ToString("dd/MM/yyyy HH:mm");
                }
                if (aBookSchedules.ClientID > 0)
                {
                    aBookModel.ClientID.id = aBookSchedules.ClientID;
                    aBookModel.ClientID.text = aBookSchedules.Client.ClientName;
                }
                aBookModel.TotalCost = aBookSchedules.TotalCost;

                //aBookModel.CreatedBy = aBook.CreatedBy;

                aBookModel.TripList = aBookSchedules.Trips.Where(t => t.BookID > 0).Select(prepareModel).ToList();
                //for(int x=0;x< aBookModel.TripList.Count;x++)
                //{
                //    TripModel aTripModel = aBookModel.TripList[x];


                //}
                if (aBookSchedules != null)
                    return aBookModel;
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private TripModel prepareModel(Trip paratrip)
        {

            TripModel aTripModel = new TripModel();
            try
            {
                aTripModel.BookID = paratrip.BookID;
                aTripModel.CarFuelCost = paratrip.CarFuelCost.HasValue ? paratrip.CarFuelCost.Value : 0;
                aTripModel.Consumption = paratrip.Consumption.HasValue ? paratrip.Consumption.Value : 0;
                aTripModel.DriverFees = paratrip.DriverFees.HasValue ? paratrip.DriverFees.Value : 0;
                aTripModel.EndDate = paratrip.EndDate.ToString("dd/MM/yyyy HH:mm");
                aTripModel.EndLocid = new AttbuteObject(paratrip.EndLocid.HasValue ? paratrip.EndLocid.Value : 0, paratrip.EndLocid.HasValue ? paratrip.Location.Name : "");
                //paratrip.EndLocid.HasValue ? paratrip.EndLocid.Value : 0;
                aTripModel.OccursOn = paratrip.OccursOn;
                aTripModel.Other = paratrip.Other.Value;
                aTripModel.ParkingFees = paratrip.ParkingFees.HasValue ? paratrip.ParkingFees.Value : 0;
                aTripModel.Rec_Type = paratrip.Rec_Type;
                aTripModel.RepeatEvery = paratrip.RepeatEvery;
                aTripModel.SalikFees = paratrip.SalikFees.HasValue ? paratrip.SalikFees.Value : 0;
                aTripModel.StartDate = paratrip.StartDate.HasValue ? paratrip.StartDate.Value.ToString("dd/MM/yyyy HH:mm") : null;
                aTripModel.TripID = paratrip.TripID;
                aTripModel.TripName = paratrip.TripName;
                aTripModel.DriverTrip = paratrip.DriverTrip;
                aTripModel.VehicleID = new AttbuteObject(paratrip.VehicleID.HasValue ? paratrip.VehicleID.Value : 0, paratrip.VehicleID.HasValue ? paratrip.Vehicle.VehicleNum : "");
                aTripModel.DriverID = new AttbuteObject(paratrip.DriverTrips.Count > 0 ? paratrip.DriverTrips.Last().DriverID : 0, paratrip.DriverTrips.Count > 0 ? paratrip.DriverTrips.Last().Driver.FullName : "");

                aTripModel.DriverTripName = paratrip.DriverTrips.Count > 0 ? paratrip.DriverTrips.Last().Driver.FullName : "";

            }
            catch (Exception ex)
            {

            }
            return aTripModel;
        }

        private Stream GetPDFDoc(string paraContent)
        {
            MemoryStream workStream = new MemoryStream();

            Document myDocument = new Document(PageSize.A4.Rotate());
            try
            {
                Document document = new Document();
                PdfWriter.GetInstance(document, workStream).CloseStream = false;
                // step 1: creation of a document-object
                // step 2:
                // step 3:  Open the document now using
                myDocument.Open();

                // step 4: Now add some contents to the document
                myDocument.Add(new iTextSharp.text.Paragraph("fffffff"));
                myDocument.Close();
            }
            catch (DocumentException de)
            {
                Console.Error.WriteLine(de.Message);
            }
            catch (IOException ioe)
            {
                Console.Error.WriteLine(ioe.Message);
            }
            // step 5: Remember to close the documnet
            myDocument.Close();
            return workStream;
        }
        [HttpPost]
        public JsonResult GetBookInfo(int? parBookScheduleID)
        {
            return Json(parBookScheduleID);

        }
        public FileResult Print(int? parBookScheduleId)
        {

            Document document = new Document();
            string contents = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(@"../Booking_Offer.htm"));
            PdfWriter.GetInstance(document, new FileStream(Request.PhysicalApplicationPath + "\\Chap0107.pdf", FileMode.Create));
            document.Open();
            ///Image pdfImage = Image.GetInstance(Server.MapPath("logo.png"));

            ///pdfImage.ScaleToFit(100, 50);

            ///pdfImage.Alignment = iTextSharp.text.Image.UNDERLYING; pdfImage.SetAbsolutePosition(180, 760);

            ///document.Add(pdfImage);
            iTextSharp.text.html.simpleparser.StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
            iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(document);
            hw.Parse(new StringReader(contents));
            document.Close();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.AddHeader("Content-Disposition", "inline;filename=" + "Chap0101.pdf");
            Response.ContentType = "application/pdf";
            Response.WriteFile("Chap0101.pdf");
            Response.Flush();
            Response.Clear();
            return null;

            //////////////////////////////////////return File(System.Web.HttpContext.Current.Server.MapPath(@"../jk.pdf"), "application/pdf");
            ////////////////////////////////////var reader = new PdfReader(System.Web.HttpContext.Current.Server.MapPath(@"../jk11.pdf"));
            ////////////////////////////////////var outputPdfStream = new MemoryStream();
            ////////////////////////////////////var stamper = new PdfStamper(reader, outputPdfStream) { FormFlattening = true, FreeTextFlattening = false };
            ////////////////////////////////////var form = stamper.AcroFields;

            ////////////////////////////////////form.SetField("BookDate", "dddd");
            ////////////////////////////////////form.SetField("BookRefNo", "500-0000324");
            ////////////////////////////////////ColumnText column = new ColumnText(stamper.GetUnderContent(1));
            ////////////////////////////////////column.AddElement(createtable());
            ////////////////////////////////////stamper.SetFullCompression();
            ////////////////////////////////////stamper.FormFlattening = true;

            ////////////////////////////////////stamper.Close();

            ////////////////////////////////////var file = outputPdfStream.ToArray();
            ////////////////////////////////////var output = new MemoryStream();
            ////////////////////////////////////output.Write(file, 0, file.Length);
            ////////////////////////////////////output.Position = 0;

            ////////////////////////////////////var cd = new System.Net.Mime.ContentDisposition
            ////////////////////////////////////{
            ////////////////////////////////////    FileName = "Reiseforsikringskort.pdf",
            ////////////////////////////////////    Inline = false,
            ////////////////////////////////////    Size = file.Length,
            ////////////////////////////////////    CreationDate = DateTime.Now

            ////////////////////////////////////};

            ////////////////////////////////////Response.AppendHeader("Content-Disposition", cd.ToString());
            ////////////////////////////////////return File(output, System.Net.Mime.MediaTypeNames.Application.Pdf);
            //pdfStamper.setFormFlattenin(true);
            //AcroFields form = pdfStamper.getAcroFields();

            //form.setField("ID", "99999");
            //form.setField("ADDR1", "425 Test Street");
            //form.setField("ADDR2", "Test, WA 91334");
            //form.setField("PHNBR", "(999)999-9999");
            //form.setField("NAME", "John Smith");

            ////CREATE TABLE
            //PdfPTable table = new PdfPTable(3);
            //Font bfBold12 = new Font(FontFamily.HELVETICA, 12, Font.BOLD, new BaseColor(0, 0, 0));
            //insertCell(table, "Table", Element.ALIGN_CENTER, 1, bfBold12);
            //table.completeRow();

            //ColumnText column = new ColumnText(pdfStamper.getOverContent(1));
            //column.addElement(table);

            //            pdfStamper.Close();
            //          reader.Close();
            //        return new FileStreamResult(pdfResponse, "application/pdf");
            ////// System.Windows.Forms.RichTextBox rtBox = new System.Windows.Forms.RichTextBox();
            ////// string path = System.Web.HttpContext.Current.Server.MapPath(@"../Untitled 1.pdf");
            //////// rtBox.LoadFile(@path);
            ////// //string x = rtBox.Text.Replace("[Enter a date]", "dddd");
            ////// FileStreamResult ddd = new FileStreamResult(path, "application/pdf");
            ////// MemoryStream workStream = new MemoryStream();
            ////// Document document = new Document();
            ////// PdfWriter.GetInstance(document, workStream).CloseStream = false;

            ////// document.Open();
            ////// document.Add(new Paragraph(rtBox.Text));
            ////// document.Add(new Paragraph(DateTime.Now.ToString()));
            ////// document.Close();

            ////// byte[] byteInfo = workStream.ToArray();
            ////// workStream.Write(byteInfo, 0, byteInfo.Length);
            ////// workStream.Position = 0;

            ////// return new FileStreamResult(workStream, "application/pdf");


            //////////  string template = System.Web.HttpContext.Current.Server.MapPath(@"../Untitled 1.pdf");
            //////////  //string[] data = parBookScheduleId.Split(';');
            //////////  PdfReader pdfReader = new PdfReader(template);
            //////////  MemoryStream pdfResponse = new MemoryStream();
            //////////  PdfStamper pdfStamper = new PdfStamper(pdfReader, pdfResponse);

            //////////  AcroFields pdfFormFields = pdfStamper.AcroFields;
            //////////  pdfFormFields.SetField("BookDate", DateTime.Now.ToString());
            //////////  // in real life, this value comes from a database
            //////////  ColumnText column = new ColumnText(pdfStamper.GetOverContent(1));
            //////////  column.AddElement(createtable());
            ////////////  pdfStamper.Close();
            //////////  pdfReader.Close();
            //////////  return new FileStreamResult(pdfResponse, "application/pdf"); ;
        }

        private IElement createtable()
        {
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font normalFont = new Font(bfTimes, 9f, Font.NORMAL, BaseColor.BLACK);
            Font titleFont = new Font(bfTimes, 11f, Font.BOLD, BaseColor.BLACK);
            Font timeFont = new Font(bfTimes, 10f, Font.NORMAL, BaseColor.BLACK);
            Font headerFont = new Font(bfTimes, 9f, Font.BOLD, BaseColor.BLACK);
            Font cellFont = new Font(bfTimes, 9f, Font.NORMAL, BaseColor.BLACK);
            Font cellFontRed = new Font(bfTimes, 9f, Font.NORMAL, BaseColor.RED);

            int cols = 3;// GridView1.Columns.Count;
            int rows = 1;// GridView1.Rows.Count;

            PdfPTable table = new PdfPTable(cols);
            table.SpacingBefore = 4;
            table.SpacingAfter = 8;
            table.WidthPercentage = 100;
            table.AddCell(new Phrase("jk1", headerFont));
            table.AddCell(new Phrase("jk2", headerFont));
            //for (int j = 0; j < GridView1.Columns.Count; j++)
            //{
            //    table.AddCell(new Phrase(GridView1.Columns[j].HeaderText, headerFont));
            //}

            table.HeaderRows = 1;
            table.AddCell(new Phrase("ddd2", cellFont));
            //for (int i = 0; i < GridView1.Rows.Count; i++)
            //{
            //    for (int k = 0; k < GridView1.Columns.Count; k++)
            //    {
            //        string cellValue = GridView1.Rows[i].Cells[k].Text;
            //        if (cellValue != null)
            //        {
            //            table.AddCell(new Phrase(cellValue, cellFont));
            //        }
            //    }
            //}
            //pdfDoc.Add(table);
            //pdfDoc.Close();
            //GridView1.AllowPaging = true;
            //GridView1.DataBind();

            return table;
        }
        public ActionResult AddNewString(string parBookScheduleId)
        {
            string[] data = parBookScheduleId.Split(';');
            if (data.Count() > 0)
            {
                if (data[1] == "Clinet")
                    return RedirectToAction("Create", "Clients");
                else if (data[1] == "Driver")
                    return RedirectToAction("Create", "Drivers");
                else if (data[1] == "Vehicle")
                    return RedirectToAction("Create", "Vehicles");

            }

            return View("");
        }

        // GET: BookSchedules/Edit/5
        public ActionResult Edit(int? parBookScheduleId)
        {
            try
            {
                if (parBookScheduleId <= 0)
                {
                    return Json("Book Id Must be not Null");
                }
                BookSchedule aBookSchedule = db.BookSchedules.Single(bk => bk.ID == parBookScheduleId);

                if (aBookSchedule == null)
                {
                    return Json("Error in reading Book ");
                }
                ViewBag.CreatedBy = new SelectList(db.Users, "ID", "UserName");

                ViewBag.BookObjectStr = new JavaScriptSerializer().Serialize(GetBookInfoaAsObjectModel(parBookScheduleId));
                return View(aBookSchedule);

            }
            catch (Exception ex)
            {
                throw (new Exception(ex.Message));
            }
            return Json("Exception Error");
        }

        [HttpPost]
        public ActionResult EditMdel(BookModel model)
        {
            try
            {
                Book aBook = db.Books.Single(bk => bk.ID == model.BookID);
                BookSchedule oldBookSched = db.BookSchedules.Single(bk => bk.ID == model.ScheduleID && bk.Status == "Active");
                oldBookSched.Status = "canceled";

                BookSchedule aBookSchedule = new BookSchedule();
                aBookSchedule.Status = "Active";
                aBookSchedule.ClientID = model.ClientID.id.Value;
                //book.Client = db.Clients.FirstOrDefault(a => a.ClientID == model.ClientID);
                aBookSchedule.Refnum = Get8Digits();
                aBookSchedule.CreatedBy = CurrentUser.UserID;
                aBookSchedule.StartDatetime = DateTime.ParseExact(model.StartDatetime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                aBookSchedule.StartLoc = model.StartLoc.id;
                if (model.DepartmentID.id > 0)
                    aBookSchedule.DepartmentID = model.DepartmentID.id;

                if (model.CustomerID.id > 0)
                    aBookSchedule.CustomerID = model.CustomerID.id;
                aBookSchedule.ContactType = model.ContactType.id.ToString();
                aBookSchedule.RecType = model.RecType == 1 ? true : false;
                aBookSchedule.TillDate = DateTime.ParseExact(model.inputTillDay, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                if (model.SchedDays != null && model.SchedDays.GetLength(0) > 0)
                {
                    foreach (var item in model.SchedDays)
                    {
                        if (item == "Sat")
                            aBookSchedule.Sat = true;
                        if (item == "Sun")
                            aBookSchedule.Sun = true;
                        if (item == "Mon")
                            aBookSchedule.Mon = true;
                        if (item == "Tue")
                            aBookSchedule.Tue = true;
                        if (item == "Wed")
                            aBookSchedule.Wed = true;
                        if (item == "Thu")
                            aBookSchedule.Thu = true;
                        if (item == "Fri")
                            aBookSchedule.Fri = true;
                    }
                }
                aBookSchedule.TotalCost = model.TotalCost;
                for (int counter = 0; counter < model.TripList.Count; counter++)
                {
                    var objTrip = model.TripList[counter];
                    Trip trip = new Trip();
                    //trip.BookID = aBookSchedule.ID;
                    if (objTrip.OverWriteLocID.id > 0)
                        trip.OverWriteLocID = objTrip.OverWriteLocID.id;
                    trip.EndLocid = objTrip.EndLocid.id;
                    if (counter == 0)
                        trip.EndDate = DateTime.ParseExact(model.StartDatetime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                    else
                        trip.EndDate = DateTime.ParseExact(model.TripList[counter - 1].EndDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                    trip.EndDate = trip.EndDate;// DateTime.ParseExact(trip.EndDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                    trip.SalikFees = objTrip.SalikFees;
                    trip.DriverFees = objTrip.DriverFees;
                    trip.CarFuelCost = objTrip.CarFuelCost;
                    trip.ParkingFees = objTrip.ParkingFees;
                    trip.Consumption = objTrip.Consumption;
                    trip.DriverTrip = Convert.ToByte(objTrip.DriverTrip);
                    trip.Rec_Type = objTrip.Rec_Type;
                    trip.RepeatEvery = objTrip.RepeatEvery;
                    trip.Other = objTrip.Other;
                    trip.CreatedAt = DateTime.Now;
                    if (objTrip.VehicleID.id > 0)
                        trip.VehicleID = objTrip.VehicleID.id;

                    aBookSchedule.Trips.Add(trip);
                    if (objTrip.DriverID.id > 0)
                    {
                        DriverTrip driverTrip = new DriverTrip();
                        driverTrip.TripID = trip.TripID;
                        driverTrip.DriverID = objTrip.DriverID.id.Value;
                        driverTrip.CreatedBy = CurrentUser.UserID;
                        trip.DriverTrips.Add(driverTrip);
                    }
                }
                aBook.BookSchedules.Add(aBookSchedule);

                db.SaveChanges(CurrentUser.UserID);
                BookResult aBookResult = new BookResult();
                aBookResult.BookRefNum = aBook.BookRef;
                aBookResult.IDBook = aBook.ID;

                aBookResult.SchRefnum = aBookSchedule.Refnum;// get the ref from book which it is the edit ref num
                aBookResult.IDBookSch = aBookSchedule.ID;

                return Json(aBookResult); //RedirectToAction("Index","Clients");

            }
            catch (Exception ex)
            {
                throw (new Exception(ex.Message));
            }
        }


        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookSchedule aBookSchedule = db.BookSchedules.Find(id);
            if (aBookSchedule == null)
            {
                return HttpNotFound();
            }
            return View(aBookSchedule);
        }

        [SessionExpireFilter]
        public ActionResult FindBooksGrid(BooksFilterModel model)
        {
            BooksService booksService = new BooksService();
            int count = 0;

            var list = booksService.FindBooks(model, out count);
            return Json(new { data = list, itemsCount = count }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

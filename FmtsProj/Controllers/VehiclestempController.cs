﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;

namespace FmtsProj.Controllers
{
    public class VehiclestempController : Controller
    {
        private BMOSContext db = new BMOSContext();

        // GET: Vehiclestemp
        public ActionResult Index()
        {
            var vehicles = db.Vehicles.Include(v => v.Owner).Include(v => v.VehicleOwner);
            return View(vehicles.ToList());
        }

        // GET: Vehiclestemp/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        // GET: Vehiclestemp/Create
        public ActionResult Create()
        {
            ViewBag.OwnerID = new SelectList(db.Owners, "ID", "OwnerName");
            ViewBag.OwnerID = new SelectList(db.VehicleOwners, "ID", "Type");
            return View();
        }

        // POST: Vehiclestemp/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,VehicleNum,SeatCount,VehicleColor,VehicleStatus,VehicleModel,CreatedBy,MakeYear,ChasisNumber,Make,OwnerID,Type")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                db.Vehicles.Add(vehicle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OwnerID = new SelectList(db.Owners, "ID", "OwnerName", vehicle.OwnerID);
            ViewBag.OwnerID = new SelectList(db.VehicleOwners, "ID", "Type", vehicle.OwnerID);
            return View(vehicle);
        }

        // GET: Vehiclestemp/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            ViewBag.OwnerID = new SelectList(db.Owners, "ID", "OwnerName", vehicle.OwnerID);
            ViewBag.OwnerID = new SelectList(db.VehicleOwners, "ID", "Type", vehicle.OwnerID);
            return View(vehicle);
        }

        // POST: Vehiclestemp/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,VehicleNum,SeatCount,VehicleColor,VehicleStatus,VehicleModel,CreatedBy,MakeYear,ChasisNumber,Make,OwnerID,Type")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OwnerID = new SelectList(db.Owners, "ID", "OwnerName", vehicle.OwnerID);
            ViewBag.OwnerID = new SelectList(db.VehicleOwners, "ID", "Type", vehicle.OwnerID);
            return View(vehicle);
        }

        // GET: Vehiclestemp/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        // POST: Vehiclestemp/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vehicle vehicle = db.Vehicles.Find(id);
            db.Vehicles.Remove(vehicle);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

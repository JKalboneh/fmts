﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;
using FmtsServices;
using FmtsServices.Model;

namespace FmtsProj.Controllers
{
    public class DriversController : ControllerBase
    {
        private BMOSContext db = new BMOSContext();

        // GET: Drivers
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create(DriverModel model)
        {
            if (ModelState.IsValid)
            {
                DriversService driversService = new DriversService();
                driversService.AddDriver(model);
                return RedirectToAction("Index");
            }

            return View(model);
        }
        [HttpPost]
        public JsonResult AddNewDriver(DriverModel model)
        {
            JsonResult result = new JsonResult();
            try
            {
                    DriversService driversService = new DriversService();
                    model.ByUserID = CurrentUser.UserID;
                    driversService.AddDriver(model);
                    result = Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            return result;
        }
        [SessionExpireFilter]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DriversService driversService = new DriversService();
            var model = driversService.FindByID(id.GetValueOrDefault());
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Edit(DriverModel model)
        {
            if (ModelState.IsValid)
            {
                DriversService driversService = new DriversService();
                model.ByUserID = CurrentUser.UserID;
                driversService.UpdateDriver(model);

                return RedirectToAction("Index");
            }
            return View(model);
        }
        [SessionExpireFilter]
        public ActionResult DeleteDriver(int driverId)
        {
            DriversService driversService = new DriversService();
            driversService.DeleteDriver(driverId, CurrentUser.UserID);
            return Json("Done", JsonRequestBehavior.AllowGet);
        }

        public ActionResult DriverGrid()
        {
            List<Driver> driver = db.Drivers.ToList();
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        public ActionResult FindDriversGrid(DriverFilterModel model)
        {
            DriversService driversService = new DriversService();
            int count = 0;

            var list = driversService.FindDrivers(model, out count);
            return Json(new { data = list, itemsCount = count }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FindDrivers(string q, int? page = 1, int? pageSize = 15, int? parAddFlage = 0, string parTagsList="", int parVehicleNumid = 0,
            string parStatDate = "",
            string parEndDate = "",
            int parCurrentStopDBId=0)
        {
            DriversService driversService = new DriversService();
            int count = 0;
            var list = driversService.FindDrivers(q, out count,1, page.Value, pageSize.Value, parTagsList,parVehicleNumid,  parStatDate, parEndDate, parCurrentStopDBId);

            var selectList = list.Select(a => new { ItemType ="", OverAllColor = a.OverAllColor, id = a.DriverID, PersonalNumber = a.PersonalNumber, text = a.FullName }).ToList();
            //if(parAddFlage!=0)
            //    selectList.Insert(0, new { id = 0, text = "All"});
            return Json(new { items = selectList, count = count }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateDriver(DriverModel driverModel)
        {
            //Update Driver 
            DriversService driversService = new DriversService();
            driversService.UpdateDriver(driverModel);

            TagsService tags = new TagsService();
            tags.UpdateDriverTags(driverModel.strTags, driverModel.DriverID, CurrentUser.UserID);

            VehicleAssignDriversService vehicleAssignDriversService = new VehicleAssignDriversService();
            vehicleAssignDriversService.UpdateByVehicleIds(driverModel.strVehicles, driverModel.DriverID, CurrentUser.UserID);

            var driver = driversService.FindByID(driverModel.DriverID);
            return Json(new { driver = driver }, JsonRequestBehavior.AllowGet);
        }

    }
}

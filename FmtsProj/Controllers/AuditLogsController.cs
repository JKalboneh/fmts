﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FmtsEntity;

namespace FmtsProj.Controllers
{
    public class AuditLogsController : Controller
    {
        private BMOSContext db = new BMOSContext();

        // GET: AuditLogs
        public ActionResult Index()
        {
            var auditLogs = db.AuditLogs.Include(a => a.User);
            return View(auditLogs.ToList());
        }

        // GET: AuditLogs/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuditLog auditLog = db.AuditLogs.Find(id);
            if (auditLog == null)
            {
                return HttpNotFound();
            }
            return View(auditLog);
        }

        // GET: AuditLogs/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "UserID", "UserName");
            return View();
        }

        // POST: AuditLogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AuditLogId,UserId,EventDateUTC,EventType,TableName,RecordId,ColumnName,OriginalValue,NewValue")] AuditLog auditLog)
        {
            if (ModelState.IsValid)
            {
                auditLog.AuditLogId = Guid.NewGuid();
                db.AuditLogs.Add(auditLog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Users, "UserID", "UserName", auditLog.UserId);
            return View(auditLog);
        }

        // GET: AuditLogs/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuditLog auditLog = db.AuditLogs.Find(id);
            if (auditLog == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Users, "UserID", "UserName", auditLog.UserId);
            return View(auditLog);
        }

        // POST: AuditLogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AuditLogId,UserId,EventDateUTC,EventType,TableName,RecordId,ColumnName,OriginalValue,NewValue")] AuditLog auditLog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(auditLog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users, "UserID", "UserName", auditLog.UserId);
            return View(auditLog);
        }

        // GET: AuditLogs/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuditLog auditLog = db.AuditLogs.Find(id);
            if (auditLog == null)
            {
                return HttpNotFound();
            }
            return View(auditLog);
        }

        // POST: AuditLogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            AuditLog auditLog = db.AuditLogs.Find(id);
            db.AuditLogs.Remove(auditLog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

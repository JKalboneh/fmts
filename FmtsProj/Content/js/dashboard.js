﻿
var baseURL = window.location.protocol + "//" + window.location.host + "/"
//this function for increase and decrease value
$(document).ready(function () {

    $(".menu_with_icon").click(function () {
        document.getElementById("TagsNameVehicle").click();
    });
    $(".menu_with_icon_driver").click(function () {
        document.getElementById("TagsNameDriver").click();
    });
    
    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
    });
    $(".map_tab").click(function () {
        $(".t_table").hide(500);
        $(".box_title").text("TRIPS MAP");
        $(".g_map").show(500);
        $(".map_table").removeClass("tabActive");
        $(this).addClass("tabActive");
    });

    $(".map_table").click(function () {
        $(".g_map").hide(500);
        $(".box_title").text("TRIPS TABLE");
        $(".t_table").show(500);
        $(".map_tab").removeClass("tabActive");
        $(this).addClass("tabActive");
    });
    $('#inputNameClient').selectivity({
        allowClear: true,
        multiple: false,
        ajax: {
            url: baseURL + "Clients/FindClients",
            minimumInputLength: 0,
            quietMillis: 250,
            params: function (term, offset) {
                // GitHub uses 1-based pages with 30 results, by default
                return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15, parAddFlage: 1 };
            },
            fetch: function (url, init, queryOptions) {
                return $.ajax(url).then(function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                id: item.id,
                                text: item.text
                            };
                        }),
                        more: (data.count > queryOptions.offset + data.items.length)
                    };
                });
            }
        },
        placeholder: 'Client',
        //templates: {
        //    resultItem: function (item) {
        //        return (
        //            '<div class="selectivity-result-item" data-item-id="' + item.id + '">' +
        //            '<b>' + item.text + '</b><br>' +
        //            '</div>'
        //        );
        //    }
        //}
    });
        
    initVehicleNum($('#inputNameVehicle'),
        VehicleFunctionNumparamters,false);
    
    function VehicleFunctionNumparamters(term, offset) {
        // GitHub uses 1-based pages with 30 results, by default
        return {
            q: term, page: 1 + Math.floor(offset / 15),
            pageSize: 15, parAddFlage: 1,
            parTagsList: $('#TagsNameVehicle').selectivity('val'),
            parDriverID: $('#inputNameDriver').selectivity('val'),//$('#inputparDriver').selectivity('val'),
            parStatDate: $("#fromDate").val(),
            parEndDate: $("#toDate").val(),
            parCurrentStopDBId: ''
        };
    };
initDriverName($('#inputNameDriver'), function (term, offset) {
    // GitHub uses 1-based pages with 30 results, by default
    return {
        q: term, page: 1 + Math.floor(offset / 15),
        pageSize: 15,
        parAddFlage: 1,
        parTagsList: $('#TagsNameDriver').selectivity('val'),
        parStatDate: $('#fromDate').val(),
        parEndDate: $('#toDate').val(),
    };
});
   
    $('#TagsNameVehicle').selectivity({
        allowClear: true,
        multiple: true,
        ajax: {
            url: baseURL + "Tags/FindTagsList",
            minimumInputLength: 0,
            quietMillis: 250,
            params: function (term, offset) {

                // GitHub uses 1-based pages with 30 results, by default
                return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15, parAddFlage: 1 };
            },
            fetch: function (url, init, queryOptions) {
                return $.ajax(url).then(function (data) {
                    return {

                        results:

                        $.map(data.items, function (item) {
                            return {
                                id: item.id,//Change it to ID 
                                text: item.text//Change it to Text
                            };
                        }),
                        more: (data.count > queryOptions.offset + data.items.length)
                    };
                });
            }
        },
        placeholder: 'Tags',
        templates: {
            resultItem: function (item) {
                return (
                    '<div class="selectivity-result-item" data-item-id="' + item.id + '">' +
                    '<b>' + item.text + '</b><br>' +
                    '</div>'
                );
            }
  }
});
    
    $('#TagsNameDriver').selectivity({
        allowClear: true,
        multiple: true,
        ajax: {
            url: baseURL + "Tags/FindTagsList",
            minimumInputLength: 0,
            quietMillis: 250,
            params: function (term, offset) {

                // GitHub uses 1-based pages with 30 results, by default
                return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15, parAddFlage: 1 };
            },
            fetch: function (url, init, queryOptions) {
                return $.ajax(url).then(function (data) {
                    return {

                        results:

                        $.map(data.items, function (item) {
                            return {
                                id: item.id,//Change it to ID 
                                text: item.text//Change it to Text
                            };
                        }),
                        more: (data.count > queryOptions.offset + data.items.length)
                    };
                });
            }
        },
        placeholder: 'Tags',
        templates: {
            resultItem: function (item) {
                return (
                    '<div class="selectivity-result-item" data-item-id="' + item.id + '">' +
                    '<b>' + item.text + '</b><br>' +
                    '</div>'
                );
            }

        }
    });
        $('#inputNameClient').on("selectivity-selected", function (e) {
        getDashboardInfo(e);
        closeText();
    });
    $('#inputNameVehicle').on("selectivity-selected", function (e) {
        getDashboardInfo(e);
        closeText();
    });
    $('#inputNameDriver').on("selectivity-selected", function (e) {
        getDashboardInfo(e);
        closeText();
    });
    //$('#fromDate').focusout('input', getDashboardInfo);
    //$('#toDate').focusout('input', getDashboardInfo);
    var fromDate = new Date().setHours(0, 0, 0, 0);
    var toDate = new Date(fromDate + 864e5).setMinutes(-1);
    $('#fromDate').flatpickr({
        enableTime: true,
        dateFormat: "d/m/Y H:i",
        confirmText: "OK",
        "plugins": [new confirmDatePlugin({})],
        onChange: function (dateobj, datestr) {
            getDashboardInfo();
        },
        defaultDate: fromDate
    });
    $('#toDate').flatpickr({
        enableTime: true,
        dateFormat: "d/m/Y H:i",
        confirmText: "OK",
        "plugins": [new confirmDatePlugin({})],
        onChange: function (dateobj, datestr) {
            getDashboardInfo();
        },
        defaultDate: toDate
    });
    $('#fromDate').keypress(function (e) {
        if (e.which == 13) {
            getDashboardInfo();
        }
    });
    $('#toDate').keypress(function (e) {
        if (e.which == 13) {
            getDashboardInfo();
        }
    });

    $("#the_name").prop('disabled', true).parent('label').addClass('disabled-label');
    $("#dialog").dialog({
        height: 'auto',
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });
    $("#CustomerGrid .details").click(function () {

        var bookid = $(this).closest("tr").find("td").eq(0).attr("id");
        bookid = bookid.trim();
        $.ajax({
            type: "POST",
            url: "/Dashboard/TripDetials",
            data: '{Bookid: "' + bookid + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            success: function (response) {
                $('#dialog').html(response);
                $('#dialog').dialog('open');
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    });
    initSlider(".drivers_item");
    initSlider(".vec_item");

   

    $("#btnClear").click(function () {
        $('#fromDate').flatpickr({
            enableTime: true,
            dateFormat: "d/m/Y H:i",
            confirmIcon: "<i class='fa fa-check'></i>", // your icon's html, if you wish to override
            confirmText: "OK ",
            showAlways: false,
            theme: "light" ,// or "dark"
            onChange: function (dateobj, datestr) {
                getDashboardInfo();
            },
            defaultDate: fromDate
        });
        $('#toDate').flatpickr({
            enableTime: true,
            dateFormat: "d/m/Y H:i",
            confirmIcon: "<i class='fa fa-check'></i>", // your icon's html, if you wish to override
            confirmText: "OK ",
            showAlways: false,
            theme: "light", // or "dark"
            onChange: function (dateobj, datestr) {
                getDashboardInfo();
            },
            defaultDate: toDate
        });
       
        getDashboardInfo();
    });

    getDashboardInfo();
});



var isDriversLosaded = false;
var isVehiclesLosaded = false;
window.PlayJehad = function() { alert("dddd"); };
function getDashboardInfo() {
    var data = {};
    data.fromDatetime = $("#fromDate").val();
    data.toDatetime = $("#toDate").val();
    data.driverId = GetIdFromselectivity($('#inputNameDriver'));
    data.clientId = GetIdFromselectivity($('#inputNameClient'));//$('#inputNameClient').selectivity('data') == null ? 0 : $('#inputNameClient').selectivity('data').id;
    data.vehicleId = GetIdFromselectivity($('#inputNameVehicle'));//.selectivity('data') == null ? 0 : $('#inputNameVehicle').selectivity('data').id;
    data.driverId = data.driverId > 0 ? data.driverId : null;
    data.clientId = data.clientId > 0 ? data.clientId : null;
    data.vehicleId = data.vehicleId > 0 ? data.vehicleId : null;

    $('#loaderOverall').show();
    $('#OverallNumber').hide();
    //jQuery('#bookTemplate').html('');


    $(".slick-slide").each(function (key, value) {
        $('.drivers_item').slick('slickRemove', 0);
        $('.vec_item').slick('slickRemove', 0);
    })


    getDriverDashboardInfo(data);
    getVehicleDashboardInfo(data);
    getBookDashboard(data);
    var unassdata = {};
    unassdata.paraDateTime = $("#fromDate").val();
    unassdata.paraToTime = $("#toDate").val();
    unassdata.paraDriverId = $('#inputNameDriver').selectivity('val');
    unassdata.paraClientId = $('#inputNameClient').selectivity('val');
    unassdata.paraVehicaleId = $('#inputNameVehicle').selectivity('val');

    getUnassignedTripsDashboard(unassdata);
}

function getDriverDashboardInfo(data) {
    
    $.ajax({
        url: '/Dashboard/GetDriverDashboardInfo/',
        data: data,
        cache: false,
        dataType: "json",
        type: "GET",
        contentType: "application/json; charset=utf-8",
        beforeSend: function () {
            $('#loaderDrivers').show();
            $('#loaderDriversItem').show();
            $('#DriversNumber').hide();
            $('#driversItem').hide(); 
        },
        success: function (data) {
            isDriversLosaded = true;
            $('#loaderDrivers').hide(); 
            $('#DriversNumber').show(); 

            if (data != null || data != undefined)
                $("#DriversNumber").text(parseInt(data.DriverUtilization) + "%");
            else
                $("#DriversNumber").text(0 + "%");

            var driversInfoList = data.DriversInfoList;

            $('#loaderDriversItem').hide(); 
            $('#driversItem').show(); 
            
            setOverallUtilization();

            $.each(driversInfoList, function (key, value) {
                var html =
                    ' <div>' +
                    ' <div class="col-lg-12 col-sm-12">' +
                    '<div class="trips_icon col-md-12">' +
                    '<div class="col-md-3 col-sm-3 col-xs-3">' +
                    ' <img src="/Content/images/person_icon.png" width="45" class="trips_box_icon" />' +
                    ' </div>' +
                    '<div class="col-md-9 col-sm-9 col-xs-9">' +
                '<h4 class="trips_box_title">' + value.FullName + '' +
                    '<p class="driver_number">' + value.PersonalNumber + '</p>' +
                    '<p class="DateRange">' + $('#fromDate').val() + '<br />' + $('#toDate').val() + '</p>' +
                    '</div>' +
                    '<div class="clear"></div>' +
                    '<div class="col-md-6 col-sm-6 col-xs-6 driver_info_box_left">' +
                    ' <p class="done_trpis">Booked</p>' +
                    '<p class="numb_done_trp">' + value.TotalHourtripAct + ' hours </p>' +
                    '</div>' +
                    '<div class="col-md-6 col-sm-6 col-xs-6 driver_info_box_right">' +
                    '<p class="done_trpis">Free</p>' +
                    ' <p class="numb_done_trp_def">' + value.FreeTime + ' hours </p>' +
                    ' </div>' +
                    ' <div class="clear"></div>' +
                    ' <div class="col-md-12 col-sm-12 col-xs-12 drv_uti no-padding">' +
                    ' <img src="/Content/images/d_u.png" />' +
                    '</div>' +
                    '<div class="clear"></div>' +
                    '</div>' +
                    ' <div class="clear"></div>' +
                    '</div>' +
                    '</div >';
                $("#driversItem").append(html);
            });

            $('#driversItem').slick('unslick').slick('reinit');
            sliderInit('#driversItem');
        },
        error: function (response) {
            console.log('getVehicleDashboardInfo error');
        },
        failure: function (response) {
            console.log('getVehicleDashboardInfo failure');
        }
    });

}

function getVehicleDashboardInfo(data)
{
    $.ajax({
        url: '/Dashboard/GetVehicleDashboardInfo/',
        data: data,
        cache: false,
        dataType: "json",
        type: "GET",
        contentType: "application/json; charset=utf-8",
        beforeSend: function () {
            $('#loaderVehicles').show();
            $('#loaderVehiclesItem').show();
            $('#VehiclesNumber').hide();
            $('#vehicleItem').hide();
        },
        success: function (data) {
            isVehiclesLosaded = true;
            $('#loaderVehicles').hide();
            $('#VehiclesNumber').show();

            if (data != null || data != undefined)
            {
                $("#VehiclesNumber").text(parseInt(data.VehicleUtilization) + "%");
                var vehiclesInfoList = data.VehiclesInfoList;

                $('#loaderVehiclesItem').hide();
                $('#vehicleItem').show();

                setOverallUtilization();

                $.each(vehiclesInfoList, function (key, value) {
                    var html =
                        ' <div>' +
                        ' <div class="col-lg-12 col-sm-12">' +
                        '<div class="trips_icon col-md-12">' +
                        '<div class="col-md-3 col-sm-3 col-xs-3">' +
                        ' <img src="/Content/images/car_icon.png" width="45" class="trips_box_icon" />' +
                        ' </div>' +
                        '<div class="col-md-9 col-sm-9 col-xs-9">' +
                        '<h4 class="trips_box_title">' + value.VehicleNum + '' +
                        '<p class="DateRange">' + $('#fromDate').val() + '<br />' + $('#toDate').val() + '</p>' +
                        '</div>' +
                        '<div class="clear"></div>' +
                        '<div class="col-md-6 col-sm-6 col-xs-6 driver_info_box_left">' +
                        ' <p class="done_trpis">Booked</p>' +
                        '<p class="numb_done_trp">' + value.TotalHourTripAct + ' hours</p>' +
                        '</div>' +
                        '<div class="col-md-6 col-sm-6 col-xs-6 driver_info_box_right">' +
                        '<p class="done_trpis">Free</p>' +
                        ' <p class="numb_done_trp_def">' + value.FreeTime + ' hours</p>' +
                        ' </div>' +
                        ' <div class="clear"></div>' +
                        ' <div class="col-md-12 col-sm-12 col-xs-12 drv_uti no-padding">' +
                        ' <img src="/Content/images/v.png" />' +
                        '</div>' +
                        '<div class="clear"></div>' +
                        '</div>' +
                        ' <div class="clear"></div>' +
                        '</div>' +
                        '</div >';
                    $(".vec_item").append(html);
                });

                $('.vec_item').slick('unslick').slick('reinit');
                sliderInit('.vec_item');

            }
            else
                $("#VehiclesNumber").text(parseFloat(0).toFixed(2) + "%");


        },
        error: function (response) {
            console.log('getVehicleDashboardInfo error');
        },
        failure: function (response) {
            console.log('getVehicleDashboardInfo error');
        }
    });

}

function getBookDashboard(data)
{
    $.ajax({
        url: '/Dashboard/GetBookDashboardInfo/',
        data: data,
        cache: false,
        dataType: "json",
        type: "GET",
        contentType: "application/json; charset=utf-8",
        beforeSend: function () {
            $('#loaderBooks').show();
            $('#bookTable').hide();
        },
        success: function (data) {
            
            $('#tbbook').empty();
            $('#loaderBooks').hide();
            $('#bookTable').show();
            $("#bookTemplate").tmpl(data).prependTo("#tbbook");
        },
        error: function (response) {
            console.log('GetBookDashboardInfo error');
        },
        failure: function (response) {
            console.log('GetBookDashboardInfo failure');
        }
    });
}
//$(".trips_box_number").eq(3).text(parseFloat(data.unassignedTrips).toFixed(2) + "%");
function getUnassignedTripsDashboard(data)
{
    $.ajax({
        url: '/Dashboard/GetUnassignedTripsDashboardInfo/',
        cache: false,
        dataType: "json",
        type: "GET",
        data:data,
        contentType: "application/json; charset=utf-8",
        beforeSend: function () {
            $('#loaderUnassigned').show();
            $('#UnassignedNumber').hide();
        },
        success: function (data) {
            $('#loaderUnassigned').hide();
            $('#UnassignedNumber').show();

            $("#UnassignedNumber").text(parseInt(data));
        },
        error: function (response) {
            console.log('getUnassignedTripsDashboard error');
        },
        failure: function (response) {
            console.log('getUnassignedTripsDashboard failure');
        }
    });
}
function setOverallUtilization()
{
    
    if (isVehiclesLosaded && isDriversLosaded)
    {
        var drivers = $("#DriversNumber").text();
        var vehicles = $("#VehiclesNumber").text();

        var all = (parseFloat(drivers) + parseFloat(vehicles))/2

        $("#OverallNumber").text(parseInt(all)+ "%" );

        $('#loaderOverall').hide();
        $('#OverallNumber').show();

        isDriversLosaded = false;
        isVehiclesLosaded = false;
    }
}

function buildDashboard(data) {

    console.log(data);
    $(".slick-slide").each(function (key, value) {
        $('.drivers_item').slick('slickRemove', 0);
        $('.vec_item').slick('slickRemove', 0);
    })
    $(".trips_box_number").eq(0).text(parseInt(data.overallUtilization)+ "%");


    if (data.driverDashboardInfo != undefined)
        $(".trips_box_number").eq(1).text(parseInt(data.driverDashboardInfo.driverUtilization) + "%");
    else
        $(".trips_box_number").eq(1).text(0 + "%");


    if (data.vehicleDashboardInfo != undefined)
        $(".trips_box_number").eq(2).text(parseInt(data.vehicleDashboardInfo.vehiclesUtilization)+ "%");
    else
        $(".trips_box_number").eq(2).text(0 + "%");


    $(".trips_box_number").eq(3).text(parseInt(data.unassignedTrips)+ "%");

    var dataCars = data.driverDashboardInfo.driversInfo;
    var dataCars1 = data.vehicleDashboardInfo.vehiclesInfo;

    data = '';// need to change
    var data1 = '';
    $.each(dataCars, function (key, value) {
        data =
            ' <div>' +
            ' <div class="col-lg-12 col-sm-12">' +
            '<div class="trips_icon col-md-12">' +
            '<div class="col-md-3 col-sm-3 col-xs-3">' +
            ' <img src="/Content/images/person_icon.png" width="45" class="trips_box_icon" />' +
            ' </div>' +
            '<div class="col-md-9 col-sm-9 col-xs-9">' +
            '<h4 class="trips_box_title">' + dataCars[key].driverName + '' +
        '<p class="driver_number">' + dataCars[key].personalNumber + '</p>' +
        '<p class="DateRange">' + $('#fromDate').val() + '<br />' + $('#toDate').val() + '</p>' +
            '</div>' +
            '<div class="clear"></div>' +
            '<div class="col-md-6 col-sm-6 col-xs-6 driver_info_box_left">' +
            ' <p class="done_trpis">Booked</p>' +
            '<p class="numb_done_trp">' + dataCars[key].driverTotalHourtripAct + '</p>' +
            '</div>' +
            '<div class="col-md-6 col-sm-6 col-xs-6 driver_info_box_right">' +
            '<p class="done_trpis">Free</p>' +
            ' <p class="numb_done_trp_def">' + dataCars[key].freeTime + '</p>' +
            ' </div>' +
            ' <div class="clear"></div>' +
            ' <div class="col-md-12 col-sm-12 col-xs-12 drv_uti no-padding">' +
            ' <img src="/Content/images/d_u.png" />' +
            '</div>' +
            '<div class="clear"></div>' +
            '</div>' +
            ' <div class="clear"></div>' +
            '</div>' +
            '</div >';
        $(".drivers_item").append(data);
    });

    $.each(dataCars1, function (key, value) {
        data1 =
            ' <div>' +
            ' <div class="col-lg-12 col-sm-12">' +
            '<div class="trips_icon col-md-12">' +
            '<div class="col-md-3 col-sm-3 col-xs-3">' +
            ' <img src="/Content/images/car_icon.png" width="45" class="trips_box_icon" />' +
            ' </div>' +
            '<div class="col-md-9 col-sm-9 col-xs-9">' +
        '<h4 class="trips_box_title">' + dataCars1[key].VehicleNum + '' +
        '<p class="DateRange">(' + $('#fromDate').val() + '    ' + $('#toDate').val() + ')</p>' +
            '</div>' +
            '<div class="clear"></div>' +
            '<div class="col-md-6 col-sm-6 col-xs-6 driver_info_box_left">' +
            ' <p class="done_trpis">Booked</p>' +
            '<p class="numb_done_trp">' + dataCars1[key].vehicleTotalHourtripAct + '</p>' +
            '</div>' +
            '<div class="col-md-6 col-sm-6 col-xs-6 driver_info_box_right">' +
            '<p class="done_trpis">Free</p>' +
            ' <p class="numb_done_trp_def">' + dataCars1[key].freeTime + '</p>' +
            ' </div>' +
            ' <div class="clear"></div>' +
            ' <div class="col-md-12 col-sm-12 col-xs-12 drv_uti no-padding">' +
            ' <img src="/Content/images/v.png" />' +
            '</div>' +
            '<div class="clear"></div>' +
            '</div>' +
            ' <div class="clear"></div>' +
            '</div>' +
            '</div >';
        $(".vec_item").append(data1);
    });

    $('.drivers_item').slick('unslick').slick('reinit');
    $('.vec_item').slick('unslick').slick('reinit');
    sliderInit('.drivers_item');
    sliderInit('.vec_item');
}

function sliderInit(slidername) {
    $(slidername).slick({
        dots: true,
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: false,
                    arrows: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true
                }
            }
        ]
    });



}

function initSlider(name) {
    $(name).slick({
        dots: true,
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: false,
                    arrows: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true
                }
            }
        ]
    });

};
function UnassignedBookings() {
    var data = {};
    data.paraDateTime = $("#fromDate").val();
    data.paraToTime = $("#toDate").val();
    data.paraDriverId = $('#inputNameDriver').selectivity('val');
    data.paraClientId = $('#inputNameClient').selectivity('val');
    data.paraVehicaleId = $('#inputNameVehicle').selectivity('val');
    //data.driverId = data.driverId > 0 ? data.driverId : null;
    //data.paraClientId = data.clientId > 0 ? data.clientId : null;
    //data.vehicleId = data.vehicleId > 0 ? data.vehicleId : null;
    var aDataAsString = JSON.stringify(data);
   
    var url = baseURL + 'UnassignedBooks/index?paraObject=' + aDataAsString;
    //    + ";paraToTime=" + $("#toDate").val()
    //    + ";paraClientId=" + $('#inputNameClient').selectivity('data')
    //    + ";paraDriverId=" + $('#inputNameDriver').selectivity('data')
    //    + ";paraVehicaleId=" + $('#inputNameVehicle').selectivity('data')
    location.href = url;
    
};


$(document).on("keydown", function (e) {
    if (e.which === 8) {
        e.preventDefault();
    }
});
function closeText() {
    $(".selectivity-single-selected-item-remove").click(function () {
        getDashboardInfo();
    })
}
$(document).unbind('keydown').bind('keydown', function (event) {
    if (event.keyCode === 8) {
        event.preventDefault();
        console.log("aaa");
        var doPrevent = true;
        var types = ["text", "password", "file", "search","div", "email", "number", "date", "color", "datetime", "datetime-local", "month", "range", "search", "tel", "time", "url", "week"];
        var d = $(event.srcElement || event.target);
        var disabled = d.prop("readonly") || d.prop("disabled");
        if (!disabled) {
            if (d[0].isContentEditable) {
                doPrevent = false;
            } else if (d.is("input")) {
                var type = d.attr("type");
                if (type) {
                    type = type.toLowerCase();
                }
                if (types.indexOf(type) > -1) {
                    doPrevent = false;
                }
            } else if (d.is("textarea")) {
                doPrevent = false;
            }
        }
        if (doPrevent) {
            event.preventDefault();
            return false;
        }
    }
});

function restBoxes() {
    $("#fromDate").val("");
    $("#toDate").val("");
    $("#inputNameVehicle").selectivity('clear');
    $("#inputNameClient").selectivity('clear');
    $("#inputNameDriver").selectivity('clear');
}
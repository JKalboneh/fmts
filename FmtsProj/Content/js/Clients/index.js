﻿
var pageWidth = $("#gridContent").parent().width() - 102;

var baseURL = window.location.protocol + "//" + window.location.host + "/"

$("#jsGrid").jsGrid({
    width: "100%",
    height: "auto",
    inserting: false,
    editing: false,
    sorting: true,
    paging: true,
    filtering: true,
    autoload: true,
    pageLoading: true,

    pagerContainer: null,
    pageIndex: 1,
    pageSize: 20,
    pageButtonCount: 15,
    pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
    pagePrevText: "Prev",
    pageNextText: "Next",
    pageFirstText: "First",
    pageLastText: "Last",
    pageNavigatorNextText: "...",
    pageNavigatorPrevText: "...",

    controller: {
        loadData: function (filter) {
            var d = $.Deferred();
            $.ajax({
                type: "GET",
                url: baseURL + "Clients/FindClientsGrid",
                data: filter,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function (response) {
                d.resolve({
                    data: response.data,
                    itemsCount: response.itemsCount
                });
            });

            return d.promise();
        }
        //,
        //updateItem: function (item) {
        //    var d = $.Deferred();
        //    //Vehicle
        //    var ctlVehicle = $("#selectivity_vehicle_" + item.DriverID);
        //    var valVehicle = ctlVehicle.selectivity('value');
        //    var strVehicles = valVehicle.join(",");
        //    //Tag
        //    var ctlTag = $("#selectivity_tag_" + item.DriverID);
        //    var valTag = ctlTag.selectivity('value');
        //    var strTags = valTag.join(",");
        //    //

        //    $.ajax({
        //        type: "GET",
        //        dataType: "json",
        //        url: baseURL + "/Drivers/UpdateDriver",
        //        data: { strVehicles: strVehicles, strTags: strTags, FullName: item.FullName, PersonalNumber: item.PersonalNumber, DriverID: item.DriverID }
        //    }).done(function (response) {
        //        d.resolve(response.driver);
        //    });
        //    return d.promise();
        //}
    },
    fields: [
        { name: "ClientName", type: "text", title: "Full Name", editing: true, width: (pageWidth * (30 / 102)) },
        { name: "Adress", type: "text", title: "Address", editing: true, width: (pageWidth * (30 / 102)) },
        { name: "PhoneNumaber", type: "text", title: "Phone Number", editing: true, width: (pageWidth * (20 / 102)) },
        {
            itemTemplate: function (value, item) {
                var strItem = JSON.stringify(item);
                var str;
                if (haspermissiontoEdit) {
                    str = '<a href ="/Clients/Edit?id=' + item.ClientID + '">' +
                        '<button class="btn btn-primary btn-md">Edit</button>' +
                        '</a>'
                        +
                        '  <button class="btn btn-primary btn-md" onclick="deleteItem(' + item.ClientID + ')">Delete</button>';
                }
                return (str);
            }, width: (pageWidth * (20 / 102))
        },
    ]
});

var grid = $("#jsGrid");

function deleteItem(clientid) {
    $("#confirmationDialog").text("Are you sure to delete this client?");
    $("#confirmationDialog").dialog({
        buttons: {
            "Confirm": function () {
                $("#confirmationDialog").dialog("close");
                $.ajax({
                    type: "GET",
                    url: baseURL + "/Clients/DeleteClient",
                    data: { clientId: clientid },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        ShowAlertMessage("The client item was successfully deleted", "success");
                        grid.jsGrid("loadData");
                    },
                    error: function (request, status, error) {
                        ShowAlertMessage("Error, Can't delete this client", "danger");
                    }
                });
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });
    $("#confirmationDialog").dialog("open");
}
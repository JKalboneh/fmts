﻿var pageWidth = $("#gridContent").parent().width() - 102;

$("#jsGrid").jsGrid({
    width: "100%",
    height: "auto",
    inserting: false,
    editing: false,
    sorting: true,
    paging: true,
    filtering: true,
    autoload: true,
    pageLoading: true,

    pagerContainer: null,
    pageIndex: 1,
    pageSize: 20,
    pageButtonCount: 15,
    pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
    pagePrevText: "Prev",
    pageNextText: "Next",
    pageFirstText: "First",
    pageLastText: "Last",
    pageNavigatorNextText: "...",
    pageNavigatorPrevText: "...",

    controller: {
        loadData: function(filter) {
            var d = $.Deferred();

            $.ajax({
                type: "GET",
                url: baseURL + "/Departments/FindDepartmentsGrid",
                data: filter,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function(response) {
                d.resolve({
                    data: response.data,
                    itemsCount: response.itemsCount
                });
            });

            return d.promise();
        }
    },
    fields: [
        { name: "Name", type: "text", title: "Name", editing: true, width: (pageWidth * (30 / 102)) },
        { name: "CustomerName", type: "text", title: "Customer Name", editing: true, width: (pageWidth * (50 / 102)) },
        {
            itemTemplate: function(value, item) {
                var strItem = JSON.stringify(item);

                if (haspermissiontoEdit) {
                    return (
                        '<a href ="/Departments/Edit?id=' + item.DepartmentID + '">' +
                        '<button class="btn btn-primary btn-md">Edit</button>' +
                        '</a>'
                        +
                        '  <button class="btn btn-primary btn-md" onclick="deleteItem(' + item.DepartmentID + ')">Delete</button>'
                    );
                } else
                {
                    return ('');
                }
            }, width: (pageWidth * (20 / 102))
        },
    ]
});

var grid = $("#jsGrid");

function deleteItem(DepartmentId) {
    $("#confirmationDialog").text("Are you sure to delete this department?");
    $("#confirmationDialog").dialog({
        buttons: {
            "Confirm": function() {
                $("#confirmationDialog").dialog("close");
                $.ajax({
                    type: "GET",
                    url: baseURL + "/Departments/DeleteDepartment",
                    data: { DepartmentId: DepartmentId },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data, text) {
                        ShowAlertMessage("The separtment item was successfully deleted", "success");
                        grid.jsGrid("loadData");
                    },
                    error: function(request, status, error) {
                        ShowAlertMessage("Error, Can't delete this department", "danger");
                    }
                });
            },
            "Cancel": function() {
                $(this).dialog("close");
            }
        }
    });
    $("#confirmationDialog").dialog("open");
}





﻿var pageWidth = $("#gridContent").parent().width() - 102;

$("#jsGrid").jsGrid({
    width: "100%",
    height: "auto",
    inserting: false,
    editing: false,
    sorting: true,
    paging: true,
    filtering: true,
    autoload: true,
    pageLoading: true,

    pagerContainer: null,
    pageIndex: 1,
    pageSize: 20,
    pageButtonCount: 15,
    pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
    pagePrevText: "Prev",
    pageNextText: "Next",
    pageFirstText: "First",
    pageLastText: "Last",
    pageNavigatorNextText: "...",
    pageNavigatorPrevText: "...",

    controller: {
        loadData: function(filter) {
            var d = $.Deferred();

            $.ajax({
                type: "GET",
                url: baseURL + "/Locations/FindLocationsGrid",
                data: filter,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function(response) {
                d.resolve({
                    data: response.data,
                    itemsCount: response.itemsCount
                });
            });

            return d.promise();
        }
        //,
        //updateItem: function(item) {
        //    var d = $.Deferred();
        //    $.ajax({
        //        type: "GET",
        //        dataType: "json",
        //        url: baseURL + "/Locations/UpdateLocation",
        //        data: { strVehicles: strVehicles, strTags: strTags, FullName: item.FullName, PersonalNumber: item.PersonalNumber, DriverID: item.DriverID }
        //    }).done(function(response) {
        //        d.resolve(response.driver);
        //    });
        //    return d.promise();
        //},
    },
    fields: [
        { name: "Number", type: "number", title: "Number", editing: true, width: (pageWidth * (10 / 102)) },
        { name: "Name", type: "text", title: "Location Name", editing: true, width: (pageWidth * (20 / 102)) },
        { name: "Description", type: "text", title: "Description", editing: true, width: (pageWidth * (25 / 102)) },
        { name: "GoogleName", type: "text", title: "Name in Google", editing: true, width: (pageWidth * (20 / 102)) },
        { name: "GPS_Latitude", type: "number", title: "GPS Latitude", editing: true, width: (pageWidth * (5 / 102)) },
        { name: "GPS_Longitude", type: "number", title: "GPS Longitude", editing: true, width: (pageWidth * (5 / 102)) },
        {
            itemTemplate: function (value, item) {
                var strItem = JSON.stringify(item);
                var str;
                if (haspermissiontoEdit) {
                    str = '<a href ="/Locations/Edit?id=' + item.LocationID + '">' +
                        '<button class="btn btn-primary btn-md">Edit</button>' +
                        '</a>'
                        +
                        '  <button class="btn btn-primary btn-md" onclick="deleteItem(' + item.LocationID + ')">Delete</button>';
                }
                return (str);
            }, width: (pageWidth * (20 / 102))
        },
    ]
});

var grid = $("#jsGrid");

function deleteItem(locationId) {
    $("#confirmationDialog").text("Are you sure to delete this location?");
    $("#confirmationDialog").dialog({
        buttons: {
            "Confirm": function() {
                $("#confirmationDialog").dialog("close");
                $.ajax({
                    type: "GET",
                    url: baseURL + "/Locations/DeleteLocation",
                    data: { locationId: locationId },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data, text) {
                        ShowAlertMessage("The location item was successfully deleted", "success");
                        grid.jsGrid("loadData");
                    },
                    error: function(request, status, error) {
                        ShowAlertMessage("Error, Can't delete this location", "danger");
                    }
                });
            },
            "Cancel": function() {
                $(this).dialog("close");
            }
        }
    });
    $("#confirmationDialog").dialog("open");
}





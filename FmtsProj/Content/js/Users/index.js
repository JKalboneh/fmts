﻿var pageWidth = $("#gridContent").parent().width() - 102;

    $("#jsGrid").jsGrid({
        width: "100%",
        height: "auto",
        inserting: false,
        editing: false,
        sorting: true,
        paging: true,
        filtering: true,
        autoload: true,
        pageLoading: true,

        pagerContainer: null,
        pageIndex: 1,
        pageSize: 20,
        pageButtonCount: 15,
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
        pagePrevText: "Prev",
        pageNextText: "Next",
        pageFirstText: "First",
        pageLastText: "Last",
        pageNavigatorNextText: "...",
        pageNavigatorPrevText: "...",

        controller: {
            loadData: function (filter) {
                var d = $.Deferred();

                $.ajax({
                    type: "GET",
                    url: baseURL + "/Users/FindUsersGrid",
                    data: filter,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                }).done(function (response) {
                    d.resolve({
                        data: response.data,
                        itemsCount: response.itemsCount
                    });
                });

                return d.promise();
            }
        },
        fields: [
            { name: "FullName", type: "text", title: "Full Name", editing: true, width: (pageWidth * (20 / 102)) },
            { name: "UserName", type: "text", title: "User Name", editing: true, width: (pageWidth * (15 / 102)) },
            { name: "Address", type: "text", title: "Address", editing: true, width: (pageWidth * (30 / 102)) },
            { name: "PhoneNumber", type: "text", title: "Phone Number", editing: true, width: (pageWidth * (20 / 102)) },
            { name: "RoleName", type: "text", title: "Role Name", editing: true, width: (pageWidth * (10 / 102)) },
            {
                itemTemplate: function (value, item) {
                    var strItem = JSON.stringify(item);
                    var str;
                    if (haspermissiontoEdit) {
                        str = '<a href ="/Users/Edit?id=' + item.UserID + '">' +
                            '<button class="btn btn-primary btn-md">Edit</button>' +
                            '</a>'
                            +
                            '  <button class="btn btn-primary btn-md" onclick="deleteItem(' + item.UserID + ')">Delete</button>';
                    }
                    return (str);
                }, width: (pageWidth * (20 / 102))
            },
         ]
    });

    var grid = $("#jsGrid");
    
function deleteItem(userId) {
    $("#confirmationDialog").text("Are you sure to delete this user?");
    $("#confirmationDialog").dialog({
        buttons: {
            "Confirm": function() {
                $("#confirmationDialog").dialog("close");
                $.ajax({
                    type: "GET",
                    url: baseURL + "/Users/DeleteUser",
                    data: { userId: userId },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data, text) {
                        ShowAlertMessage("The user item was successfully deleted", "success");
                        grid.jsGrid("loadData");
                    },
                    error: function(request, status, error) {
                        ShowAlertMessage("Error, Can't delete this user", "danger");
                    }
                });
            },
            "Cancel": function() {
                $(this).dialog("close");
            }
        }
    });
    $("#confirmationDialog").dialog("open");
    }


   


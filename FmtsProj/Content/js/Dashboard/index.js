﻿

var pageWidth = $("#gridContent").parent().width() - 102;
var baseURL = window.location.protocol + "//" + window.location.host + "/"


$("#jsGrid").jsGrid({
    width: "100%",
    height: "auto",
    inserting: false,
    editing: false,
    sorting: true,
    paging: true,
    filtering: false,
    autoload: true,
    pageLoading: true,

    pagerContainer: null,
    pageIndex: 1,
    pageSize: 20,
    pageButtonCount: 15,
    pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
    pagePrevText: "Prev",
    pageNextText: "Next",
    pageFirstText: "First",
    pageLastText: "Last",
    pageNavigatorNextText: "...",
    pageNavigatorPrevText: "...",

    controller: {
        loadData: function (filter) {
            var d = $.Deferred();
            $.ajax({
                type: "GET",
                url: baseURL + "/Dashboard/FindNotifications",
                data: { NewValue: DashboardFilter},
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function (response) {
                d.resolve({
                    data: response.data,
                    itemsCount: response.itemsCount
                });
            });

            return d.promise();
        }
    },
    rowClick: function (args) {
        
        var url = baseURL + "Books/Edit?parBookScheduleId=" + args.item.RecordId;
        //    + ";paraToTime=" + $("#toDate").val()
        //    + ";paraClientId=" + $('#inputNameClient').selectivity('data')
        //    + ";paraDriverId=" + $('#inputNameDriver').selectivity('data')
        //    + ";paraVehicaleId=" + $('#inputNameVehicle').selectivity('data')
        location.href = url;
    },

    fields: [
        { name: "UserName", type: "text", title: "User Name", editing: true, width: (pageWidth * (15 / 102)) },
        { name: "EventDateUTC", type: "text", title: "Event Date UTC", editing: true, width: (pageWidth * (25 / 102)) },
        { name: "EventType", type: "text", title: "Event Type", editing: true, width: (pageWidth * (10 / 102)) },
        { name: "TableName", type: "text", title: "Table Name", editing: true, width: (pageWidth * (25 / 102)) },
        { name: "ColumnName", type: "text", title: "Column Name", editing: true, width: (pageWidth * (25 / 102)) },
        { name: "OriginalValue", type: "text", title: "Original Value", editing: true, width: (pageWidth * (25 / 102)) },
        { name: "NewValue", type: "text", title: "New Value", editing: true, width: (pageWidth * (25 / 102)) }
    ]
});


var grid = $("#jsGrid");

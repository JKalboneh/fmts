﻿

var pageWidth = $("#gridContent").parent().width() - 102;
var baseURL = window.location.protocol + "//" + window.location.host + "/"

var vehicleCtrl = function (config) {
    jsGrid.Field.call(this, config);
};
vehicleCtrl.prototype = new jsGrid.Field({
    itemTemplate: function (value, item) {
        var html = '';
        if (value !== null && value.length) {
            value.forEach(function (entry) {
                html = html + (
                    '<span class="selectivity-multiple-selected-item" data-item-id="' + entry.ID + '"><a class="selectivity-multiple-selected-item-remove">'
                    + '' + entry.VehicleNum + '</a ></span>'
                );
                //item.strVehicleIds = item.strVehicleIds + ',' + entry.ID;
            });
        }
        return html;
    },
    editTemplate: function (value, item) {
        var values = [];
        if (value !== null && value.length) {
            value.forEach(function (entry) {
                var val = {};
                val.id = entry.ID;
                val.text = entry.VehicleNum;
                values.push(val);
                //item.strVehicleIds = item.strVehicleIds + ',' + entry.ID;
            });
        }
        debugger;
//        var $result = initVehicleNum($("<div class='selectivity-input' width='100%' id='selectivity_vehicle_" + item.DriverID + "'>"),
        var $result = initVehicleNum($("<div class='selectivity-input' width='100%' id='selectivity_vehicle_" + item.DriverID + "'>"),
            function (term, offset) {
                // GitHub uses 1-based pages with 30 results, by default
                return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15 };
            }, true, null, values);

        //var $result = $("<div class='selectivity-input' width='100%' id='selectivity_vehicle_" + item.DriverID + "'>").selectivity({
        //    allowClear: true,
        //    multiple: true,
        //    data: values,
        //    ajax: {
        //        url: baseURL + "Vehicles/FindVehicles",
        //        minimumInputLength: 0,
        //        quietMillis: 250,
        //        params: function (term, offset) {
        //            // GitHub uses 1-based pages with 30 results, by default
        //            return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15 };
        //        },
        //        fetch: function (url, init, queryOptions) {
        //            return $.ajax(url).then(function (data) {
        //                return {
        //                    results: $.map(data.items, function (item) {
        //                        return {
        //                            id: item.id,
        //                            text: item.text
        //                        };
        //                    }),
        //                    more: (data.count > queryOptions.offset + data.items.length)
        //                };
        //            });
        //        }
        //    },
        //    placeholder: 'Search for a vehicles',
        //    //templates: {
        //    //    resultItem: function (item) {
        //    //        return (
        //    //            '<div class="selectivity-result-item" data-item-id="' + item.id + '">' +
        //    //            '<b>' + item.text + '</b><br>' +
        //    //            '</div>'
        //    //        );
        //    //    }
        //    //}
        //}


        //);

        return $result;
    },
    editValue: function () {
        return this;
    }
});
jsGrid.fields.vehicleType = vehicleCtrl;

var tagsCtrl = function (config) {
    jsGrid.Field.call(this, config);
};
tagsCtrl.prototype = new jsGrid.Field({
    itemTemplate: function (value, item) {
        var html = '';
        if (value !== null && value.length) {
            value.forEach(function (entry) {
                html = html + (
                    '<span class="selectivity-multiple-selected-item" data-item-id="' + entry.TagID + '"><a class="selectivity-multiple-selected-item-remove">'
                    + '' + entry.Name + '</a ></span>'
                );
            });
        }
        return html;
    },
    editTemplate: function (value, item) {
        var values = [];
        if (value !== null && value.length) {
            value.forEach(function (entry) {
                var val = {};
                val.id = entry.TagID;
                val.text = entry.Name;
                values.push(val);
            });
        }
        var $result = $("<div class='selectivity-input' width='100%' id='selectivity_tag_" + item.DriverID + "'>").selectivity({
            allowClear: true,
            multiple: true,
            data: values,
            ajax: {
                url: baseURL + "Tags/FindTags",
                minimumInputLength: 0,
                quietMillis: 250,
                params: function (term, offset) {
                    // GitHub uses 1-based pages with 30 results, by default
                    return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15 };
                },
                fetch: function (url, init, queryOptions) {
                    return $.ajax(url).then(function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    id: item.TagID,
                                    text: item.Name
                                };
                            }),
                            more: (data.count > queryOptions.offset + data.items.length)
                        };
                    });
                }
            },
            placeholder: 'Search for a tags'
        }
        );

        return $result;
    },
    editValue: function () {
        return this;
    }
});
jsGrid.fields.tagsType = tagsCtrl;


$("#jsGrid").jsGrid({
    width: "100%",
    height: "auto",
    inserting: false,
    editing: haspermissiontoEdit,
    sorting: true,
    paging: true,
    filtering: true,
    autoload: true,
    pageLoading: true,

    pagerContainer: null,
    pageIndex: 1,
    pageSize: 20,
    pageButtonCount: 15,
    pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
    pagePrevText: "Prev",
    pageNextText: "Next",
    pageFirstText: "First",
    pageLastText: "Last",
    pageNavigatorNextText: "...",
    pageNavigatorPrevText: "...",

    controller: {
        loadData: function (filter) {
            var d = $.Deferred();
            $.ajax({
                type: "GET",
                url: baseURL + "/Drivers/FindDriversGrid",
                data: filter,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function (response) {
                d.resolve({
                    data: response.data,
                    itemsCount: response.itemsCount
                });
            });

            return d.promise();
        },
        updateItem: function (item) {
            var d = $.Deferred();
            //Vehicle
            var ctlVehicle = $("#selectivity_vehicle_" + item.DriverID);
            var valVehicle = ctlVehicle.selectivity('value');
            var strVehicles = valVehicle.join(",");
            //Tag
            var ctlTag = $("#selectivity_tag_" + item.DriverID);
            var valTag = ctlTag.selectivity('value');
            var strTags = valTag.join(",");
            //

            $.ajax({
                type: "GET",
                dataType: "json",
                url: baseURL + "/Drivers/UpdateDriver",
                data: { strVehicles: strVehicles, strTags: strTags, FullName: item.FullName, PersonalNumber: item.PersonalNumber, DriverID: item.DriverID }
            }).done(function (response) {
                d.resolve(response.driver);
            });
            return d.promise();
        }
    },
    fields: [
        { name: "FullName", type: "text", title: "Full Name", editing: true, width: (pageWidth * (15 / 102)) },
        { name: "PersonalNumber", type: "text", title: "Personal Number", editing: true, width: (pageWidth * (10 / 102)) },
        { name: "strJoiningDate", type: "date", title: "Joining Date", editing: true, width: (pageWidth * (10 / 102)) },
        { name: "VehicleAssign", type: "vehicleType", title: "Vehicles", editing: true, width: (pageWidth * (25 / 102)) },
        { name: "Tags", type: "tagsType", title: "Tags", editing: true, width: (pageWidth * (25 / 102)) },
        { type: "control", editButton: true, modeSwitchButton: true, deleteButton: false, width: (pageWidth * (5 / 102)) },
        {
            itemTemplate: function (value, item) {
                var strItem = JSON.stringify(item);
                var str;
                if (haspermissiontoEdit) {
                    str = '<a href ="/Drivers/Edit?id=' + item.DriverID + '">' +
                        '<button class="btn btn-primary btn-md btn-edit">Edit</button>' +
                        '</a>'
                        +
                        '  <button class="btn btn-primary btn-md" onclick="deleteItem(' + item.DriverID + ')">Delete</button>';
                }
                return (
                   str
                );
            }, width: (pageWidth * (10 / 102))
        }
    ]
});


var grid = $("#jsGrid");

function deleteItem(driverId) {
    $("#confirmationDialog").text("Are you sure to delete this driver?");
    $("#confirmationDialog").dialog({
        buttons: {
            "Confirm": function () {
                $("#confirmationDialog").dialog("close");
                $.ajax({
                    type: "GET",
                    url: baseURL + "/Drivers/DeleteDriver",
                    data: { driverId: driverId },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        ShowAlertMessage("The driver item was successfully deleted", "success");
                        grid.jsGrid("loadData");
                    },
                    error: function (request, status, error) {
                        ShowAlertMessage("Error, Can't delete this driver", "danger");
                    }
                });
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });
    $("#confirmationDialog").dialog("open");
}

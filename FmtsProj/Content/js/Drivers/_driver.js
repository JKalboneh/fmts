﻿$(document).ready(function () {
    $('#EmaritesIdExpairy').flatpickr({
        enableTime: true,
        dateFormat: "d/m/Y",
        "plugins": [new confirmDatePlugin({})],
        onChange: function (dateobj, datestr) {
            //getDashboardInfo();
        }
    });

    $('#JoiningDate').flatpickr({
        enableTime: true,
        dateFormat: "d/m/Y",
        "plugins": [new confirmDatePlugin({})],
        onChange: function (dateobj, datestr) {
            //getDashboardInfo();
        }
    });
})
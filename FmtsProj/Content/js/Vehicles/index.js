﻿var pageWidth = $("#gridContent").parent().width() - 102;


var driverCtrl = function (config) {
    jsGrid.Field.call(this, config);
};
driverCtrl.prototype = new jsGrid.Field({

    itemTemplate: function (value, item) {
        var html = '';
        if (value !== null && value.length) {
            value.forEach(function (entry) {
                html = html + (
                    '<span class="selectivity-multiple-selected-item" data-item-id="' + entry.DriverID + '"><a class="selectivity-multiple-selected-item-remove">'
                    + '' + entry.FullName + '</a ></span>'
                );
            });
        }
        return html;
    }
    ,
    editTemplate: function (value, item) {
        
        var values = [];
        if (value !== null && value.length) {
            value.forEach(function (entry) {
                var val = {};
                val.id = entry.DriverID;
                val.text = entry.FullName;
                values.push(val);
            });
        }
        
        var $result = initDriverName($("<div class='selectivity-input' width='100%' id='selectivity_" + item.ID + "'>"), function (term, offset) {
            // GitHub uses 1-based pages with 30 results, by default
            return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15};
        }, true, null, values);

        return $result;
    },
    editValue: function () {
        return this;
    }
});
jsGrid.fields.driverType = driverCtrl;

$("#jsGrid").jsGrid({

    width: "100%",
    height: "auto",

    inserting: false,
    editing: haspermissiontoEdit,
    sorting: true,
    paging: true,
    filtering: true,
    autoload: true,
    pageLoading: true,

    pagerContainer: null,
    pageIndex: 1,
    pageSize: 20,
    pageButtonCount: 15,
    pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
    pagePrevText: "Prev",
    pageNextText: "Next",
    pageFirstText: "First",
    pageLastText: "Last",
    pageNavigatorNextText: "...",
    pageNavigatorPrevText: "...",

    controller: {
        loadData: function (filter) {
            var d = $.Deferred();
            $.ajax({
                type: "GET",
                url: baseURL + "/Vehicles/FindVehiclesGrid",
                data: filter,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function (response) {
                d.resolve({
                    data: response.data,
                    itemsCount: response.itemsCount
                });
            });

            return d.promise();
        },
        updateItem: function (item) {
            var d = $.Deferred();
            var ctl = $("#selectivity_" + item.ID);
            var val = ctl.selectivity('value');
            var strDrivers = val.join(",");
            $.ajax({
                type: "GET",
                dataType: "json",
                url: baseURL + "/Vehicles/UpdateVehicleDrivers",
                data: { drivers: strDrivers, vehicleId: item.ID }
            }).done(function (response) {
                d.resolve(response.vehicle);
            });
            return d.promise();
        }
    },
    fields: [
        { name: "VehicleNum", type: "text", title: "Number", editing: false, width: (pageWidth * (10 / 102)) },
        { name: "SeatCount", type: "number", title: "Seat Count", editing: false, width: (pageWidth * (3 / 102)) },
        { name: "VehicleColor", type: "text", title: "Color", editing: false, width: (pageWidth * (3 / 102)) },
        { name: "VehicleStatus", type: "text", title: "Status", editing: false, width: (pageWidth * (3 / 102)) },
        { name: "MakeYear", type: "text", title: "Year", editing: false, width: (pageWidth * (4 / 102)) },
        { name: "ChasisNumber", type: "text", title: "ChasisNumber", editing: false, width: (pageWidth * (12 / 102)) },
        { name: "Make", type: "text", title: "Make", editing: false, width: (pageWidth * (12 / 102)) },
        { name: "OwnerName", type: "text", title: "Owner Name", editing: false, width: (pageWidth * (8 / 102)) },
        { name: "DriverAssign", type: "driverType", title: "Drivers", editing: true, width: (pageWidth * (20 / 102)) },
        { type: "control", editButton: true, modeSwitchButton: false, deleteButton: false, width: (pageWidth * (5 / 102)) },
        {
            itemTemplate: function (value, item) {
                var strItem = JSON.stringify(item);
                var str;
                if (haspermissiontoEdit)
                    str = '<a href ="/Vehicles/Edit?id=' + item.ID + '">' +
                        '<button class="btn btn-primary btn-md">Edit</button>' +
                        '</a>'
                        +
                        '  <button class="btn btn-primary btn-md" onclick="deleteItem(' + item.ID + ')">Delete</button>';
                return (
                    str
                );
            }, width: (pageWidth * (10 / 102))
        }

    ]
});

var grid = $("#jsGrid");

function deleteItem(vehicleId) {
    $("#confirmationDialog").text("Are you sure to delete this vehicle?");
    $("#confirmationDialog").dialog({
        buttons: {
            "Confirm": function () {
                $("#confirmationDialog").dialog("close");
                $.ajax({
                    type: "GET",
                    url: baseURL + "/Vehicles/DeleteVehicle",
                    data: { vehicleId: vehicleId },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        ShowAlertMessage("The vehicle item was successfully deleted", "success");
                        grid.jsGrid("loadData");
                    },
                    error: function (request, status, error) {
                        ShowAlertMessage("Error, Can't delete this vehicle", "danger");
                    }
                });
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });
    $("#confirmationDialog").dialog("open");
}

﻿var baseURL = window.location.protocol + "//" + window.location.host + "/";

function initVehicleNum(elem,
    vehicalFunctionParamters, isMultiple, ValidDateFunction, paravalues)
{
    var $result = elem.selectivity({
        allowClear: true,
        multiple: isMultiple,
        data: paravalues,
        ajax: {
            url: baseURL + "Vehicles/FindVehicles",
            minimumInputLength: 0,
            quietMillis: 250,
            params: vehicalFunctionParamters,
            fetch: function (url, init, queryOptions) {
                return $.ajax(url).then(function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                id: item.id,
                                text: item.text,
                                SeatCount : item.SeatCount,
                                Make : item.Make,
                                BusyInSameTrip : item.BusyInSameTrip,
                                DriverTagsMissmatch: item.DriverTagsMissmatch,
                                VehicalOutTime : item.VehicalOutTime
                            };
                        }),
                        more: (data.count > queryOptions.offset + data.items.length)
                    };
                });
            }
        },
        placeholder: 'Vehicles',
        templates: {
            resultItem: function (item) {
                var color = 'green';
                if (item.BusyInSameTrip != "0")//InTimeColorCode
                            color = 'red';
                if (item.DriverTagsMissmatch != "0")//VehicalTagsList
                            color = 'lightgray';
                if (item.VehicalOutTime != "0")//DriverinVactionColor
                            color = 'yellow';
                return (
                    
                    '<div class="selectivity-result-item" data-item-id="' + item.id + '">' +
                    '<b style=\"color:' + color + '\">' + item.text + '|' + '</b><br>' +
                    '</div>'
                );
            }
        }
    });

    elem.on("selectivity-selecting", function (e) {
        if (e.item != undefined) {
            if (e.item.BusyInSameTrip != '0') {
                ShowAlertMessage("Error, The current Vehicale busy in another Trip Trip id:#", "danger");
                e.preventDefault();
            } else if (e.item.DriverTagsMissmatch != '0') {
                ShowAlertMessage("warning, The selected Vehicale # does not caver all Tags:#", "warning");
            } else if (e.item.VehicalOutTime != '0') {
                ShowAlertMessage("warning, The selected Driver # in the maintenance time:#", "warning");
            }
        }
    });
    if (ValidDateFunction != undefined)
        elem.on("selectivity-opening", ValidDateFunction);
    return $result;
}

function GetValue(para)
{
    if ($(para).selectivity('val') != null && $(para).selectivity('val') != undefined)
        return $(para).selectivity('val')
    else
        return "";

}
function initDriverName(elem,
    driverFunctionParamters, isMultiple, ValidDateFunction, paraValues) {
    var $result = elem.selectivity({
        allowClear: true,
        multiple: isMultiple,
        data: paraValues,
        ajax: {
            url: baseURL + "Drivers/FindDrivers",
            minimumInputLength: 0,
            quietMillis: 250,
            params: driverFunctionParamters,
            fetch: function (url, init, queryOptions) {
                return $.ajax(url).then(function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                id: item.id,
                                text: item.text,
                                PersonalNumber: item.PersonalNumber,
                                OverAllColor: item.OverAllColor
                            };
                        }),
                        more: (data.count > queryOptions.offset + data.items.length)
                    };
                });
            }
        },
        placeholder: 'Driver',
        templates: {
            resultItem: function (item) {
                var color = 'green';
                item.ItemType = 3;
                if (item.OverAllColor != undefined) {

                    if (item.OverAllColor[1] != undefined) {
                        if (item.OverAllColor[1].Value != "0")//InTimeColorCode
                        {
                            color = 'red';
                            item.ItemType = 1;
                        }
                    }
                    if (color == "green" && item.OverAllColor[2] != undefined) {
                        if (item.OverAllColor[2].Value != "0")//VehicalTagsList
                        {
                            color = 'lightgray';
                            item.ItemType = 2;
                        }
                    }
                    if (color == "green" && item.OverAllColor[0] != undefined) {
                        if (item.OverAllColor[0].Value != "0")//DriverinVactionColor
                        {
                            color = 'yellow;';
                            item.ItemType = 0;
                        }
                    }
                }
                return (
                    '<div class="selectivity-result-item" data-item-id="' + item.id + '">' +
                    '<b style=\"color:' + color + '\">' + '<tbody class="dataNot"><tr><td>' + item.text + '</td><td>' + item.PersonalNumber+'</td><td>111111</td><td>55555</td></tr></tbody>' + '</b>' +
                    '</div>'
                );
            }
        }
    });
    elem.on("selectivity-selecting", function (e) {
        if (e.item != undefined)
        {
            if (e.item.ItemType == '1')
            {
                //alert("");
                ShowAlertMessage("Error, The current Driver busy in another Trip Trip id:#", "danger");
                e.preventDefault();
            } else if (e.item.ItemType == '2')
            {
                ShowAlertMessage("warning, The selected Driver # does not caver all Tags:#", "warning");
            } else if (e.item.ItemType == '0') {
                ShowAlertMessage("warning, The selected Driver # in the vacation in thats time:#", "warning");
            }
        }
    });
    if (ValidDateFunction != undefined)
        elem.on("selectivity-opening", ValidDateFunction);
    return $result; 
};
function ShowAlertMessage(text, type) {
    var devId = "";
    if (type == "success") {
        $(".successMessage").text(text)
        devId = ".alert.alert-success"
    }
    if (type == "danger") {
        $(".dangerMessage").text(text)
        devId = ".alert.alert-danger"
    }
    if (type == "info") {
        $(".infoMessage").text(text)
        devId = ".alert.alert-info"
    }
    if (type == "warning") {
        $(".warningMessage").text(text)
        devId = ".alert.alert-warning"
    }

    $(devId).show(1000).delay(5000).queue(function(n) {
        $(this).hide(1000); n();
    });

}

function GetIdFromselectivity(elem)
{
    return elem.selectivity('data') == null ? 0 : elem.selectivity('data').id;
}
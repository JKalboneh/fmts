﻿var pageWidth = $("#gridContent").parent().width() - 102;

$("#jsGrid").jsGrid({
    width: "100%",
    height: "auto",
    inserting: false,
    editing: false,
    sorting: true,
    paging: true,
    filtering: true,
    autoload: true,
    pageLoading: true,

    pagerContainer: null,
    pageIndex: 1,
    pageSize: 20,
    pageButtonCount: 15,
    pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
    pagePrevText: "Prev",
    pageNextText: "Next",
    pageFirstText: "First",
    pageLastText: "Last",
    pageNavigatorNextText: "...",
    pageNavigatorPrevText: "...",

    controller: {
        loadData: function(filter) {
            var d = $.Deferred();

            $.ajax({
                type: "GET",
                url: baseURL + "/Tags/FindTagsGrid",
                data: filter,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function(response) {
                d.resolve({
                    data: response.data,
                    itemsCount: response.itemsCount
                });
            });

            return d.promise();
        }
        
    },
    fields: [
        { name: "Name", type: "text", title: "Tag Name", editing: true, width: (pageWidth * (70 / 102)) },
        {
            itemTemplate: function (value, item) {
                var strItem = JSON.stringify(item);
                var str;
                if (haspermissiontoEdit) {
                    str = '<a href ="/Tags/Edit?id=' + item.TagID + '">' +
                        '<button class="btn btn-primary btn-md">Edit</button>' +
                        '</a>'
                        +
                        '  <button class="btn btn-primary btn-md" onclick="deleteItem(' + item.TagID + ')">Delete</button>';
                }
                return (str);
            }, width: (pageWidth * (30 / 102))
        },
    ]
});

var grid = $("#jsGrid");

function deleteItem(tagId) {
    $("#confirmationDialog").text("Are you sure to delete this tag?");
    $("#confirmationDialog").dialog({
        buttons: {
            "Confirm": function() {
                $("#confirmationDialog").dialog("close");
                $.ajax({
                    type: "GET",
                    url: baseURL + "/Tags/DeleteTag",
                    data: { tagId: tagId },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data, text) {
                        ShowAlertMessage("The tag item was successfully deleted", "success");
                        grid.jsGrid("loadData");
                    },
                    error: function(request, status, error) {
                        ShowAlertMessage("Error, Can't delete this tag", "danger");
                    }
                });
            },
            "Cancel": function() {
                $(this).dialog("close");
            }
        }
    });
    $("#confirmationDialog").dialog("open");
}





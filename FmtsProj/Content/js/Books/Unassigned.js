﻿

var pageWidth = $("#gridContent").parent().width() - 102;
var baseURL = window.location.protocol + "//" + window.location.host + "/"


$("#jsGrid").jsGrid({
    width: "100%",
    height: "auto",
    inserting: false,
    editing: false,
    sorting: true,
    paging: true,
    filtering: false,
    autoload: true,
    pageLoading: true,

    pagerContainer: null,
    pageIndex: 1,
    pageSize: 20,
    pageButtonCount: 15,
    pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
    pagePrevText: "Prev",
    pageNextText: "Next",
    pageFirstText: "First",
    pageLastText: "Last",
    pageNavigatorNextText: "...",
    pageNavigatorPrevText: "...",

    controller: {
        loadData: function (filter) {
            var objectData = JSON.parse($('#FilterObject').val());
            var d = $.Deferred();
            $.ajax({
                type: "GET",
                url: baseURL + "UnassignedBooks/FindUnassignedBookGrid",
                data: objectData
                ,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function (response) {
                d.resolve({
                    data: response.data,
                    itemsCount: response.itemsCount
                });
            });

            return d.promise();
        },
    },
    rowClick: function (args) {

        var url = baseURL + "Books/Edit?parBookScheduleId=" + args.item.BookScheduleID;
        //    + ";paraToTime=" + $("#toDate").val()
        //    + ";paraClientId=" + $('#inputNameClient').selectivity('data')
        //    + ";paraDriverId=" + $('#inputNameDriver').selectivity('data')
        //    + ";paraVehicaleId=" + $('#inputNameVehicle').selectivity('data')
        location.href = url;},
    fields: [
        { name: "BookRef", type: "text", title: "BookRef", editing: true, width: (pageWidth * (15 / 102)) },
        { name: "ClientName", type: "text", title: "Client Name", editing: true, width: (pageWidth * (15 / 102)) },
        { name: "StartDatetime", type: "date", title: "Start Date time", editing: true, width: (pageWidth * (10 / 102)) },
        { name: "StartLocation", type: "text", title: "Start Location", editing: true, width: (pageWidth * (10 / 102)) },
        { name: "EndDate", type: "date", title: "End Date", editing: true, width: (pageWidth * (25 / 102)) },
        {
            width: (pageWidth * (10 / 102))
        }
    ]
});


var grid = $("#jsGrid");
$(document).ready(function () {

    $('#inputNameClient').selectivity({
        allowClear: false,
        multiple: false,
        placeholder: 'Client',
    });
    $('#inputNameVehicle').selectivity({
        allowClear: false,
        multiple: false,
        placeholder: 'Client',
    });
    $('#inputNameDriver').selectivity({
        allowClear: false,
        multiple: false,
        placeholder: 'Client',
    });
    $("#jsGrid tr").click(function () {
        var tr = $(this)[0];
        var trID = tr.id;
        alert("trID=" + trID);
    });
    //var objectData = JSON.parse($('#FilterObject').val());
    //if (objectData !== null && objectData !== undefined) {
    //    $("#fromdate").val(objectData.paraDateTime);
    //    $("#toDate").val(objectData.paraToTime);
    //    if (objectData.paraClientId !=null)
    //        $("#inputNameClient").selectivity('data', { id: objectData.paraClientId.id, text: objectData.paraClientId.text });//.val();
    //    if (objectData.paraVehicaleId != null)
    //        $("#inputNameVehicle").selectivity('data', { id: objectData.paraVehicaleId.id, text: objectData.paraVehicaleId.text });//.val(objectData.);
    //    if (objectData.paraDriverId!= null)
    //        $("#inputNameDriver").selectivity('data', { id: objectData.paraDriverId.id, text: objectData.paraDriverId.text });//.val(objectData.);.val(objectData.);
    //}

});


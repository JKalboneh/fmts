﻿
var tripData = [];
$("#btnSave").click(function () {
    if ($("#inputNameClient .selectivity-single-select-input").val() == "") {
        $(".error_message").text("Please fill the client field").slideDown();
    }
    else if ($("#bookDate").val() == "") {
        $(".error_message").hide().text("Please fill the Day date field").slideDown();
    }
    else if ($("#inputLocationNm").selectivity('data') == null) {
        $(".error_message").hide().text("Please fill the start location field").slideDown();
    }  

    //else if ($("#inputTillDay").val() == "") {
    //    $(".error_message").hide().text("Please fill the Ending Day field").slideDown();
    //}
    //else if ($("#bookDate").val() >= $("#inputTillDay").val()) {
    //    $(".error_message").hide().text("BookDate should be less than ending date").slideDown();
    //}
    else {
        $(".error_message").slideUp();
        //        DisableControl(true);
        var bookTripsData = {};
        bookTripsData.CustomerID = $('#inputNameCustomer').selectivity('data');
        bookTripsData.DepartmentID = $('#inputNameDepartment').selectivity('data');
        bookTripsData.ClientID = $('#inputNameClient').selectivity('data');
        bookTripsData.TotalCost = $(".total_cost_number").text();
        //  bookTripsData.bookTime = $("#bookHourTime").val();
        //  bookTripsData.bookDateTImeCompine = $("#arrTimeHiddden").val();
        bookTripsData.ContactType = $("#contractType").selectivity('data');
        bookTripsData.StartDatetime = $('#bookDate').val();// $("#bookDate").val();
        bookTripsData.StartLoc = $("#inputLocationNm").selectivity('data');
        bookTripsData.inputTillDay = $("#inputTillDay").val();
        bookTripsData.Rec_Type = $("#RecurringTrip").prop("checked");
        //bookTripsData.RecType = $(".rec_trip").find("li.rec_trip_active").text();
        bookTripsData.RecType = $("#RecurringTrip").prop("checked");
        var daysActive = $(".week_footer ul").find("li.week_day_active").text();
        var daysClassActive = $(".week_footer ul").find("li.week_day_active").attr("id");
        var daysActiveArray = daysActive.split(' ');
        bookTripsData.SchedDays = daysActiveArray;
        if (tripData.length != 0) {
            var lastEndDate = bookTripsData.StartDatetime;
            for (var i = 0; i < tripData.length; i++) {
                if (i == 0) {
                    tripData[0].StartDate = bookTripsData.StartDatetime;
                    lastEndDate = tripData[0].EndDate;
                }
                if (i != 0) {
                    tripData[i].StartDate = lastEndDate;
                    lastEndDate = tripData[i].EndDate;
                }
            }
        }


    }
    bookTripsData.TripList = [];
    bookTripsData.TripList = tripData;
    pushToserverSave(bookTripsData);
    console.log(bookTripsData);
    //var myJSON = JSON.stringify(bookTripsData);
    //localStorage.setItem("data", myJSON);
})

function getTrips() {
    return JSON.parse(tripData.getItem('trips'));
}

function pushToserverSave(bookTripsData) {
    var baseURL = window.location.protocol + "//" + window.location.host + "/"
    $.ajax({
        url: baseURL + "Books/AddBook",
        type: "POST",
        data: JSON.stringify(bookTripsData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#shadooows').show();
        },

        error: function (response) {
            console.log("Error " + response);
            $('#shadooows').hide();
        },
        success: function (result) {
            if (result.IDBookSch == '' || result.IDBookSch == undefined)
            {
                alert('Error in saving the Book Tip ' + result);
                $('#shadooows').hide();
            }
            else {
                
                $("#btnSave").hide();
                $("#btnEditSave").show();

                // window.location.href = baseURL + "Books/AddBook";
                var url = '/Books/Edit?parBookScheduleId=' + result.IDBookSch;
                location.href = url;

            }
        }
    });
}

function pushToserverEdit(bookTripsData, parBookId) {
    var baseURL = window.location.protocol + "//" + window.location.host + "/"
    $.ajax({
        url: baseURL + "Books/Edit?parBookScheduleId=" + parBookId,
        type: "POST",
        data: JSON.stringify(bookTripsData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function (response) {
            alert("Error " + response);
        },
        success: function (result) {
            $("#RefNumlbl").text("Book Reference#" + result.BookRefNum);
            //window.location.href = baseURL + "Books/AddBook";
            //var url = '/Books/index';//+ result;
            //location.href = url;
        }
    });
}
$('#btnParSave').click(function () {

    if ($("#inputDistLocNm").selectivity('data') == null || $("#inputDistLocNm").selectivity('data') == "") {
        $(".error_message_Trip").text("Please fill the destination field").slideDown();
    }
    //else if ($("#inputparVehicleNum").selectivity('data') == null) {
    //    $(".error_message_Trip").hide().text("Please fill the vehicle").slideDown();
    //}
    else if ($("#inputparArriv").val() < $("#bookDate").val()) {
        $(".error_message_Trip").hide().text("Please the arival time must be greater than the day time field ").slideDown();
    }

    else if ($("#inputparArriv").val() <= $("#prevStopDate").val() && $("#prevStopDate").val()!="") {
        $(".error_message_Trip").hide().text("Please the arival time must be greater than the previous stop ").slideDown();
    }

    else if ($("#inputparArriv").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Arival Time field").slideDown();
    }

    else if ($("#SalikFees").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Salik Fees field").slideDown();
    }
    else if ($("#DriverFees").val() == "") {
        $(".error_message_Trip").hide().text("Please fill Driver Fees field").slideDown();
    }
    else if ($("#CarFuelCost").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Car Fuel Cost field").slideDown();
    }
    else if ($("#ParkingFees").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Parking Fees field").slideDown();
    }
    else if ($("#Consumption").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Consumption field").slideDown();
    }
    else if ($("#Other").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Other field").slideDown();
    }

    else {
        $(".error_message_Trip").slideUp();
        $("#inputTillDay").val($("#inputparArriv").val());
        $("#divTripCreate").hide();
        $("#divAddTrip").show();
        var trip = {};
        trip.EndLocid = $("#inputDistLocNm").selectivity('data');
        trip.DriverID = $("#inputparDriver").selectivity('data');
        trip.EndDate = $("#inputparArriv").val();
        trip.VehicleID = $("#inputparVehicleNum").selectivity('data');
        trip.SalikFees = $("#SalikFees").val();
        trip.DriverFees = $("#DriverFees").val();
        trip.CarFuelCost = $("#CarFuelCost").val();
        trip.ParkingFees = $("#ParkingFees").val();
        trip.Consumption = $("#Consumption").val();
        trip.Other = $("#Other").val();
        trip.driverTrip = $("#driverTrip").prop("checked");
        trip.overwriteStartLoc = $("#overwriteStartLoc").selectivity('data');
        tripData.push(trip);
        //collectTrips(trip);
        setTimeout(collectTrips, 100);
        addTripBoxs();
    }



});


$("#btnEditSave").click(function () {

    var url = baseURL + "Books/Edit?parBookScheduleId=" + $("#RefNumInput").val();
    location.href = url;

})




var counterTripId = -1;

function addTripBoxs() {
    counterTripId++;
    var castFinal = parseFloat($("#SalikFees").val(), 10) + parseFloat($("#DriverFees").val(), 10) + parseFloat($("#CarFuelCost").val(), 10) + parseFloat($("#ParkingFees").val(), 10) + parseFloat($("#Consumption").val(), 10) + parseFloat($("#Other").val(), 10);
    var x = "<div id='stop" + counterTripId + "' data-number='" + counterTripId + "'  class='col-lg-3 col-sm-6'><div class='row'><div class='col-sm-12 col-md-12'><div class='thumbnail trips_box'><ul><div class='btn-group three_dotss'  onclick='javascript:openMenu(this);' role='group'><img class='dropdown-toggle' aria-haspopup='true' ";
    x = x + "<img src='/Content/images/three_dots.png' /><ul class='dropdown-menu'><li onclick='javascript:editTripBox(this.id);' id='id" + counterTripId + "' class='editBoxTrip'><a href='javascript:void(0)'>Edit</a></li><li id='delete" + counterTripId + "' onclick='javascript:deleteBlockTrip(this)'><a href='javascript:void(0)'>Delete</a></li></ul></div ><li>";
    x = x + "<img src='/Content/images/Pin-Map- Icon.png' /><span style='margin-left:7px;' class='destBoxData'>";
    x = x + $("#inputDistLocNm").selectivity('data').text;
    x = x + "</span></li><li>";
    x = x + "<img src='/Content/images/Person avatar.png' /><span style='margin-left:7px;' class='personImage'>";
    var DrvierArrayInfo = $("#inputparDriver").selectivity('data');
    if (DrvierArrayInfo != null && DrvierArrayInfo != undefined) {
        var sss = DrvierArrayInfo.text.split('|').join('<br>');//$("#inputparDriver").val();
        x = x + sss;
    }
    x = x + "</span ></li><li>";
    x = x + "<img src='/Content/images/caravatar.png' /><span style='margin-left:7px;' class='carImage'>";
    x = x + $("#inputparVehicleNum").selectivity('data').text.split('|').join('<br>');
    x = x + "</span ></li><li>";
    if ($("#driverTrip").is(":checked") == true) {
        x = x + "<img src='/Content/images/timeconfirmavatar.png' /><span style='margin-left:7px;' class='tripDateData'>" + $("#inputparArriv").val() + "</span></li ></ul ><button class='btn btn-primary btn-md btn-block color_D_T' style='border-radius:0px;' type='button'>";
    } else {
        x = x + "<img src='/Content/images/timeconfirmavatar.png' /><span style='margin-left:7px;' class='tripDateData'>" + $("#inputparArriv").val() + "</span></li ></ul ><button class='btn btn-primary btn-md btn-block' style='border-radius:0px;' type='button'>";
    }
    x = x + "Cost <span class='totalCost'>" + castFinal + " " + "</span> AED";
    x = x + "</button ></div ></div ></div ></div > ";
    $("#TripDiv").append(x);
    $("#prevStopDate").val($("#inputparArriv").val());
    $("#prevStopDate").attr("data-number", counterTripId);
    clearBoxs();
}


function showSpanText(textValue, textBoxId) {
    console.log(textValue + " " + textBoxId);
    if (textValue == "") {
        $("#" + textBoxId).parent().siblings("small").hide();
    } else {
        $("#" + textBoxId).find("small").css("display", "inline-block");
        $("#" + textBoxId).parent().siblings('small').css("display", "inline-block");
        $("#" + textBoxId).parent().siblings().find("b.textB").text(textValue);
        console.log(textValue + " " + textBoxId);
    }
}

function openMenu(id) {
    $(id).toggleClass("open");
    $(id).parents(".col-lg-3").siblings().find("div.open").removeClass("open");
    //$(this).addClass("open");
}


$(document).click(function (event) {
    if (!$(event.target).closest('.btn-group').length) {
        if ($('.btn-group').hasClass("open")) {
            $('.btn-group').removeClass("open");
        }
    }
})



function deleteBlockTrip(tBlock) {
    blockHash - 1;
    var blockHash = $("#" + tBlock.id).parents(".col-lg-3").data("number");
    var blockPrevIndex = $("#stop" + (blockHash-1)).find(".tripDateData").text();
    var prevBlockId = blockHash - 1;

    if (tBlock.id == $("#prevStopDate").data("number") && blockPrevIndex!="") {
        $("#prevStopDate").val(blockPrevIndex);
        console.log(prevBlockId);
        $("#prevStopDate").data("number", prevBlockId);
    } else {
        $("#prevStopDate").val(blockPrevIndex);
        $("#prevStopDate").attr("data-number", 0);
    }
    var blockIndex = $("#" + tBlock.id).parents(".col-lg-3").data("number");
    tripData.splice(blockIndex, 1);
    var total = Number($(".total_cost_number").text());
    var blockId = $("#" + tBlock.id).parents(".col-lg-3").attr("id");
    var blockCost = Number($("#" + blockId).find("span.totalCost").text());
    console.log(blockCost);
    var totalCaluc = total - blockCost;
    $(".total_cost_number").text(totalCaluc);
    $("#" + tBlock.id).parents(".col-lg-3").addClass("Deleted");
    $("#" + blockId).hide(500);
    setTimeout(deleteBlockT, 2000);

}

function deleteBlockT() {
    $(".Deleted").remove();
    $(".col-lg-3").each(function (i) {
        i--;
        $(this).attr('id', "stop" + (i + 1));
        $(this).data('number',(i + 1));
    });
}

$(".week_footer ul li").click(function () {
    $(this).toggleClass("week_day_active");

})



function DisableControl(disable) {
    $('#inputNameCustomer').selectivity.readOnly = disable;
    $('#inputNameClient').selectivity.readOnly = disable;
    $('#inputNameDepartment').selectivity.readOnly = disable;
    $('#inputLocationID').selectivity.readOnly = disable;
 //   $('#contractType').selectivity.readOnly = disable;
    //$("input").attr('disabled', disable);
    //$(".search_header").attr('disabled', disable);
    if (disable) {
        /// $(".box").css('backgroundColor', 'rgba(238, 238, 238,.11)');
        $("#divAddTrip").hide();
//        $(".rec").hide();
        //        $('#dropdown-menu').prop('disabled', true);
    } else {
        //      $(".box").css('backgroundColor', '#FFF');
        $("#divAddTrip").show();
  //      $(".rec").show();
        //    $('#dropdown-menu').prop('disabled', false);
    }
}


    var baseURL = window.location.protocol + "//" + window.location.host + "/"
    $(document).ready(function () {

       
        $("#inputparVehicleNum").selectivity('data', { id: 0, text: '' })

        $(".flatpickr-confirm").on("click",function () {
            if ($("#inputparArriv").val != "") {
                $("#inputparDriver").selectivity('setOptions', { readOnly: false });
                $("#inputparDriver").attr("disabled", false);
            }

        })
        //if ($("#bookDate").val() == "" && $("#inputparArriv").val() == "") {
        //    $("#inputparDriver").selectivity('setOptions', { readOnly: true });
        //    $("#inputparDriver").attr("disabled", true);
        //} else {
        //    $("#inputparDriver").selectivity('setOptions', { readOnly: false });
        //    $("#inputparDriver").attr("disabled", false);
        //}
        $('#RecurringTrip').click(function () {
            
            var $this = $(this);
            HandelRecurringTrip($this.is(':checked'));
        });
        $("#RecurringTrip").attr("checked", false);
        HandelRecurringTrip($("#RecurringTrip").prop("checked"));

        $('#bookDate').flatpickr({
            enableTime: true,
            dateFormat: "d/m/Y H:i",
            "plugins": [new confirmDatePlugin({})],
            onChange: function (dateobj, datestr) {
                //getDashboardInfo();
            }
        });

        $('#inputTillDay').flatpickr({
            enableTime: true,
            dateFormat: "d/m/Y H:i",
            "plugins": [new confirmDatePlugin({})],
            onChange: function (dateobj, datestr) {
                //getDashboardInfo();
            }
        });
        $("#divTripCreate").hide();
        $("#divAddTrip").show();


        $("#divAddTrip").click(function () {

            $("#divTripCreate").show();
            $("#divAddTrip").hide();
            clearBoxs();
            //showSpanText($(this).text(), 'divTripCreate');
        });

        $('#inputNameCustomer').selectivity({
            allowClear: true,
            multiple: false,
            ajax: {
                url: baseURL + "Customers/FindCustomers",
                minimumInputLength: 0,
                quietMillis: 250,
                params: function (term, offset) {
                    // GitHub uses 1-based pages with 30 results, by default
                    return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15, parClientid: GetIdFromselectivity($('#inputNameClient'))};
                },
                fetch: function (url, init, queryOptions) {
                    return $.ajax(url).then(function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    id: item.id,
                                    text: item.text
                                };
                            }),
                            more: (data.count > queryOptions.offset + data.items.length)
                        };
                    });
                }
            },
            placeholder: 'Customer'
        
            //templates: {
            //    resultItem: function (item) {
            //        return (
            //            '<div class="selectivity-result-item" data-item-id="' + item.id + '">' +
            //            '<b>' + item.text + '</b><br>' +
            //            '</div>'
            //        );
            //    }
            //}
        }).on('change', function (ev) {
            $("#inputNameDepartment").selectivity('clear');
        });
        $('#inputNameCustomer').on("selectivity-opening", function (e)
        {
            var clientdata = $("#inputNameClient").selectivity('data');
            if (clientdata == null) {
                ShowAlertMessage("Error, You must select Client first:#", "danger");
                e.preventDefault();
            }
        });
        $('#inputNameDepartment').on("selectivity-opening", function (e)
        {
            var clientdata = $("#inputNameCustomer").selectivity('data');
            if (clientdata == null) {
                ShowAlertMessage("Error, You must select Customer first:#", "danger");
                e.preventDefault();
            }
        });

        $('#inputNameClient').selectivity({
            allowClear: true,
            multiple: false,

            ajax: {
                url: baseURL + "Clients/FindClients",
                minimumInputLength: 0,
                quietMillis: 250,
                params: function (term, offset) {
                    // GitHub uses 1-based pages with 30 results, by default
                    return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15, parClientid: GetIdFromselectivity($('#inputNameClient'))};
                },
                fetch: function (url, init, queryOptions) {
                    return $.ajax(url).then(function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    id: item.id,
                                    text: item.text
                                };
                            }),
                            more: (data.count > queryOptions.offset + data.items.length)
                        };
                    });
                }
            },
            placeholder: 'Client',

        }).on('change', function (ev) {
            $("#inputNameCustomer").selectivity('clear');
            $("#inputNameDepartment").selectivity('clear');
        });
        $('#inputNameDepartment').selectivity({
            allowClear: true,
            multiple: false,
            ajax: {
                url: baseURL + "Departments/FindDepartment",
                minimumInputLength: 0,
                quietMillis: 250,
                params: function (term, offset) {
                    // GitHub uses 1-based pages with 30 results, by default
                    return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15, parCustomerid: GetIdFromselectivity($('#inputNameCustomer'))};
                },
                fetch: function (url, init, queryOptions) {
                    return $.ajax(url).then(function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    id: item.id,
                                    text: item.text
                                };
                            }),
                            more: (data.count > queryOptions.offset + data.items.length)
                        };
                    });
                }
            },
            placeholder: 'Department',
            //templates: {
            //    resultItem: function (item) {
            //        return (
            //            '<div class="selectivity-result-item" data-item-id="' + item.id + '">' +
            //            '<b>' + item.text + '</b><br>' +
            //            '</div>'
            //        );
            //    }
            //}
        });
        $('#inputLocationNm').selectivity({
            allowClear: true,
            multiple: false,
            ajax: {
                url: baseURL + "Locations/FindLocations",
                minimumInputLength: 0,
                quietMillis: 250,
                params: function (term, offset) {
                    // GitHub uses 1-based pages with 30 results, by default
                    return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15 };
                },
                fetch: function (url, init, queryOptions) {
                    return $.ajax(url).then(function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    id: item.id,
                                    text: item.text
                                };
                            }),
                            more: (data.count > queryOptions.offset + data.items.length)
                        };
                    });
                }
            },
            placeholder: '',

        });
        $('#contractType').selectivity({
            allowClear: false,
            multiple: false,
            readOnly: false,
            showSearchInputInDropdown: false,
            items: [{ id: 1, text: 'Long Term' }, { id: 2, text: 'Short Term' }],
            placeholder: 'Long Term',

        });
        $('#contractType').selectivity('data', { id: 1, text: "Long Term" })

        $('#shadooows').hide();
    });
function HandelRecurringTrip(ischecked) {
    if (ischecked) {
        $("#RecDiv").hide().slideDown();
    } else {
        $("#RecDiv").show().slideUp();
    }
}
    function collectTrips() {
        console.log($(".totalCost").text());
        var x = $(".totalCost").text().split(" ");
        var total = 0;
        var i = 0;
        for (i in x) { total += Number(x[i]); }
        console.log(x);
        $(".total_cost_number").text(total);
    }


function editTripBox(id) {
    $("#divTripCreate").show();
    $("#divAddTrip").hide();
    $("#btnParSave").hide();
    $(".hidden_edit").show();
    $(".btnParSaveCancelAdd").hide();
    var objId = $("#" + id).parents(".col-lg-3").data("number");
    $("#inputIdOfTrip").val(objId);
    var VehicleIDData = tripData[objId].VehicleID.id;
    var VehicleTextData = tripData[objId].VehicleID.text;
    $("#inputparVehicleNum").selectivity('data', { id: VehicleIDData, text: VehicleTextData })

        var EndLocid = tripData[objId].EndLocid.id;//destaination
        var EndLocText = tripData[objId].EndLocid.text;//destaination
        $("#inputDistLocNm").selectivity('data', { id: EndLocid, text: EndLocText })

        var driverID = tripData[objId].DriverID.id;
        var driverText = tripData[objId].DriverID.text;
        $("#inputparDriver").selectivity('data', { id: driverID, text: driverText })

      
        var EndDate = tripData[objId].EndDate;//ariavel time
        var arivalTimes = $("#inputparArriv").val(EndDate);
        var DriverFees = tripData[objId].DriverFees;
        var CarFuelCost = tripData[objId].CarFuelCost;
        var Consumption = tripData[objId].Consumption;
        var Other = tripData[objId].Other;
        var ParkingFees = tripData[objId].ParkingFees;
        var SalikFees = tripData[objId].SalikFees;
        var driverTrip = tripData[objId].driverTrip;
        var overwriteStartLoc = tripData[objId].overwriteStartLoc;


        var SalikFeess = $("#SalikFees").val(SalikFees);
        var DriverFeess = $("#DriverFees").val(DriverFees);
        var CarFuelCosts = $("#CarFuelCost").val(CarFuelCost);

        var ParkingFeess = $("#ParkingFees").val(ParkingFees);
        var Consumptions = $("#Consumption").val(Consumption);
        var Others = $("#Other").val(Other);
        var driiverTrip = $("#driverTrip").attr("checked", driverTrip);
    }



    function saveeditTripBox() {
        var objId1 = $("#inputIdOfTrip").val();
        getPrevNextStopDate(objId1);
        if ($("#inputDistLocNm").selectivity('data') == null || $("#inputDistLocNm").selectivity('data') == "") {
            $(".error_message_Trip").text("Please fill the destination field").slideDown();
        }
        else if ($("#inputparDriver").selectivity('data') == null) {
            $(".error_message_Trip").hide().text("Please fill the driver").slideDown();
        }
        else if ($("#inputparVehicleNum").selectivity('data') == null) {
            $(".error_message_Trip").hide().text("Please fill the vehicle").slideDown();
        }
        else if ($("#inputparArriv").val() < $("#bookDate").val()) {
            $(".error_message_Trip").hide().text("Please the arival time must be greater than the day time field ").slideDown();
        }
        else if ($("#inputparArriv").val() == "") {
            $(".error_message_Trip").hide().text("Please fill the Arival Time field").slideDown();
        }

        else if ($("#SalikFees").val() == "") {
            $(".error_message_Trip").hide().text("Please fill the Salik Fees field").slideDown();
        }
        else if ($("#DriverFees").val() == "") {
            $(".error_message_Trip").hide().text("Please fill Driver Fees field").slideDown();
        }
        else if ($("#CarFuelCost").val() == "") {
            $(".error_message_Trip").hide().text("Please fill the Car Fuel Cost field").slideDown();
        }
        else if ($("#ParkingFees").val() == "") {
            $(".error_message_Trip").hide().text("Please fill the Parking Fees field").slideDown();
        }
        else if ($("#Consumption").val() == "") {
            $(".error_message_Trip").hide().text("Please fill the Consumption field").slideDown();
        }
        else if ($("#Other").val() == "") {
            $(".error_message_Trip").hide().text("Please fill the Other field").slideDown();
        }
        else if (getPrevNextStopDate(objId1) == true) {
            $(".error_message_Trip").hide().text("please ariaval date must be greatre than prev stop and less than next stop").slideDown();
        }
        else {

            $(".error_message_Trip").hide().slideUp();
            var desainationInput = $('#inputDistLocNm').selectivity('data');
            var Drivers = $('#inputparDriver').selectivity('data');
            var Vehicles = $('#inputparVehicleNum').selectivity('data');
            var arivalTimes = $("#inputparArriv").val();

            var SalikFeess = $("#SalikFees").val();
            var DriverFeess = $("#DriverFees").val();
            var CarFuelCosts = $("#CarFuelCost").val();

            var ParkingFeess = $("#ParkingFees").val();
            var Consumptions = $("#Consumption").val();
            var Others = $("#Other").val();
            var driiverTrip = $("#driverTrip").prop("checked");
            var overwriteStartLoc = $("#overwriteStartLoc").selectivity('data');


            tripData[objId1].VehicleID = Vehicles;
            tripData[objId1].DriverID = Drivers;
            tripData[objId1].EndLocid = desainationInput;//destaination
            tripData[objId1].EndDate = arivalTimes;//ariavel time

            tripData[objId1].DriverFees = DriverFeess;
            tripData[objId1].CarFuelCost = CarFuelCosts;
            tripData[objId1].Consumption = Consumptions;
            tripData[objId1].Other = Others;
            tripData[objId1].ParkingFees = ParkingFeess;
            tripData[objId1].SalikFees = SalikFeess;
            tripData[objId1].driverTrip = driiverTrip;
            tripData[objId1].overwriteStartLoc = overwriteStartLoc;

            var castFinalTotal = parseFloat($("#SalikFees").val(), 10) + parseFloat($("#DriverFees").val(), 10) + parseFloat($("#CarFuelCost").val(), 10) + parseFloat($("#ParkingFees").val(), 10) + parseFloat($("#Consumption").val(), 10) + parseFloat($("#Other").val(), 10);
            var destText = $("#stop" + objId1).find(".destBoxData").text($('#inputDistLocNm').selectivity('data').text);
            var personName = $("#stop" + objId1).find(".personImage").text($('#inputparDriver').selectivity('data').text);
            var carName = $("#stop" + objId1).find(".carImage").text($('#inputparVehicleNum').selectivity('data').text);
            var tripDate = $("#stop" + objId1).find(".tripDateData").text($("#inputparArriv").val());
            var totalCost = $("#stop" + objId1).find(".totalCost").text(castFinalTotal + " ");

            $("#divTripCreate").hide();
            $("#divAddTrip").show();
            $("#btnParSave").show();
            $(".hidden_edit").hide();
            $(".btnParSaveCancelAdd").show();
            collectTrips();
            console.log(objId1);
            console.log(tripData[objId1]);
        }
    }

    function getPrevNextStopDate(id) {
        currid = id;
        var currNext = parseInt(currid, 10) + 1;
        var currPrev = parseInt(currid, 10) - 1;
        var prevStopDate = String($("#stop" + currPrev).find(".tripDateData").text());

        var currentStopDate = $("#inputparArriv").val();

        var nextstopDate = String($("#stop" + currNext).find(".tripDateData").text());

        if (currPrev < 0) {
            prevStopDate = $("#bookDate").val();
        } else {
            prevStopDate = $("#stop" + currPrev).find(".tripDateData").text();
        }
        if (String(currentStopDate) <= String(prevStopDate) && String(prevStopDate) != "" || String(currentStopDate) >= String(nextstopDate) && String(nextstopDate) != "") {
            return true;
            //console.log("please change the ariavel time");
        } else {
            return false;
            //console.log("heek tmam ma bdo t3'yeer");
        }


    }

    function cancelEdit() {
        $("#divTripCreate").hide();
        $("#divAddTrip").show();
        $(".btnParSaveCancelAdd").show();
        $("#btnParSave").show();
        $(".hidden_edit").hide();
        clearBoxs();
    }
    function clearBoxs() {
        $(".box_des").find("input[type=text] , textarea ,input[type=number],input[type=datetime-local]").each(function () {
            $(this).val('');
            $("#inputDistLocNm").selectivity('clear');
            //$("#inputparDriver").selectivity('clear');
            //$("#inputparVehicleNum").selectivity('clear');
        });

        $('#inputDistLocNm').selectivity.text = '';
        $('#inputparDriver').selectivity.text = '';
        $('#inputparVehicleNum').selectivity.text = '';
        $('#overwriteStartLoc').selectivity.text = '';
    }
//dialog = $("#dialog").dialog({
//    autoOpen: false,
//    height: 400,
//    width: 350,
//    modal: true,
//    buttons: {
//        "Create an account": addUser,
//        Cancel: function () {
//            dialog.dialog("close");
//        }
//    },
//    close: function () {
//        form[0].reset();
//        allFields.removeClass("ui-state-error");
//    }
//});
 

function confirmDatePlugin(pluginConfig) {
	var defaultConfig = {
		confirmIcon: "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='17' height='17' viewBox='0 0 17 17'> <g> </g> <path d='M15.418 1.774l-8.833 13.485-4.918-4.386 0.666-0.746 4.051 3.614 8.198-12.515 0.836 0.548z' fill='#000000' /> </svg>",
		confirmText: "OK ",
		showAlways: false,
		theme: "light"
	};

	var config = {};
	for (var key in defaultConfig) {
		config[key] = pluginConfig && pluginConfig[key] !== undefined ? pluginConfig[key] : defaultConfig[key];
	}

	return function (fp) {
		var hooks = {
			onKeyDown: function onKeyDown(_, __, ___, e) {
				if (fp.config.enableTime && e.key === "Tab" && e.target === fp.amPM) {
					e.preventDefault();
					fp.confirmContainer.focus();
				} else if (e.key === "Enter" && e.target === fp.confirmContainer) fp.close();
			},
			onReady: function onReady() {
				if (fp.calendarContainer === undefined) return;

				fp.confirmContainer = fp._createElement("div", "flatpickr-confirm " + (config.showAlways ? "visible" : "") + " " + config.theme + "Theme", config.confirmText);

				fp.confirmContainer.tabIndex = -1;
				fp.confirmContainer.innerHTML += config.confirmIcon;

				fp.confirmContainer.addEventListener("click", fp.close);
				fp.calendarContainer.appendChild(fp.confirmContainer);
			}
		};

		if (!config.showAlways) {
			hooks.onChange = function (dateObj, dateStr) {
				var showCondition = fp.config.enableTime || fp.config.mode === "multiple";
				if (dateStr && !fp.config.inline && showCondition) return fp.confirmContainer.classList.add("visible");
				fp.confirmContainer.classList.remove("visible");
			};
		}

		return hooks;
	};
}

if (typeof module !== "undefined") module.exports = confirmDatePlugin;


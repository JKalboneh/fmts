// JavaScript source code


(function ($) {

    $.fn.clickToggle = function (func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function () {
            var data = $(this).data();
            var tc = data.toggleclicked;
            $.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this; 
    };
}(jQuery));


var tripDataSave = [];
var totalBlock = 0;
var DisableControlMode = true;

$("#btnEditPage").on("click", function () {
    DisableControl(false);
    $("#btnSavePage").show();
    $("#btnEditPage").hide();
    checkStatus();
})

$('#btnSavePage').on('click', function () {
    $.confirm({
        title: 'Alert Message!',
        content: 'Do you want to save the changes...?',
        buttons: {
            cancel: function () {

            },
            somethingElse: {
                text: 'Save',
                btnClass: 'btn-blue',
                keys: [
                    'enter',
                    'shift'
                ],
                action: function () {
                    this.$content // reference to the content
                    $("#btnEditPage").show();
                    $("#btnSavePage").hide();
                    DisableControl(true);
                    checkStatus();
                    var aOldobjectData = JSON.parse($('#BookObjectStr').val());
                    var objectData = {};
                    
                    objectData.CustomerID = $('#inputNameCustomer').selectivity('data');
                    objectData.DepartmentID = $('#inputNameDepartment').selectivity('data');
                    objectData.ClientID = $('#inputNameClient').selectivity('data');
                    objectData.ContactType = $("#contractType").selectivity('data');
                    objectData.StartDatetime = $("#bookDate").val();
                    objectData.StartLoc = $("#inputLocationID").selectivity('data')
                    objectData.inputTillDay = $("#inputTillDay").val();
                    objectData.RecType = $("#RecurringTrip").prop("checked");
                    objectData.BookID = aOldobjectData.BookID;
                    objectData.ScheduleID = aOldobjectData.ScheduleID;
                    objectData.Status = aOldobjectData.Status;
                    objectData.Refnum = aOldobjectData.Refnum;
                    objectData.CreatedBy = aOldobjectData.CreatedBy;
                    objectData.TillDate = aOldobjectData.TillDate ;
                    objectData.SchedDays = aOldobjectData.SchedDays ;
                    objectData.TotalCost = aOldobjectData.TotalCost ;

                    var daysActive = $(".week_footer ul").find("li.week_day_active").text();
                    var daysClassActive = $(".week_footer ul").find("li.week_day_active").attr("id");
                    var daysActiveArray = daysActive.split(' ');
                    objectData.SchedDays = daysActiveArray;

                    objectData.TripList= [];
                    objectData.TripList.push(tripDataSave);
                    console.log(objectData);
                    pushToserverEdit(objectData);
                }
            }
        }
    });
});
function PushFromAnotherObject(paraTripList)
{
   
}
function AddtoHistory(e)
{
    var activeStartStr="";
    var activeEndStr="";
    if (e.Status == "Active") {
        var str = "<li>" + activeStartStr + "<a href='/Books/Edit?parBookScheduleId=" + e.BookHistoryID + "'>";
        $("#currentActSch").append(str + e.BookRefNumID + "</a>" + activeEndStr + "</li>");
    } else {
        var str = "<li>" + activeStartStr + "<a href='/Books/Edit?parBookScheduleId=" + e.BookHistoryID + "'>";
        $("#History").append(str + e.BookRefNumID + "</a>" + activeEndStr + "</li>");
    }
}
function pushToserverEdit(bookTripsData) {
    var baseURL = window.location.protocol + "//" + window.location.host + "/"
    $.ajax({
        url: "/Books/EditMdel",
        type: "POST",
        data: JSON.stringify(bookTripsData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#shadooows').show();
        },
        error: function (response) {
            alert("Error " + response.responseText);
            $('#shadooows').hide();

        },
        success: function (result) {
            //AddtoHistory(result.IDBookSch, result.Refnum);

            var url = '/Books/Edit?parBookScheduleId=' + result.IDBookSch;
            location.href = url;

            //window.location.href = baseURL + "Books/AddBook";
            //var url = '/Books/index';//+ result;
            //location.href = url;
        }
    });
}


function getBooktripData() {
    $.ajax({
        url: '/Books/GetBookInfoaAsObjectModel/',
        data: data,
        cache: false,
        dataType: "json",
        type: "GET",
        contentType: "application/json; charset=utf-8",
        beforeSend: function () {
        },
        success: function (data) {
            return data;
        },
        error: function (response) {
        },
        failure: function (response) {
        }
    });
}
function DisableControl(disable)
{
    DisableControlMode = disable;
    $('#inputNameClient').css("pointer-events", "none");
    if (disable)
    {
        $('#inputNameCustomer').css("pointer-events", "none");
        
        $('#inputNameDepartment').css("pointer-events", "none");
        $('#inputLocationID').css("pointer-events", "none");
        $("#contractType").css("pointer-events", "none");

        $('#inputparVehicleNum').css("pointer-events", "none");
        $('#inputparDriver').css("pointer-events", "none");
        $('#inputDistLocNm').css("pointer-events", "none");
        $('#overwriteStartLoc').css("pointer-events", "none");
        $('#btnParSaveEdit').hide();
        $("#divAddTrip").hide();
        $(".week_footer_cover").css("display","block");
    }
    else {
        $(".week_footer_cover").css("display", "none");
        $('#inputNameCustomer').css("pointer-events", "auto");
//        $('#inputNameClient').css("pointer-events", "auto");
        $('#inputNameDepartment').css("pointer-events", "auto");
        $('#inputLocationID').css("pointer-events", "auto");
        $("#contractType").css("pointer-events", "auto");
        $('#btnParSaveEdit').show();
        $("#divAddTrip").show();
        $("WeekDays").find('li').prop('enabled', true);

        $('#inputparVehicleNum').css("pointer-events", "auto");
        $('#inputparDriver').css("pointer-events", "auto");
        $('#inputDistLocNm').css("pointer-events", "auto");
        $('#overwriteStartLoc').css("pointer-events", "auto");

    }
    
    //.attr('disabled', disable);
    $("#RecurringTrip").attr('disabled', disable);
    $('#inputparVehicleNum').attr('disabled', disable);
    $('#inputparDriver').attr('disabled', disable);
    $('#inputDistLocNm').attr('disabled', disable);
    $('#overwriteStartLoc').attr('disabled', disable);
    $('#driverTrip').attr('disabled', disable);

    $('#SalikFees').attr('disabled', disable);
    $('#DriverFees').attr('disabled', disable);
    $('#CarFuelCost').attr('disabled', disable);
    $('#ParkingFees').attr('disabled', disable);
    $('#Consumption').attr('disabled', disable);
    $('#Other').attr('disabled', disable);
    $('#customerCost').attr('disabled', disable);
    

    $('#inputNameCustomer').attr('disabled', disable);
    $('#inputNameClient').attr('disabled', true);
    $('#inputNameDepartment').attr('disabled', disable);
    $('#inputLocationID').attr('disabled', disable);
    $("#contractType").attr('disabled', disable);

    $("#bookDate").attr('disabled', disable);
    $("#inputTillDay").attr('disabled', disable);
    $("#inputparArriv").attr('disabled', disable);
    $(".rec").attr('disabled', disable);
    return DisableControlMode;
}
DisableControlMode = true;

function HandelRecurringTrip(ischecked)
{
        if (ischecked){
            $("#RecDiv").hide().slideDown();
        } else {
            $("#RecDiv").show().slideUp();
        }
}
var objectData;
$(document).ready(function ()
{
    $("#divAddTrip").click(function () {
        $("#btnParSave").show();
        $("#btnParSaveCancel").show();
    });

    $("#btnPrint").on("click", function () {
        var baseURL = window.location.protocol + "//" + window.location.host + "/"
        $.ajax({
            type: "GET",
            url: baseURL + "Books/Print",
            data: { parBookScheduleId: objectData.ScheduleId },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
            },
            error: function (request, status, error) {
                ShowAlertMessage("Error, Can't delete this client", "danger");
            }
        });
      
    });
    $(".btnParSaveCancelAdd").hide();
    $('#shadooows').hide();
    $('#RecurringTrip').click(function () {
        
        var $this = $(this);
        HandelRecurringTrip($this.is(':checked'));
    });
    // if ($('#BookObjectStr').val() == '')
     //   $('#BookObjectStr').val(getBooktripData());
    
    var objectData = JSON.parse($('#BookObjectStr').val());
    if(objectData.Status!="Active")
    {
        $("#btnParSave").hide();
        $(".hidden_edit").hide();
        $("#btnEditSavePage").hide();
        $("#currentActSch").show();
        $("#activelbl").show();
    } else
    {
        $("#currentActSch").hide();
        $("#activelbl").hide();
    }
    var recurType = "";
    if (objectData.RecType == 0) {
        recurType = false;
    } else {
        recurType = true;
    }
    $("#RecurringTrip").attr("checked", recurType );
    HandelRecurringTrip($("#RecurringTrip").prop("checked"));

    DisableControl(true);

    console.log(objectData);
    $("#RefNumlbl").text("Book Ref Num :" + objectData.BookRef);
    $(".total_cost_number").text(objectData.TotalCost);
    $(objectData.bookHistory).each(function (x, y)
    {
        AddtoHistory(y);
    });
    $("#BookRefNum").val(objectData.BookRefNumID);
    if (objectData.SchedDays.length > 1) {
        $(".no").removeClass("rec_trip_active");
        $(".rec_trip li").first().addClass("rec_trip_active");
        $(".week_footer_wrapper").show();
    }

    //remove the last item from array
    //objectData.SchedDays.pop();
    for (i = 0; i <= 7; i++) {
        for (j = 0; j <= 7; j++) {
            if (objectData.SchedDays[i] == $(".week_footer").find("li").eq(j).text().trim()) {
                $(".week_footer").find("li").eq(j).addClass("week_day_active");
            }
        }
    }

        var DestinName  = "";
        var driverName  = "";
        var vehicleName = "";
        var endDate = "";
        var dataDateConv = "";
        var counterTripId = -1;
        
        $(objectData.TripList).each(function (x, y) {
            counterTripId++;
            console.log(y);
//            
            DestinName = y.EndLocid == undefined ? "" : y.EndLocid.text;//$(".destinationBox").find("li#" + y.EndLocid).text();
            driverName = y.DriverID == undefined ? "" : y.DriverID.text;//$(".driverBox").find("li#" + y.DriverID).text();
            vehicleName = y.VehicleID == undefined ? "" : y.VehicleID.text;//  y.VehicleName;//$(".vehicleBox").find("li#" + y.VehicleID).text();
            endDate = y.EndDate;
            var editOrOpenStr = DisableControlMode == true ? "Open" : "Edit";
            var castFinal = parseFloat(y.SalikFees, 10) + parseFloat(y.DriverFees, 10) + parseFloat(y.CarFuelCost, 10) + parseFloat(y.ParkingFees, 10) + parseFloat(y.Consumption, 10) + parseFloat(y.Other, 10);
            var x = "<div " + " StopDBId='" + y.TripID + "' id='stop" + counterTripId + "' data-number='" + counterTripId + "'  class='col-lg-3 col-sm-6'><div class='row'><div class='col-sm-12 col-md-12'><div class='thumbnail trips_box'><ul><div class='btn-group three_dotss'  onclick='javascript:openMenu(this);' role='group'><img class='dropdown-toggle' aria-haspopup='true' ";
            x = x + "<input type='hidden' value=\"" + y.TripID + "\" id='StopDBId'><img src='/Content/images/three_dots.png' /><ul class='dropdown-menu'><li onclick='javascript:editTripBox(this.id," + y.TripID + ");' id='id" + counterTripId + "' class='editBoxTrip'><a href='javascript:void(0)'>" + editOrOpenStr + "</a></li><li class='deleteBtn' id='delete" + counterTripId + "' onclick='javascript:deleteBlockTrip(this)'><a href='javascript:void(0)'>Delete</a></li></ul></div ><li>";
            x = x + "<img src='/Content/images/Pin-Map- Icon.png' /><span style='margin-left:7px;' class='destBoxData'>";
            x = x + DestinName;
            x = x + "</span></li><li>";
            x = x + "<img src='/Content/images/Person avatar.png' /><span style='margin-left:7px;' class='personImage'>";
            x = x + driverName;
            x = x + "</span ></li><li>";
            x = x + "<img src='/Content/images/caravatar.png' /><span style='margin-left:7px;' class='carImage'>";
            x = x + vehicleName;
            x = x + "</span ></li><li>";
            if (y.driverTrip == 1) {
                x = x + "<img src='/Content/images/timeconfirmavatar.png' /><span style='margin-left:7px;' class='tripDateData'>" + y.EndDate + "</span></li ></ul ><button class='btn btn-primary btn-md btn-block color_D_T' style='border-radius:0px;' type='button'>";
            } else {
                x = x + "<img src='/Content/images/timeconfirmavatar.png' /><span style='margin-left:7px;' class='tripDateData'>" + y.EndDate + "</span></li ></ul ><button class='btn btn-primary btn-md btn-block' style='border-radius:0px;' type='button'>";
            }
            x = x + "Cost <span class='totalCost'>" + castFinal + " " + "</span> AED";
            x = x + "</button ></div ></div ></div ></div > ";
            $("#TripDiv").append(x);
        });
        //console.log(objectData.TripList);
        //tripDataSave.push(objectData.TripList);

  
        var inputNameData = objectData.CustomerID.id == null ? { id: -1, text: '' } : { id: objectData.CustomerID.id, text: objectData.CustomerID.text}
    $('#inputNameCustomer').selectivity({
        allowClear: true,
        multiple: false,
        //readOnly: true,
        data: inputNameData,
        ajax: {
            url: baseURL + "Customers/FindCustomers",
            minimumInputLength: 0,
            quietMillis: 250,
            params: function (term, offset) {
                // GitHub uses 1-based pages with 30 results, by default
                return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15, parClientid: GetIdFromselectivity($('#inputNameClient'))};
            },
            fetch: function (url, init, queryOptions) {
                return $.ajax(url).then(function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                id: item.id,
                                text: item.text
                            };
                        }),
                        more: (data.count > queryOptions.offset + data.items.length)
                    };
                });
            }
        },
        placeholder: 'Customer',
        //templates: {
        //    resultItem: function (item) {
        //        return (
        //            '<div class="selectivity-result-item" data-item-id="' + item.id + '">' +
        //            '<b>' + item.text + '</b><br>' +
        //            '</div>'
        //        );
        //    }
        //}
    });

    $('#inputNameClient').selectivity({
        allowClear: true,
        multiple: false,
        //readOnly: true,
        data: { id: objectData.ClientID.id, text: objectData.ClientID.text },
        ajax: {
            url: baseURL + "Clients/FindClients",
            minimumInputLength: 0,
            quietMillis: 250,
            params: function (term, offset) {
                // GitHub uses 1-based pages with 30 results, by default
                return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15 };
            },
            fetch: function (url, init, queryOptions) {
                return $.ajax(url).then(function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                id: item.id,
                                text: item.text
                            };
                        }),
                        more: (data.count > queryOptions.offset + data.items.length)
                    };
                });
            }
        },
        placeholder: 'Client',

    });
    var DepartmentData = objectData.DepartmentID.id == null ? { id: -1, text: '' } : { id: objectData.DepartmentID.id, text: objectData.DepartmentID.text }
    
    $('#inputNameDepartment').selectivity({
        allowClear: true,
        multiple: false,
       // readOnly: true,
        data: DepartmentData,
        ajax: {
            url: baseURL + "Departments/FindDepartment",
            minimumInputLength: 0,
            quietMillis: 250,
            params: function (term, offset) {
                // GitHub uses 1-based pages with 30 results, by default
                return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15, parCustomerid: GetIdFromselectivity($('#inputNameCustomer'))};
            },
            fetch: function (url, init, queryOptions) {
                return $.ajax(url).then(function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                id: item.id,
                                text: item.text
                            };
                        }),
                        more: (data.count > queryOptions.offset + data.items.length)
                    };
                });
            }
        },
        placeholder: 'Department',
        //templates: {
        //    resultItem: function (item) {
        //        return (
        //            '<div class="selectivity-result-item" data-item-id="' + item.id + '">' +
        //            '<b>' + item.text + '</b><br>' +
        //            '</div>'
        //        );
        //    }
        //}
    });

    $('#inputNameCustomer').on("selectivity-opening", function (e) {
        var clientdata = $("#inputNameClient").selectivity('data');
        if (clientdata == null) {
            ShowAlertMessage("Error, You must select Client first:#", "danger");
            e.preventDefault();
        }
    });
    $('#inputNameDepartment').on("selectivity-opening", function (e) {
        var clientdata = $("#inputNameCustomer").selectivity('data');
        if (clientdata == null) {
            ShowAlertMessage("Error, You must select Customer first:#", "danger");
            e.preventDefault();
        }
    });


    var locationData = objectData.StartLoc.id == null ? { id: null, text: '' } : { id: objectData.StartLoc.id, text: objectData.StartLoc.text }
    $('#inputLocationID').selectivity({
        allowClear: true,
        multiple: false,
        //readOnly: true,
        data: locationData,
        ajax: {
            url: baseURL + "Locations/FindLocations",
            minimumInputLength: 0,
            quietMillis: 250,
            params: function (term, offset) {
                // GitHub uses 1-based pages with 30 results, by default
                return { q: term, page: 1 + Math.floor(offset / 15), pageSize: 15 };
            },
            fetch: function (url, init, queryOptions) {
                return $.ajax(url).then(function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                id: item.id,
                                text: item.text
                            };
                        }),
                        more: (data.count > queryOptions.offset + data.items.length)
                    };
                });
            }
        },
        placeholder: '',

    });

    $('#contractType').selectivity({
        allowClear: false,
        multiple: false,
        //readOnly: true,
        items: [{ id: 1, text: 'Long Term' }, { id: 2, text: 'Short Term' }],
        placeholder: 'Long Term',

    });
    $('#contractType').selectivity('data', { id: objectData.ContactType.id, text: objectData.ContactType.text })      


    $('#bookDate').flatpickr({
        enableTime: true,
        dateFormat: "d/m/Y H:i",
        "plugins": [new confirmDatePlugin({})],
        onChange: function (dateobj, datestr) {
            //getDashboardInfo();
        }
    });

    $('#inputTillDay').flatpickr({
        enableTime: true,
        dateFormat: "d/m/Y H:i",
        "plugins": [new confirmDatePlugin({})],
        onChange: function (dateobj, datestr) {
            //getDashboardInfo();
        }
    });

    $("#bookDate").val(objectData.StartDatetime);
    $("#inputTillDay").val(objectData.inputTillDay);

    window.boxNumber = 4;
    console.log(objectData.TripList);
    tripDataSave.push(objectData.TripList);
})





$('#btnParSave').click(function () {
     
    var currentCounter = getNumberBox();
    if ($("#inputDistLocNm").selectivity('data') == null || $("#inputDistLocNm").selectivity('data') == "") {
        $(".error_message_Trip").text("Please fill the destination field").slideDown();
    }
    else if ($("#inputparDriver").selectivity('data') == null) {
        $(".error_message_Trip").hide().text("Please fill the driver").slideDown();
    }
    //else if ($("#inputparVehicleNum").selectivity('data') == null) {
    //    $(".error_message_Trip").hide().text("Please fill the vehicle").slideDown();
    //}
    else if ($("#inputparArriv").val() < $("#bookDate").val()) {
        $(".error_message_Trip").hide().text("Please the arival time must be greater than the day time field ").slideDown();
    }
    else if ($("#inputparArriv").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Arival Time field").slideDown();
    }

    else if ($("#SalikFees").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Salik Fees field").slideDown();
    }
    else if ($("#DriverFees").val() == "") {
        $(".error_message_Trip").hide().text("Please fill Driver Fees field").slideDown();
    }
    else if ($("#CarFuelCost").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Car Fuel Cost field").slideDown();
    }
    else if ($("#ParkingFees").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Parking Fees field").slideDown();
    }
    else if ($("#Consumption").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Consumption field").slideDown();
    }
    else if ($("#Other").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Other field").slideDown();
    }

    else {
        $(".error_message_Trip").slideUp();
        addTripBoxsSave();
        if (DisableControlMode == false) {
            $("#divTripCreate").hide();
            $("#divAddTrip").show();
        }
        var trip = {};

        trip.EndLocid = $("#inputDistLocNm").selectivity('data');
        trip.DriverID = $("#inputparDriver").selectivity('data');
        trip.EndDate = $("#inputparArriv").val();
        trip.VehicleID = $("#inputparVehicleNum").selectivity('data');
        trip.SalikFees = $("#SalikFees").val();
        trip.DriverFees = $("#DriverFees").val();
        trip.CarFuelCost = $("#CarFuelCost").val();
        trip.ParkingFees = $("#ParkingFees").val();
        trip.Consumption = $("#Consumption").val();
        trip.Other = $("#Other").val();
        trip.driverTrip = $("#driverTrip").prop("checked");
        trip.overwriteStartLoc = $("#overwriteStartLoc").selectivity('data');
        tripDataSave[0].push(trip);
        //collectTrips(trip);
        setTimeout(collectTrips, 100);
    }
});



setTimeout(getNumberBox, 4000);

function getNumberBox() {
    return $(".trips_box").length-1;
}


var counterTripIdNew = getNumberBox();
function addTripBoxsSave() {
    var DrvierArrayInfo = $("#inputparDriver").selectivity('data');
    counterTripIdNew++;
    var DrvierArrayInfo = $("#inputparDriver").selectivity('data');
    var castFinal = parseFloat($("#SalikFees").val(), 10) + parseFloat($("#DriverFees").val(), 10) + parseFloat($("#CarFuelCost").val(), 10) + parseFloat($("#ParkingFees").val(), 10) + parseFloat($("#Consumption").val(), 10) + parseFloat($("#Other").val(), 10);
    var x = "<div id='stop" + counterTripIdNew + "' data-number='" + counterTripIdNew + "'  class='col-lg-3 col-sm-6'><div class='row'><div class='col-sm-12 col-md-12'><div class='thumbnail trips_box'><ul><div class='btn-group three_dotss'  onclick='javascript:openMenu(this);' role='group'><img class='dropdown-toggle' aria-haspopup='true' ";
    x = x + "<img src='/Content/images/three_dots.png' /><ul class='dropdown-menu'><li onclick='javascript:editTripBox(this.id,0);' id='id" + counterTripIdNew + "' class='editBoxTrip'><a href='javascript:void(0)'>Edit</a></li><li id='delete" + counterTripIdNew + "' onclick='javascript:deleteBlockTrip(this)'><a href='javascript:void(0)'>Delete</a></li></ul></div ><li>";
    x = x + "<img src='/Content/images/Pin-Map- Icon.png' /><span style='margin-left:7px;'  class='destBoxData'>";
    x = x + $("#inputDistLocNm").selectivity('data').text;
    x = x + "</span></li><li>";
    x = x + "<img src='/Content/images/Person avatar.png' /><span style='margin-left:7px;' class='personImage'>";
    if (DrvierArrayInfo != null && DrvierArrayInfo != undefined) {
        var sss = DrvierArrayInfo.text.split('|').join('<br>');//$("#inputparDriver").val();
        x = x + sss;
    }
    x = x + "</span ></li><li>";
    x = x + "<img src='/Content/images/caravatar.png' /><span style='margin-left:7px;' class='carImage'>";
    x = x + $("#inputparVehicleNum").selectivity('data').text.split('|').join('<br>');
    x = x + "</span ></li><li>";
    if ($("#driverTrip").is(":checked") == true) {
        x = x + "<img src='/Content/images/timeconfirmavatar.png' /><span style='margin-left:7px;' class='tripDateData'>" + $("#inputparArriv").val() + "</span></li ></ul ><button class='btn btn-primary btn-md btn-block color_D_T' style='border-radius:0px;' type='button'>";
    } else {
        x = x + "<img src='/Content/images/timeconfirmavatar.png' /><span style='margin-left:7px;' class='tripDateData'>" + $("#inputparArriv").val() + "</span></li ></ul ><button class='btn btn-primary btn-md btn-block' style='border-radius:0px;' type='button'>";
    }
    x = x + "Cost <span class='totalCost'>" + castFinal + " " + "</span> AED";
    x = x + "</button ></div ></div ></div ></div > ";
    $("#TripDiv").append(x);

}





function showSpanText(textValue, textBoxId) {
    console.log(textValue + " " + textBoxId);
    if (textValue == "") {
        $("#" + textBoxId).parent().siblings("small").hide();
    } else {
        $("#" + textBoxId).find("small").css("display", "inline-block");
        $("#" + textBoxId).parent().siblings('small').css("display", "inline-block");
        $("#" + textBoxId).parent().siblings().find("b.textB").text(textValue);
        console.log(textValue + " " + textBoxId);
    }
}

function openMenu(id) {
    $(id).toggleClass("open");
    $(id).parents(".col-lg-3").siblings().find("div.open").removeClass("open");
    //$(this).addClass("open");
}


$(document).click(function (event) {
    if (!$(event.target).closest('.btn-group').length) {
        if ($('.btn-group').hasClass("open")) {
            $('.btn-group').removeClass("open");
        }
    }
})


$(".rec_trip li").click(function () {
    $(this).addClass("rec_trip_active");
    $(this).siblings().removeClass("rec_trip_active");
})


//$(".rec_trip li").on("click", function () {
//    if ($(this).hasClass("no")) {
//        {
//            $(".week_footer_wrapper").hide();
//            $("#inputTillDay").hide();
//            $("#lblTillDate").hide();

//        }
//    } else {
//        $(".week_footer_wrapper").show();
//        $("#inputTillDay").show();
//        $("#lblTillDate").show();
        
//    }
//})


function deleteBlockTrip(tBlock) {
    var blockIndex = $("#" + tBlock.id).parents(".col-lg-3").data("number");
    tripDataSave.splice(blockIndex, 1);
    var total = Number($(".total_cost_number").text());
    var blockId = $("#" + tBlock.id).parents(".col-lg-3").attr("id");
    var blockCost = Number($("#" + blockId).find("span.totalCost").text());
    //console.log(blockCost);
    var totalCaluc = total - blockCost;
    $(".total_cost_number").text(totalCaluc);
    $("#" + tBlock.id).parents(".col-lg-3").addClass("Deleted");
    $("#" + blockId).hide(500);
    setTimeout(deleteBlockT, 2000);

}

function deleteBlockT() {
    $(".Deleted").remove();
    $(".col-lg-3").each(function (i) {
        i--;
        $(this).attr('id', "stop" + (i + 1));
        $(this).data('number', (i + 1));
    });
}

$(".week_footer ul li").click(function () {
    $(this).toggleClass("week_day_active");

})



function collectTrips() {
    //console.log($(".totalCost").text());
    var x = $(".totalCost").text().split(" ");
    var total = 0;
    var i = 0;
    for (i in x) { total += Number(x[i]); }
    $(".total_cost_number").text(total);
}
//setTimeout(collectTrips, 3000);
var baseURL = window.location.protocol + "//" + window.location.host + "/";

function editTripBox(id, partripid) {
    $("#divTripCreate").show();
    $("#divAddTrip").hide();
    $("#btnParSave").hide();
    $(".hidden_edit").show();
    var objId = $("#" + id).parents(".col-lg-3").data("number");
    $("#inputIdOfTrip").val(objId);
    $("#TripID").val(partripid);
    var VehicleIDData = tripDataSave[0][objId].VehicleID.id;
    var VehicleTextData = tripDataSave[0][objId].VehicleID.text;
    $("#inputparVehicleNum").selectivity('data', { id: VehicleIDData, text: VehicleTextData })

    var EndLocid = tripDataSave[0][objId].EndLocid.id;//destaination
    var EndLocText = tripDataSave[0][objId].EndLocid.text;//destaination
    $("#inputDistLocNm").selectivity('data', { id: EndLocid, text: EndLocText })

    var driverID = tripDataSave[0][objId].DriverID.id;
    var driverText = tripDataSave[0][objId].DriverID.text;
    $("#inputparDriver").selectivity('data', { id: driverID, text: driverText })

    var locationBoxs = $(".destinationBox").find("li#" + EndLocid).text();

    var EndDate = tripDataSave[0][objId].EndDate;//ariavel time
    var arivalTimes = $("#inputparArriv").val(EndDate);
    var DriverFees = tripDataSave[0][objId].DriverFees;
    var CarFuelCost = tripDataSave[0][objId].CarFuelCost;
    var Consumption = tripDataSave[0][objId].Consumption;
    var Other = tripDataSave[0][objId].Other;
    var ParkingFees = tripDataSave[0][objId].ParkingFees;
    var SalikFees = tripDataSave[0][objId].SalikFees;
    var driverTrip = tripDataSave[0][objId].driverTrip;
    var overwriteStartLoc = tripDataSave[0][objId].overwriteStartLoc;


    var SalikFeess = $("#SalikFees").val(SalikFees);
    var DriverFeess = $("#DriverFees").val(DriverFees);
    var CarFuelCosts = $("#CarFuelCost").val(CarFuelCost);

    var ParkingFeess = $("#ParkingFees").val(ParkingFees);
    var Consumptions = $("#Consumption").val(Consumption);
    var Others = $("#Other").val(Other);
    var driiverTrip = $("#driverTrip").attr("checked", driverTrip);
}



function saveeditTripBox() {
    var objId1 = $("#inputIdOfTrip").val();
    console.log(objId1);
    getPrevNextStopDate(objId1);
    if ($("#inputDistLocNm").selectivity('data') == null || $("#inputDistLocNm").selectivity('data') == "") {
        $(".error_message_Trip").text("Please fill the destination field").slideDown();
    }
    else if ($("#inputparDriver").selectivity('data') == null) {
        $(".error_message_Trip").hide().text("Please fill the driver").slideDown();
    }
    //else if ($("#inputparVehicleNum").selectivity('data') == null) {
    //    $(".error_message_Trip").hide().text("Please fill the vehicle").slideDown();
    //}
    else if ($("#inputparArriv").val() < $("#bookDate").val()) {
        $(".error_message_Trip").hide().text("Please the arival time must be greater than the day time field ").slideDown();
    }
    else if ($("#inputparArriv").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Arival Time field").slideDown();
    }

    else if ($("#SalikFees").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Salik Fees field").slideDown();
    }
    else if ($("#DriverFees").val() == "") {
        $(".error_message_Trip").hide().text("Please fill Driver Fees field").slideDown();
    }
    else if ($("#CarFuelCost").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Car Fuel Cost field").slideDown();
    }
    else if ($("#ParkingFees").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Parking Fees field").slideDown();
    }
    else if ($("#Consumption").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Consumption field").slideDown();
    }
    else if ($("#Other").val() == "") {
        $(".error_message_Trip").hide().text("Please fill the Other field").slideDown();
    }
    else if (getPrevNextStopDate(objId1)==true){
        $(".error_message_Trip").hide().text("please ariaval date must be greatre than prev stop and less than next stop").slideDown();
    }
    else {
        $(".error_message_Trip").hide().slideUp();
        var objId1 = $("#inputIdOfTrip").val();
        var desainationInput = $('#inputDistLocNm').selectivity('data');
        var Drivers = $('#inputparDriver').selectivity('data');
        var Vehicles = $('#inputparVehicleNum').selectivity('data');
        var arivalTimes = $("#inputparArriv").val();

        var SalikFeess = $("#SalikFees").val();
        var DriverFeess = $("#DriverFees").val();
        var CarFuelCosts = $("#CarFuelCost").val();

        var ParkingFeess = $("#ParkingFees").val();
        var Consumptions = $("#Consumption").val();
        var Others = $("#Other").val();
        var driiverTrip = $("#driverTrip").prop("checked");
        var overwriteStartLoc = $("#overwriteStartLoc").selectivity('data');


        tripDataSave[0][objId1].VehicleID = Vehicles;
        tripDataSave[0][objId1].DriverID = Drivers;
        tripDataSave[0][objId1].EndLocid = desainationInput;//destaination
        tripDataSave[0][objId1].EndDate = arivalTimes;//ariavel time

        tripDataSave[0][objId1].DriverFees = DriverFeess;
        tripDataSave[0][objId1].CarFuelCost = CarFuelCosts;
        tripDataSave[0][objId1].Consumption = Consumptions;
        tripDataSave[0][objId1].Other = Others;
        tripDataSave[0][objId1].ParkingFees = ParkingFeess;
        tripDataSave[0][objId1].SalikFees = SalikFeess;
        tripDataSave[0][objId1].driverTrip = driiverTrip;
        tripDataSave[0][objId1].overwriteStartLoc = overwriteStartLoc;
    

        var castFinalTotal = parseFloat($("#SalikFees").val(), 10) + parseFloat($("#DriverFees").val(), 10) + parseFloat($("#CarFuelCost").val(), 10) + parseFloat($("#ParkingFees").val(), 10) + parseFloat($("#Consumption").val(), 10) + parseFloat($("#Other").val(), 10);
        var destText = $("#stop" + objId1).find(".destBoxData").text($('#inputDistLocNm').selectivity('data').text);
        var personName = $("#stop" + objId1).find(".personImage").text($('#inputparDriver').selectivity('data').text);
        var carName = $("#stop" + objId1).find(".carImage").text($('#inputparVehicleNum').selectivity('data').text);
        var tripDate = $("#stop" + objId1).find(".tripDateData").text($("#inputparArriv").val());
        var totalCost = $("#stop" + objId1).find(".totalCost").text(castFinalTotal + " ");

        $("#divTripCreate").hide();
        if (DisableControlMode)
            $("#divAddTrip").hide();
        else
            $("#divAddTrip").show();
        $("#btnParSave").show();
        $(".hidden_edit").hide();
        clearBoxs();
        collectTrips();
    }
}



function cancelEdit() {
    $("#divTripCreate").hide();
    if (DisableControlMode == true)
        $("#divAddTrip").hide();
    else
        $("#divAddTrip").show();
    $("#btnParSave").show();
    $(".hidden_edit").hide();
    clearBoxs();
}
function clearBoxs() {
    $(".box_des").find("input[type=text] , textarea ,input[type=number],input[type=datetime-local]").each(function () {
        $(this).val('');
        $("#inputDistLocNm").selectivity('clear');
        $("#inputparDriver").selectivity('clear');
        $("#inputparVehicleNum").selectivity('clear');
    });

    $('#inputDistLocNm').selectivity.text = '';
    $('#inputparDriver').selectivity.text = '';
    $('#inputparVehicleNum').selectivity.text = '';
    $('#overwriteStartLoc').selectivity.text = '';
}

function checkStatus() {
    if (DisableControlMode == true) {
        $(".deleteBtn").hide();
        $(".editBoxTrip").find("a").text("Open");
    } else {
        $(".deleteBtn").show();
        $(".editBoxTrip").find("a").text("Edit");
    }
}

function getPrevNextStopDate(id) {
    currid = id;
    var currNext = parseInt(currid, 10)+1;
    var currPrev = parseInt(currid, 10)-1;
    var prevStopDate = String($("#stop" + currPrev).find(".tripDateData").text());

    var currentStopDate = $("#inputparArriv").val();

    var nextstopDate = String($("#stop" + currNext).find(".tripDateData").text());

    if (currPrev < 0) {
        prevStopDate = $("#bookDate").val();
    }else{
        prevStopDate = $("#stop" + currPrev).find(".tripDateData").text();
    }
    if (String(currentStopDate) <= String(prevStopDate) || String(currentStopDate) >= String(nextstopDate) && String(nextstopDate)!="") {
        return true;
        //console.log("please change the ariavel time");
    } else {
        return false;
        //console.log("heek tmam ma bdo t3'yeer");
    }


}


﻿var pageWidth = $("#gridContent").parent().width() - 102;

$("#jsGrid").jsGrid({
    width: "100%",
    height: "auto",
    inserting: false,
    editing: false,
    sorting: true,
    paging: true,
    filtering: true,
    autoload: true,
    pageLoading: true,

    pagerContainer: null,
    pageIndex: 1,
    pageSize: 20,
    pageButtonCount: 15,
    pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
    pagePrevText: "Prev",
    pageNextText: "Next",
    pageFirstText: "First",
    pageLastText: "Last",
    pageNavigatorNextText: "...",
    pageNavigatorPrevText: "...",

    controller: {
        loadData: function(filter) {
            var d = $.Deferred();

            $.ajax({
                type: "GET",
                url: baseURL + "/Books/FindBooksGrid",
                data: filter,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function (response) {
                debugger
                d.resolve({
                    data: response.data,
                    itemsCount: response.itemsCount
                });
            });

            return d.promise();
        }
    },
    fields: [
        { name: "BookRef", type: "text", title: "BookRef",  width: (pageWidth * (20 / 102)) },
        { name: "ClientName", type: "text", title: "Client Name",  width: (pageWidth * (20 / 102)) },
        { name: "CustomerName", type: "text", title: "Customer Name",  width: (pageWidth * (20 / 102)) },
        { name: "DepartmentName", type: "text", title: "Department Name",  width: (pageWidth * (15 / 102)) },
        { name: "strTillDate", type: "text", title: "Till Date", filtering: false, width: (pageWidth * (15 / 102)) },
        {
            itemTemplate: function(value, item) {
                
                if (haspermissiontoEdit) {
                    return (
                        '<a href ="/Books/Edit?parBookScheduleId=' + item.BookScheduleID + '">' +
                        '<button class="btn btn-primary btn-md">Edit</button>' +
                        '</a>'
                    );
                }
                else
                {
                    return ('');
                }
            }, width: (pageWidth * (10 / 102))
        },
    ]
});







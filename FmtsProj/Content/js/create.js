

    $("#divTripCreate").hide();
    $("#divAddTrip").show();

    $(document).ready(function () {



        $("#divTripCreate").hide();
        $("#divAddTrip").show();

        $("#listLocation li").click(
            function (e) {
                $("#hotelId").val($(this).text());
                //$("#inputIdClient").val($(this).attr('id'));
            })

        $("#listClient li").click(
            function (e) {
                $("#inputNameClient").val($(this).text());
                //$("#inputIdClient").val($(this).attr('id'));
            })

        $("#listLocation li").click(
            function (e) {
                $("#inputLocationNm").val($(this).text());
                //$("#inputIdClient").val($(this).attr('id'));
            })

        $("#divAddTrip").click(function () {


            $("#divTripCreate").show();
            $("#divAddTrip").hide();
        });
        var inputValues = new Array();
        $('inputNameClient').each(function () {
            inputValues[$(this).attr('name')] = $(this).val();
        });
        $('inputDate').each(function () {
            inputValues[$(this).attr('name')] = $(this).val();
        });
        $('inputLocationID').each(function () {
            inputValues[$(this).attr('name')] = $(this).val();
        });
        $('inputTillDay').each(function () {
            inputValues[$(this).attr('name')] = $(this).val();
        });
        $('inputLocationNm').each(function () {
            inputValues[$(this).attr('name')] = $(this).val();
        });
    });


    var minute = 0; // minute by default will be 0
    var hour = 1;
    $(".minute").val(0 + "m");
    $(".hour").val(1 + 'pm');
    function increseDecrease(incDec) {
        if (incDec == "increase") {
            minute++;
            $(".minute").val(minute + "m");
            if (minute >= 60) {
                minute = 0;
            }
        } else {
            minute--;
            $(".minute").val(minute + "m");
            if (minute <= 0) {
                minute = 60;
            }
        }
    }


    function tIncreseDecrease(incDec) {
        if (incDec == "increase") {
            hour++;
            $(".hour").val(hour + "pm");
            if (hour >= 12) {
                hour = 0;
            }
        } else {
            hour--;
            $(".hour").val(hour + "am");
            if (hour <= 0) {
                hour = 13;
            }
        }
    }

  

    function getFromLocal() {
        return JSON.parse(localStorage.getItem('trips'));
    }


    function pushToLocal(trip) {
        // Retrieve the trips object from local storage
        if (localStorage && localStorage.getItem('trips')) {
            var trips = JSON.parse(localStorage.getItem('trips'));

            // push the new json array
            trips.push(trip);
            // add the new value to the local storage ( trips )
            localStorage.setItem('trips', JSON.stringify(trips));
        } else {
            trips = [trip];
            localStorage.setItem('trips', JSON.stringify(trips));
        }
    }

    function clearLocal() {
        // clear all data from the local storage ( trip )
        localStorage.removeItem('trips');
    }
    function add(tripNo, tripYes, clientName, bookDate, startLocation, destaination, driver, arrivelTime, SalikFees, DriverFees, CarFuelCost, ParkingFees, Consumption, Vehicle, Other, tillDay) {
        var trip = {};
        trip.startLocation = startLocation;
        trip.bookDate = bookDate;
        trip.Vehicle = Vehicle;
        trip.tillDay = tillDay;
        trip.clientName = clientName;
        trip.tripNo = tripNo;
        trip.tripYes = tripYes;
        trip.destaination = destaination;
        trip.driver = driver;
        trip.arrivelTime = arrivelTime;
        trip.StartDatetime = arrivelTime;
        trip.SalikFees = SalikFees;
        trip.DriverFees = DriverFees;
        trip.CarFuelCost = CarFuelCost;
        trip.ParkingFees = ParkingFees;
        trip.Consumption = Consumption;
        trip.Other = Other;
        pushToLocal(trip);

    }



    $('#btnParSave').click(function () {
        
        var clientName = Number($(".clientActive").attr("id"));
        var bookDate = $("#bookDate").val();
        var startLocation = $(".locationActive").attr("id");
        var destaination = $("#inputDistLocNm").val();
        var driver = Number($(".VehicalActive").attr("id"));
        var Vehicle = Number($(".DriverActive").attr("id"));
        var arrivelTime = $("#partialArivalTime").val();
        var SalikFees = $("#SalikFees").val();
        var DriverFees = $("#DriverFees").val();
        var CarFuelCost = $("#CarFuelCost").val();
        var ParkingFees = $("#ParkingFees").val();
        var Consumption = $("#Consumption").val();
        var Other = $("#Other").val();
        var tripYes = $("#tripYes").prop("checked");
        var tripNo = $("#tripNo").prop("checked");
        var tillDay = $("#inputTillDay").val();
        add(tripNo, tripYes, clientName, bookDate, startLocation, destaination, driver, arrivelTime, SalikFees, DriverFees, CarFuelCost, ParkingFees, Consumption, Vehicle, Other, tillDay);
        collectTrips();
    });


    function collectTrips() {
        console.log($(".totalCost").text());
        var x = $(".totalCost").text().split(" ");
        var total = 0;
        var i = 0;
        for (i in x) { total += Number(x[i]); }
        console.log(x);
        $(".total_cost_number").text(total);
    }
 


    //$("#btnSave").click(function () {
    //    var tripsData = {};
    //    tripsData.clientName = $("#inputNameClient").val();
    //    tripsData.bookDate = $("#inputDate").val();
    //    tripsData.startLocation = $("#inputLocationNm").val();
    //    tripsData.recurringTrip = $(".rec_trip").find("li.rec_trip_active").text();
    //    tripsData.tillDay = $("#inputTillDay").val();
    //    var daysActive = $(".week_footer ul").find("li.week_day_active").text();
    //    var daysActiveArray = daysActive.split(' ');
    //    tripsData.weekOfDay = daysActiveArray;
    //    tripsData.trips = getFromLocal();
    //    console.log(tripsData);
    //    //localStorage.setItem('tripsData', JSON.stringify(tripsData));
    //    //pushToserver(tripsData);
    //})



    (function ($) {
        $.fn.clickToggle = function (func1, func2) {
            var funcs = [func1, func2];
            this.data('toggleclicked', 0);
            this.click(function () {
                var data = $(this).data();
                var tc = data.toggleclicked;
                $.proxy(funcs[tc], this)();
                data.toggleclicked = (tc + 1) % 2;
            });
            return this;
        };
    }(jQuery));


    $('#btnEdit').clickToggle(function () {
        var tripsData = {};
        tripsData.clientId = Number($(".clientActive").attr("id"));
        tripsData.clientName = $("#inputNameClient").val();
        tripsData.bookDate = $("#bookDate").val();
        tripsData.startLocation = $(".locationActive").attr("id");
        tripsData.recurringTrip = $(".rec_trip").find("li.rec_trip_active").text();
        tripsData.tillDay = $("#inputTillDay").val();
        tripsData.driverTrip = $("#driverTrip").is(":checked");
        tripsData.CustomerCost = $("#customerCost").is(":checked");
        var daysActive = $(".week_footer ul").find("li.week_day_active").text();
        var daysActiveArray = daysActive.split(' ');
        tripsData.weekOfDay = daysActiveArray;
        tripsData.trips = getFromLocal();
        console.log(tripsData);
        $("input").attr('disabled', true);
        
        $(".search_header").attr('disabled', false);
        $(".box").css('backgroundColor', 'rgba(238, 238, 238,.11)');
        $("#divAddTrip").hide();
        $(".rec").hide();
        $(this).text("Edit Trip");
        pushToserver(tripsData,'Create');

    }, function () {
        $(this).text("Create Booking Reference");
        $("input").attr('disabled', false);
        // $("button").attr('disabled',true);
        $(".box").css('backgroundColor', '#FFF');
        $("#divAddTrip").show();
        $(".rec").show();
        });

    function pushToserver(trips,mode) {
        var json = JSON.stringify(trips);
        localStorage.removeItem("trips");
        $.ajax(
            {
                url: "/Books/SaveData",
                data: json,
                type: 'POST'
                , contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    alert(result)
                  //  var url = '/Books/Edit?bookRefId=' + result;
                  //  location.href = url;
                   // console.log(result);
                },
                error: function (result) {
                    console.log(result);
                }
            }
        );
    }




    $(".week_footer ul li").click(function () {
        $(this).toggleClass("week_day_active");

    })


    $(".rec_trip li").click(function () {
        $(this).addClass("rec_trip_active");
        $(this).siblings().removeClass("rec_trip_active");
    })



    sumjq = function (selector) {
        var sum = 0;
        $(selector).each(function (i, j) {
            sum = sum + Number($(this).text());

        });
        return sum;
    }

    function callX(textValue, textBoxId) {
        console.log(textValue + " " + textBoxId);
        if (textValue == "") {
            $("#" + textBoxId).parent().siblings("small").hide();
        } else {
            $("#" + textBoxId).find("small").css("display", "inline-block");
            $("#" + textBoxId).parent().siblings('small').css("display", "inline-block");
            $("#" + textBoxId).parent().siblings().find("b.textB").text(textValue);
            console.log(textValue + " " + textBoxId);
        }
    }

    //$("#listClient li").click(function () {
    //    var textDate = $(this).text();

    //})

    function openMenu(id) {
        $(id).toggleClass("open");
        $(id).parents(".col-lg-3").siblings().find("div.open").removeClass("open");
        //$(this).addClass("open");
    }


    $(document).click(function (event) {
        if (!$(event.target).closest('.btn-group').length) {
            if ($('.btn-group').hasClass("open")) {
                $('.btn-group').removeClass("open");
            }
        }
    })


    $("#listClient li").on("click", function () {
        var textData = $(this).text();
        console.log(textData);
        $(".clientSearch").show();
        $(".textB").text(textData);

    })

$(".clientListId li").click(function () {
    $(this).addClass('clientActive');
    $(this).siblings().removeClass('clientActive');
})

$(".startLocationId li").click(function () {
    $(this).addClass('locationActive');
    $(this).siblings().removeClass('locationActive');
})
$(".parDriverListId li").click(function () {
    $(this).addClass('DriverActive');
    $(this).siblings().removeClass('DriverActive');
})

$(".parVehicalListId li").click(function () {
    $(this).addClass('VehicalActive');
    $(this).siblings().removeClass('VehicalActive');
})

function deleteBlockTrip(tBlock) {
    var total = Number($(".total_cost_number").text());
    var blockId = $("#" + tBlock.id).parents(".col-lg-3").attr("id");
    var blockCost = Number($("#" + blockId).find("span.totalCost").text());
    console.log(blockCost);
    var totalCaluc = total - blockCost;
    $(".total_cost_number").text(totalCaluc);
    $("#" + tBlock.id).parents(".col-lg-3").addClass("Deleted");
    $("#" + blockId).hide(500);
    setTimeout(deleteBlockT, 2000);
    
}

function deleteBlockT() {
    console.log(0);
    $(".Deleted").remove();
}
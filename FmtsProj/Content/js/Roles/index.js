﻿var pageWidth = $("#gridContent").parent().width() - 102;

$(document).ready(function () {
    $("#editPermissionsModal").dialog(
        {
            autoOpen: false,
            width: 600
            //height: auto
        });
});

$("#jsGrid").jsGrid({
    width: "100%",
    height: "auto",
    inserting: false,
    editing: false,
    sorting: true,
    paging: true,
    filtering: true,
    autoload: true,
    pageLoading: true,

    pagerContainer: null,
    pageIndex: 1,
    pageSize: 20,
    pageButtonCount: 15,
    pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
    pagePrevText: "Prev",
    pageNextText: "Next",
    pageFirstText: "First",
    pageLastText: "Last",
    pageNavigatorNextText: "...",
    pageNavigatorPrevText: "...",

    controller: {
        loadData: function (filter) {
            var d = $.Deferred();
            $.ajax({
                type: "GET",
                url: baseURL + "/Roles/FindRolesGrid",
                data: filter,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function (response) {
                d.resolve({
                    data: response.data,
                    itemsCount: response.itemsCount
                });
            });
            return d.promise();
        }
    },
    fields: [
        { name: "RoleName", type: "text", title: "Name", editing: true, width: (pageWidth * (20 / 102)) },
        { name: "Description", type: "text", title: "Description", editing: true, width: (pageWidth * (50 / 102)) },
        {
            itemTemplate: function (value, item) {
                var strItem = JSON.stringify(item);
                var str;
                if (haspermissiontoEdit) {
                    str =
                        '<button class="btn btn-primary btn-md" onclick="showEditPermissionModal(' + item.RoleID + ')">Edit Permissions</button>' +
                        '  <a href ="/Roles/Edit?id=' + item.RoleID + '">' +
                        '<button class="btn btn-primary btn-md">Edit</button>' +
                        '</a>'
                        +
                        '  <button class="btn btn-primary btn-md" onclick="deleteItem(' + item.RoleID + ')">Delete</button>';
                }
                return (str);
            }, width: (pageWidth * (30 / 102))
        },
    ]
});

var grid = $("#jsGrid");

function deleteItem(roleId) {
    $("#confirmationDialog").text("Are you sure to delete this role?");
    $("#confirmationDialog").dialog({
        buttons: {
            "Confirm": function () {
                $("#confirmationDialog").dialog("close");
                $.ajax({
                    type: "GET",
                    url: baseURL + "/Roles/DeleteRole",
                    data: { roleId: roleId },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        ShowAlertMessage("The role item was successfully deleted", "success");
                        grid.jsGrid("loadData");
                    },
                    error: function (request, status, error) {
                        ShowAlertMessage("Error, Can't delete this role", "danger");
                    }
                });
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });
    $("#confirmationDialog").dialog("open");
}

var mainRoleId = 0;
function showEditPermissionModal(roleId) {
    mainRoleId = roleId;
    //clear the items before open dialog
    $("#check-list-box li").each(function (idx, li) {

        var id = li.id;
        $("#" + id).removeClass("ist-group-item-primary active");
        $("#" + id + " > span").removeClass("glyphicon-check");
        $("#" + id + " > span").addClass("glyphicon-unchecked");
    });

    $('#editPermissionsModal').dialog('open');
    $("#loaderOverall").show();

    $.ajax({
        type: "GET",
        url: baseURL + "/Roles/GetRolePermissions",
        data: { roleId: roleId },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $("#loaderOverall").hide();

            data.forEach(function (item) {
                var id = item.PermissionID;
                var ctrlId = "#per_" + id;
                $(ctrlId).addClass("ist-group-item-primary active");
                $(ctrlId + " > span").removeClass("glyphicon-unchecked");
                $(ctrlId + " > span").addClass("glyphicon-check");
            });

        },
        error: function (request, status, error) {
        }
    });
}

$("#btnPermissionsClose").click(function () {
    $('#editPermissionsModal').dialog('close');
});
$("#btnPermissionsSave").click(function () {
    $("#loaderOverall").show();
    var checkedItems = "";
    $("#check-list-box li.active").each(function (idx, li) {
        checkedItems = checkedItems + ',' + li.id.split("_")[1];
    });
    $.ajax({
        type: "POST",
        url: baseURL + "/Roles/SaveRolePermissions",
        data: JSON.stringify({ roleId: mainRoleId, permissionIds: checkedItems }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#loaderOverall").hide();
            ShowAlertMessage("The permissions were successfully saved", "success");
            $('#editPermissionsModal').dialog('close');
        },
        error: function (request, status, error) {
            ShowAlertMessage("Error, Can't save this permissions", "danger");
        }
    });
});



﻿using FmtsProj.Controllers;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static FmtsServices.Model.Enums;

namespace FmtsProj.Common
{
    public static class Common 
    {
        public static bool HasPermissions(PermissionsEnum permission)
        {
            UserModel CurrentUser = null;
            HttpContext ctx = System.Web.HttpContext.Current;
            if (ctx.Session["User"] != null)
            {
                CurrentUser = (UserModel)ctx.Session["User"];
            }
            if(CurrentUser != null)
            {
                var item = CurrentUser.Permissions.FirstOrDefault(a => a.Name.Contains(permission.ToString()));
                if (item != null)
                    return true;
            }

            return false;
        }
    }
}
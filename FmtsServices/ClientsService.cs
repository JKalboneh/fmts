﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class ClientsService
    {
        private BMOSContext db = new BMOSContext();

        public List<ClientModel> FindClinets(ClientsFilterModel model, out int count)
        {
            try
            {
                var query = db.Clients.AsQueryable();//.Include("VehicleAssignDrivers").Include("Tags").AsQueryable();

                #region Filter
                if (!String.IsNullOrEmpty(model.ClientName))
                {
                    query = query.Where(a => a.ClientName.Contains(model.ClientName));
                }
                if (!String.IsNullOrEmpty(model.PhoneNumber))
                {
                    query = query.Where(a => a.PhoneNumaber.Contains(model.PhoneNumber));
                }
                #endregion

                var list = query.Select(a => new ClientModel()
                {
                    ClientID = a.ClientID,
                    ClientName = a.ClientName,
                    PhoneNumaber = a.PhoneNumaber,
                    Adress = a.Adress
                }).OrderByDescending(a => a.ClientID).Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);
                count = query.Count();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ClientModel> FindClients(string filter, out int count, int page = 1, int pageSize = 15)
        {
            try
            {
                var query = db.Clients.AsQueryable();
                if (!String.IsNullOrEmpty(filter))
                    query = query.Where(a => a.ClientName.Contains(filter) || a.PhoneNumaber.Contains(filter));
                count = query.Count();
                var list = query.Select(a => new ClientModel()
                {
                    ClientID = a.ClientID,
                    ClientName = a.ClientName
                }).OrderBy(a => a.ClientName).Skip(pageSize * (page - 1)).Take(pageSize).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ClientModel FindByID(int clientId)
        {
            try
            {
                var query = db.Clients.Where(a => a.ClientID == clientId).AsQueryable();

                var item = query.Select(a => new ClientModel()
                {
                    ClientID = a.ClientID,
                    ClientName = a.ClientName,
                    PhoneNumaber = a.PhoneNumaber,
                    Adress = a.Adress
                }).FirstOrDefault();

                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddClient(ClientModel model)
        {
            try
            {
                var client = new Client();

                client.ClientName = model.ClientName;
                client.PhoneNumaber = model.PhoneNumaber;
                client.Adress = model.Adress;

                db.Clients.Add(client);
                db.SaveChanges(model.ByUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateClient(ClientModel model)
        {
            try
            {
                var client = db.Clients.Where(a => a.ClientID == model.ClientID).FirstOrDefault();

                if (client != null)
                {
                    client.ClientName = model.ClientName;
                    client.PhoneNumaber = model.PhoneNumaber;
                    client.Adress = model.Adress;

                    var entry = db.Entry(client);
                    entry.State = EntityState.Modified;
                    db.SaveChanges(model.ByUserID);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteClient(int clientId, int byUserId)
        {
            try
            {
                var client = db.Clients.FirstOrDefault(a => a.ClientID == clientId);
                db.Entry(client).State = EntityState.Deleted;
                db.SaveChanges(byUserId);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

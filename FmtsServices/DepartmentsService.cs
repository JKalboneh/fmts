﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FmtsServices
{
    public class DepartmentsService
    {
        private BMOSContext db = new BMOSContext();
        public List<DepartmentModel> FindDepartments(string filter, out int count, int page = 1, int pageSize = 15, int parCustomerid = 0)
        {
            try
            {
                var query = db.Departments.Include("Customer").Where(a => a.CustmerID == parCustomerid).AsQueryable();
                if (!String.IsNullOrEmpty(filter))
                    query = query.Where(a => a.Name.Contains(filter));
                count = query.Count();
                var list = query.Select(a => new DepartmentModel()
                {
                    DepartmentID = a.DepartmentID,
                    Name = a.Name,
                    CustmerID = a.CustmerID,
                    CustomerName = a.Customer.Name
                }).OrderBy(a => a.CustomerName).Skip(pageSize * (page - 1)).Take(pageSize).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DepartmentModel> FindDepartments(DepartmentFilterModel model, out int count)
        {
            try
            {
                var query = db.Departments.Include("Customer").AsQueryable();

                #region Filter
                if (!String.IsNullOrEmpty(model.Name))
                {
                    query = query.Where(a => a.Name.Contains(model.Name));
                }
                if (!String.IsNullOrEmpty(model.CustomerName))
                {
                    query = query.Where(a => a.Customer.Name.Contains(model.CustomerName));
                }
                #endregion

                var list = query.Select(a => new DepartmentModel()
                {
                    DepartmentID = a.DepartmentID,
                    Name = a.Name,
                    CustmerID = a.CustmerID,
                    CustomerName = a.Customer.Name
                }).OrderByDescending(a => a.DepartmentID).Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);
                count = query.Count();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DepartmentModel FindByID(int DepartmentId)
        {
            try
            {
                var query = db.Departments.Include("Customer").Where(a => a.DepartmentID == DepartmentId).AsQueryable();


                var item = query.Select(a => new DepartmentModel()
                {
                    DepartmentID = a.DepartmentID,
                    Name = a.Name,
                    CustmerID = a.CustmerID,
                    CustomerName = a.Customer.Name
                }).FirstOrDefault();

                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddDepartment(DepartmentModel model)
        {
            try
            {
                var Department = new Department();

                Department.Name = model.Name;
                Department.CustmerID = model.CustmerID;

                db.Departments.Add(Department);
                db.SaveChanges(model.ByUserID);
                model.DepartmentID = Department.DepartmentID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateDepartment(DepartmentModel model)
        {
            try
            {
                var Department = db.Departments.Where(a => a.DepartmentID == model.DepartmentID).FirstOrDefault();

                if (Department != null)
                {
                    Department.Name = model.Name;
                    Department.CustmerID = model.CustmerID;

                    var entry = db.Entry(Department);
                    entry.State = EntityState.Modified;
                    db.SaveChanges(model.ByUserID);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteDepartment(int DepartmentId,int byUserId)
        {
            try
            {
                var Department = db.Departments.FirstOrDefault(a => a.DepartmentID == DepartmentId);
                db.Entry(Department).State = EntityState.Deleted;
                db.SaveChanges(byUserId);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

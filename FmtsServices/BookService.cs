﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class BooksService
    {
        private BMOSContext db = new BMOSContext();
        
        public IList<BookDashBoardModel> GetBookDashBoardModel(int? parClientid, DateTime? parStartDateTime, DateTime? parEndDateTime, int? paraDriverId, int? paraVehicalid)
        {
            if (parClientid.HasValue)
            {
                IQueryable<BookDashBoardModel> aBookDashBoard = from bk in db.BookSchedules.Include("Client").Include("Location")
                                                                where bk.ID > 0 &&
                                                                bk.Status == "Active" &&
                                                                (!paraVehicalid.HasValue || bk.Trips.Any(tp => tp.VehicleID==paraVehicalid)) &&
                                                                (!paraDriverId.HasValue || bk.Trips.Any(tp=>tp.DriverTrips.Any(dt=>dt.DriverID==paraDriverId)))&&
                                                                 ((
                                                                    ( 
                                                                        (parStartDateTime.HasValue && parStartDateTime <= bk.StartDatetime) &&
                                                                        (parStartDateTime.HasValue && parStartDateTime <= bk.TillDate)
                                                                     ) ||
                                                                    (
                                                                        (parEndDateTime.HasValue && parEndDateTime >= bk.StartDatetime) &&
                                                                        (parEndDateTime.HasValue && parEndDateTime <= bk.TillDate)
                                                                    ) )
                                                                && (!parClientid.HasValue || bk.ClientID == parClientid))
                                                                select new BookDashBoardModel
                                                                {
                                                                    BookID = bk.ID,
                                                                    BookRef = bk.Book.BookRef,
                                                                    ClientName = bk.Client.ClientName,
                                                                    StartLocation = bk.Location.Name,
                                                                    Sat = bk.Sat.Value,
                                                                    Sun = bk.Sun.Value,
                                                                    Mon = bk.Mon.Value,
                                                                    Thu = bk.Thu.Value,
                                                                    Wed = bk.Wed.Value,
                                                                    Tue = bk.Tue.Value,
                                                                    Fri = bk.Fri.Value,
                                                                    EndDate = bk.TillDate.ToString()
                                                                };

                return aBookDashBoard.ToList();
            }else
            {
                IQueryable<BookDashBoardModel> aBookDashBoard = from bk in db.BookSchedules.Include("Trip").Include("Client").Include("Location")
                                                                where bk.ID > 0 && bk.Status == "Active" &&
                                                                (!paraVehicalid.HasValue || bk.Trips.Any(tp => tp.VehicleID == paraVehicalid)) &&
                                                                (!paraDriverId.HasValue|| bk.Trips.Any(tp => tp.DriverTrips.Any(dt => dt.DriverID == paraDriverId))) &&

                                                                (
                                                                    (
                                                                        (parStartDateTime.HasValue && parStartDateTime <= bk.StartDatetime) &&
                                                                        (parStartDateTime.HasValue && parStartDateTime <= bk.TillDate)
                                                                    ) ||
                                                                    (
                                                                        (parEndDateTime.HasValue && parEndDateTime >= bk.StartDatetime) &&
                                                                        (parEndDateTime.HasValue && parEndDateTime <= bk.TillDate)
                                                                        ) 
                                                                        )
                                                                select new BookDashBoardModel
                                                                {
                                                                    BookID = bk.ID,
                                                                    BookRef = bk.Book.BookRef,
                                                                    ClientName = bk.Client.ClientName,
                                                                    StartLocation = bk.Location.Name,
                                                                    //StartDatetime = bk.StartDatetime.ToString("dd-MM-yyyyy HH:mm"),
                                                                    EndLocation = bk.Trips.FirstOrDefault().Location.Name,
                                                                    Sat = bk.Sat.Value==true?true:false,
                                                                    Sun = bk.Sun.Value == true ? true : false,
                                                                    Mon = bk.Mon.Value == true ? true : false,
                                                                    Thu = bk.Thu.Value == true ? true : false,
                                                                    Wed = bk.Wed.Value == true ? true : false,
                                                                    Tue = bk.Tue.Value == true ? true : false,
                                                                    Fri = bk.Fri.Value == true ? true : false,
                                                                    EndDate = bk.TillDate.ToString()
                                                                };

                return aBookDashBoard.ToList();

            }
        }

        public List<BookBaseModel> FindBooks(BooksFilterModel model, out int count)
        {
            try
            {
                var query = db.BookSchedules.Include("Book").Include("Client").Include("Customer").Include("Department").AsQueryable();

                #region Filter
                if (!String.IsNullOrEmpty(model.BookRef))
                {
                    query = query.Where(a => a.Book.BookRef.Contains(model.BookRef));
                }
                if (!String.IsNullOrEmpty(model.ClientName))
                {
                    query = query.Where(a => a.Client.ClientName.Contains(model.ClientName));
                }
                if (!String.IsNullOrEmpty(model.CustomerName))
                {
                    query = query.Where(a => a.Customer.Name.Contains(model.CustomerName));
                }
                if (!String.IsNullOrEmpty(model.DepartmentName))
                {
                    query = query.Where(a => a.Department.Name.Contains(model.DepartmentName));
                }
                if (!String.IsNullOrEmpty(model.Status))
                {
                    query = query.Where(a => a.Status.Contains(model.Status));
                }
                #endregion

                var list = query.Select(a => new BookBaseModel()
                {
                    BookID = a.BookID,
                    BookScheduleID = a.ID,
                    BookRef = a.Book != null? a.Book.BookRef : "",
                    ClientName= a.Client != null ? a.Client.ClientName: "",
                    CustomerName = a.Customer != null ? a.Customer.Name: "",
                    DepartmentName = a.Department != null ? a.Department.Name : "",
                    TillDate = a.TillDate,
                    Status = a.Status.ToUpper()
                }).OrderByDescending(a => a.BookID).Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize).ToList();
                count = query.Count();
                list.ForEach(a => a.strTillDate = a.TillDate.HasValue ? a.TillDate.Value.ToString("dd/MM/yyyy") : "");
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }



}

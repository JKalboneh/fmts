﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class CustomersService
    {
        private BMOSContext db = new BMOSContext();
        public List<CustomerModel> FindCustomers(CustomerFilterModel model, out int count)
        {
            try
            {
                var query = db.Customers.AsQueryable();

                #region Filter
                if (!String.IsNullOrEmpty(model.Name))
                {
                    query = query.Where(a => a.Name.Contains(model.Name));
                }
                if (!String.IsNullOrEmpty(model.Description))
                {
                    query = query.Where(a => a.Description.Contains(model.Description));
                }
                #endregion

                var list = query.Select(a => new CustomerModel()
                {
                    CustomerID = a.CustomerID,
                    Name = a.Name,
                    Description = a.Description,
                    ClientID = a.ClientID
                }).OrderByDescending(a => a.CustomerID).Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);
                count = query.Count();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CustomerModel> FindCustomers(string filter, out int count, int page = 1, int pageSize = 15,int parClientId=0)
        {
            try
            {
                var query = db.Customers.Where(a=>a.ClientID.Value == parClientId).AsQueryable();
                if (!String.IsNullOrEmpty(filter))
                    query = query.Where(a => a.Name.Contains(filter));
                count = query.Count();
                var list = query.Select(a => new CustomerModel()
                {
                    CustomerID = a.CustomerID,
                    Name = a.Name,
                    Description = a.Description,
                    ClientID = a.ClientID
                }).OrderBy(a => a.CustomerID).Skip(pageSize * (page - 1)).Take(pageSize).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CustomerModel FindByID(int CustomerId)
        {
            try
            {
                var query = db.Customers.Where(a => a.CustomerID == CustomerId).AsQueryable();


                var item = query.Select(a => new CustomerModel()
                {
                    CustomerID = a.CustomerID,
                    Name = a.Name,
                    Description = a.Description,
                    ClientID = a.ClientID
                }).FirstOrDefault();

                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddCustomer(CustomerModel model)
        {
            try
            {
                var Customer = new Customer();
                
                Customer.Name = model.Name;
                Customer.Description = model.Description;
                Customer.ClientID = model.ClientID;

                db.Customers.Add(Customer);
                db.SaveChanges(model.ByUserID);
                model.CustomerID = Customer.CustomerID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateCustomer(CustomerModel model)
        {
            try
            {
                var Customer = db.Customers.Where(a => a.CustomerID == model.CustomerID).FirstOrDefault();

                if (Customer != null)
                {
                    Customer.Name = model.Name;
                    Customer.Description = model.Description;
                    Customer.ClientID = model.ClientID;

                    var entry = db.Entry(Customer);
                    entry.State = EntityState.Modified;
                    db.SaveChanges(model.ByUserID);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCustomer(int CustomerId, int byUserId)
        {
            try
            {
                var Customer = db.Customers.FirstOrDefault(a => a.CustomerID == CustomerId);
                db.Entry(Customer).State = EntityState.Deleted;
                db.SaveChanges(byUserId);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

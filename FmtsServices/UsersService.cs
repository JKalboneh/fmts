﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class UsersService
    {
        private BMOSContext db = new BMOSContext();
        public List<UserModel> FindUsers(UserFilterModel model, out int count)
        {
            try
            {
                var query = db.Users.AsQueryable();

                #region Filter
                if (!String.IsNullOrEmpty(model.FullName))
                {
                    query = query.Where(a => a.FullName.Contains(model.FullName));
                }
                if (!String.IsNullOrEmpty(model.UserName))
                {
                    query = query.Where(a => a.UserName.Contains(model.UserName));
                }
                if (!String.IsNullOrEmpty(model.Address))
                {
                    query = query.Where(a => a.Address.Contains(model.Address));
                }
                if (!String.IsNullOrEmpty(model.PhoneNumber))
                {
                    query = query.Where(a => a.PhoneNumber == model.PhoneNumber);
                }

                #endregion

                var list = query.Select(prepareModel).OrderByDescending(a => a.UserID).Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize).ToList();
                list.ForEach(c => c.Password = Encryption.Decrypt(c.Password));
                count = query.Count();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public UserModel FindByID(int userId)
        {
            try
            {
                var query = db.Users.Where(a => a.UserID == userId).AsQueryable();

                var item = query.Select(prepareModel).FirstOrDefault();
                item.Password = Encryption.Decrypt(item.Password);
                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public UserModel LoginUser(LoginModel model)
        {
            try
            {
                model.Password = Encryption.Encrypt(model.Password);
                var user = db.Users.Include("Roles").Where(a => a.UserName.Equals(model.Username) && a.Password.Equals(model.Password))
                    .Select(a => new UserModel()
                    {
                        UserID = a.UserID,
                        FullName = a.FullName,
                        UserName = a.UserName,
                        Address = a.Address,
                        Password = a.Password,
                        PhoneNumber = a.PhoneNumber,
                        Roles = a.Roles.Select(x => new RoleModel()
                        {
                            RoleID = x.RoleID,
                            RoleName = x.RoleName,
                            Description = x.Description
                        }).ToList(),
                        Permissions = a.Roles.SelectMany(x => x.Permissions).Select(y => new PermissionModel()
                        {
                            PermissionID = y.PermissionID,
                            Name = y.Name,
                            Description = y.Description,
                            GroupID = y.GroupID,
                            Sorting = y.Sorting
                        }).ToList()
                    })
                    .FirstOrDefault();

                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddUser(UserModel model)
        {
            try
            {
                var user = new User();
                user = prepareEntity(model);
                db.Users.Add(user);
                var aRole = db.Roles.FirstOrDefault(a => a.RoleID == model.RoleID);
                user.Roles.Add(aRole);
                db.SaveChanges(model.ByUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdateUser(UserModel model)
        {
            try
            {
                var user = db.Users.Where(a => a.UserID == model.UserID).FirstOrDefault();

                if (user != null)
                {
                    user.UserID = model.UserID;
                    user.FullName = model.FullName;
                    user.UserName = model.UserName;
                    user.Password = Encryption.Encrypt(model.Password);
                    user.PhoneNumber = model.PhoneNumber;
                    user.Address = model.Address;
                    var entry = db.Entry(user);
                    entry.State = EntityState.Modified;
                    var aRole = db.Roles.FirstOrDefault(a => a.RoleID == model.RoleID);
                    if (user.Roles.Any(r => r.RoleID > 0 && r.RoleID != model.RoleID))
                    {
                        user.Roles.Remove(user.Roles.FirstOrDefault(r => r.RoleID > 0 && r.RoleID != model.RoleID));
                        user.Roles.Add(aRole);
                    }
                    else if (!user.Roles.Any())
                    {
                        user.Roles.Add(aRole);
                    }

                    db.SaveChanges(model.ByUserID);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteUser(int userId, int byUserId)
        {
            try
            {
                var user = db.Users.Include(u=>u.Roles).FirstOrDefault(a => a.UserID == userId);
                db.Entry(user).State = EntityState.Deleted;
                if(user.Roles != null)
                {
                    var aRole = user.Roles.FirstOrDefault();
                    if(aRole != null)
                    {
                        var currentRole = db.Roles.FirstOrDefault(a => a.RoleID == aRole.RoleID);
                        user.Roles.Remove(currentRole);
                    }
                }
                db.SaveChanges(byUserId);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private UserModel prepareModel(User user)
        {
            UserModel userModel = new UserModel();
            userModel.UserID = user.UserID;
            userModel.FullName = user.FullName;
            userModel.UserName = user.UserName;
            userModel.Password = user.Password;
            userModel.PhoneNumber = user.PhoneNumber;
            userModel.Address = user.Address;
            var aRolresult = user.Roles.FirstOrDefault();
            if (aRolresult != null)
            {
                userModel.RoleName = aRolresult.RoleName;
                userModel.RoleID = aRolresult.RoleID;
            }
            return userModel;
        }

        private User prepareEntity(UserModel model)
        {
            User user = new User();
            user.FullName = model.FullName;
            user.UserName = model.UserName;
            user.Password = Encryption.Encrypt(model.Password);
            user.PhoneNumber = model.PhoneNumber;
            user.Address = model.Address;
            return user;
        }
    }
}

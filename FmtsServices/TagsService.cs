﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class TagsService
    {
        private BMOSContext db = new BMOSContext();

        public List<TagModel> FindTags(TagFilterModel model, out int count)
        {
            try
            {
                var query = db.Tags.AsQueryable();

                #region Filter
                if (!String.IsNullOrEmpty(model.Name))
                {
                    query = query.Where(a => a.Name.Contains(model.Name));
                }
                #endregion

                count = query.Count();
                var list = query.Select(a => new TagModel()
                {
                    TagID = a.TagID,
                    Name = a.Name,
                    Type = a.Type
                });

                return list.OrderByDescending(a=>a.TagID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TagModel> FindTags(string filter, out int count, int page = 1, int pageSize = 15)
        {
            try
            {
                var query = db.Tags.AsQueryable();
                if (!String.IsNullOrEmpty(filter))
                {
                        query = query.Where(a => a.Name.Contains(filter) );
                }
                count = query.Count();
                var list = query.Select(a => new TagModel()
                {
                    Name = a.Name,
                    TagID = a.TagID,
                    Type = a.Type
                }).OrderBy(a => a.Name).Skip(pageSize * (page - 1)).Take(pageSize).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public TagModel FindByID(int tagId)
        {
            try
            {
                var query = db.Tags.Where(a => a.TagID == tagId).AsQueryable();

                var item = query.Select(a => new TagModel()
                {
                    TagID = a.TagID,
                    Name = a.Name,
                    Type = a.Type
                }).FirstOrDefault();

                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void UpdateDriverTags(string strTags, int driverId, int byUserId)
        {
            try
            {
                int[] tagsIds = null;
                if (!String.IsNullOrEmpty(strTags))
                {
                    var strIds = strTags.Trim(',').Split(',');
                    tagsIds = Array.ConvertAll(strIds, s => int.Parse(s));
                }
                var driver = db.Drivers.Include("Tags").FirstOrDefault(a => a.DriverID == driverId);
                if (driver != null)
                {
                    var currentList = driver.Tags.ToList();

                    //remove deleted items
                    if (currentList != null && currentList.Count > 0)
                    {
                        foreach (var current in currentList)
                        {
                            if (tagsIds == null || (tagsIds != null && !tagsIds.Contains(current.TagID)))
                            {
                                driver.Tags.Remove(current);
                            }
                        }
                    }

                    //remove new items
                    if (tagsIds != null)
                    {
                        foreach (var tagId in tagsIds)
                        {
                            var item = currentList.FirstOrDefault(a => a.TagID == tagId);
                            if (item == null)
                            {
                                var tag = db.Tags.FirstOrDefault(a => a.TagID == tagId);
                                if (tag != null)
                                {
                                    driver.Tags.Add(tag);
                                }
                            }
                        }
                        
                    }

                    var entry = db.Entry(driver);
                    entry.State = EntityState.Modified;
                    db.SaveChanges(byUserId);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddTag(TagModel model)
        {
            try
            {
                var tag = new Tag();
                tag.Name = model.Name;
                db.Tags.Add(tag);
                db.SaveChanges(model.ByUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateTag(TagModel model)
        {
            try
            {
                var tag = db.Tags.Where(a => a.TagID == model.TagID).FirstOrDefault();

                if (tag != null)
                {
                    tag.Name = model.Name;

                    var entry = db.Entry(tag);
                    entry.State = EntityState.Modified;
                    db.SaveChanges(model.ByUserID);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteTag(int tagId, int byUserId)
        {
            try
            {
                var tag = db.Tags.FirstOrDefault(a => a.TagID == tagId);
                db.Entry(tag).State = EntityState.Deleted;
                db.SaveChanges(byUserId);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

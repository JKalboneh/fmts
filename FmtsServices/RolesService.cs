﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class RolesService
    {
        private BMOSContext db = new BMOSContext();
        
        public List<RoleModel> FindRoles(RoleFilterModel model, out int count)
        {
            try
            {
                var query = db.Roles.AsQueryable();

                #region Filter
                if(!String.IsNullOrEmpty(model.RoleName))
                {
                    query = query.Where(a => a.RoleName.Contains(model.RoleName));
                }
                if (!String.IsNullOrEmpty(model.Description))
                {
                    query = query.Where(a => a.Description.Contains(model.Description));
                }
                #endregion

                var list = query.Select(a => new RoleModel()
                {
                    RoleID = a.RoleID,
                    RoleName = a.RoleName,
                    Description = a.Description
                }).OrderByDescending(a => a.RoleID).Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);
                count = query.Count();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public RoleModel FindByID(int roleId)
        {
            try
            {
                var query = db.Roles.Where(a => a.RoleID == roleId).AsQueryable();

                var item = query.Select(a => new RoleModel()
                {
                    RoleID = a.RoleID,
                    RoleName = a.RoleName,
                    Description = a.Description
                }).FirstOrDefault();

                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveRole(RoleModel model)
        {
            try
            {
                var role = new Role();
                role.RoleName = model.RoleName;
                role.Description = model.Description;
                db.Roles.Add(role);
                db.SaveChanges(model.ByUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateRole(RoleModel roleModel)
        {
            var role = db.Roles.FirstOrDefault(a => a.RoleID == roleModel.RoleID);
            if (role != null)
            {
                role.RoleName = roleModel.RoleName;
                role.Description = roleModel.Description;

                var entry = db.Entry(role);
                entry.State = EntityState.Modified;
                db.SaveChanges(roleModel.ByUserID);
            }
        }

        public bool DeleteRole(int roleId, int byUserId)
        {
            try
            {
                var role = db.Roles.FirstOrDefault(a => a.RoleID == roleId);
                db.Entry(role).State = EntityState.Deleted;
                db.SaveChanges(byUserId);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
    }
}

﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class LocationsService
    {
        private BMOSContext db = new BMOSContext();

        public List<LocationModel> FindLocations(LocationFilterModel model, out int count)
        {
            try
            {
                var query = db.Locations.AsQueryable();

                #region Filter
                if (!String.IsNullOrEmpty(model.Name))
                {
                    query = query.Where(a => a.Name.Contains(model.Name));
                }
                if (!String.IsNullOrEmpty(model.Description))
                {
                    query = query.Where(a => a.Description.Contains(model.Description));
                }
                if (model.Number.HasValue)
                {
                    query = query.Where(a => a.Number == model.Number);
                }
                if (!String.IsNullOrEmpty(model.GoogleName))
                {
                    query = query.Where(a => a.GoogleName.Contains(model.GoogleName));
                }
                #endregion

                var list = query.Select(a => new LocationModel()
                {
                    LocationID = a.LocationID,
                    Number = a.Number,
                    Name = a.Name,
                    Description = a.Description,
                    GPS_Latitude = a.GPS_Latitude,
                    GPS_Longitude = a.GPS_Longitude,
                    GoogleName = a.GoogleName
                }).OrderByDescending(a => a.LocationID).Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);
                count = query.Count();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<LocationModel> FindLocations(string filter, out int count, int page = 1, int pageSize = 15)
        {
            try
            {
                var query = db.Locations.AsQueryable();
                if (!String.IsNullOrEmpty(filter))
                        query = query.Where(a => a.Name.Contains(filter));
                count = query.Count();
                var list = query.Select(a => new LocationModel()
                {
                    LocationID = a.LocationID,
                    Number = a.Number,
                    Name = a.Name,
                    Description = a.Description,
                    GPS_Latitude = a.GPS_Latitude,
                    GPS_Longitude = a.GPS_Longitude,
                    GoogleName = a.GoogleName
                }).OrderBy(a => a.Name).Skip(pageSize * (page - 1)).Take(pageSize).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public LocationModel FindByID(int locationId)
        {
            try
            {
                var query = db.Locations.Where(a => a.LocationID == locationId).AsQueryable();


                var item = query.Select(a => new LocationModel()
                {
                    LocationID = a.LocationID,
                    Number = a.Number,
                    Name = a.Name,
                    Description = a.Description,
                    GPS_Latitude = a.GPS_Latitude,
                    GPS_Longitude = a.GPS_Longitude,
                    GoogleName = a.GoogleName
                }).FirstOrDefault();
                
                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddLocation(LocationModel model)
        {
            try
            {
                var location = new Location();
                location.Number = model.Number;
                location.Name = model.Name;
                location.Description = model.Description;
                location.GoogleName = model.GoogleName;
                location.GPS_Latitude = model.GPS_Latitude;
                location.GPS_Longitude = model.GPS_Longitude;
                db.Locations.Add(location);
                db.SaveChanges(model.ByUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateLocation(LocationModel model)
        {
            try
            {
                var location = db.Locations.Where(a=>a.LocationID == model.LocationID).FirstOrDefault();

                if(location != null)
                {
                    location.Number = model.Number;
                    location.Name = model.Name;
                    location.Description = model.Description;
                    location.GoogleName = model.GoogleName;
                    location.GPS_Latitude = model.GPS_Latitude;
                    location.GPS_Longitude = model.GPS_Longitude;

                    var entry = db.Entry(location);
                    entry.State = EntityState.Modified;
                    db.SaveChanges(model.ByUserID);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteLocation(int locationId, int byUserId)
        {
            try
            {
                var location = db.Locations.FirstOrDefault(a=>a.LocationID == locationId);
                db.Entry(location).State = EntityState.Deleted;
                db.SaveChanges(byUserId);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }



}

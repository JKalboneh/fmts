﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FmtsServices.Model.Enums;

namespace FmtsServices
{
    public class PermissionService
    {
        private BMOSContext db = new BMOSContext();

        public IList<PermissionModel> GetAll()
        {
            var query = db.Permissions;

            var list = query.Select(a => new PermissionModel()
            {
                PermissionID = a.PermissionID,
                Name = a.Name,
                Description = a.Description,
                GroupID = a.GroupID,
                Sorting = a.Sorting
            }).OrderBy(a => a.GroupID).OrderBy(a => a.Sorting);

            return list.ToList();
        }

        public IList<PermissionGroupsModel> GetGroupsModelList()
        {
            var PermissionsList = GetAll();

            var permissionsGroupEnums = Enum.GetValues(typeof(PermissionsGroupsEnum)).Cast<PermissionsGroupsEnum>();
            var permissionGroupsList = new List<PermissionGroupsModel>();

            foreach (var groupEnum in permissionsGroupEnums)
            {
                var permissionGroupsModel = new PermissionGroupsModel();
                permissionGroupsModel.PermissionGroupID = (int)groupEnum;
                permissionGroupsModel.Name = groupEnum.ToString();
                permissionGroupsModel.Permissions = PermissionsList.Where(a => a.GroupID == (int)groupEnum).ToList();
                permissionGroupsList.Add(permissionGroupsModel);
            }

            return permissionGroupsList;
        }

        public IList<PermissionModel> GetByRoleID(int roleId)
        {
            var roles = db.Roles.Include("Permissions").FirstOrDefault(a => a.RoleID == roleId);
            var permissions = roles.Permissions.ToList();
            var list = permissions.Select(a => new PermissionModel()
            {
                PermissionID = a.PermissionID,
                Name = a.Name,
                Description = a.Description,
                GroupID = a.GroupID,
                Sorting = a.Sorting
            }).OrderBy(a => a.GroupID).OrderBy(a => a.Sorting);

            return list.ToList();
        }

       
        public void UpdateRolePermissions(int roleId, string permissionIds,int byUserId)
        {
            try
            {
                int[] arrPermissionsIds = null;
                if (!String.IsNullOrEmpty(permissionIds))
                {
                    var strIds = permissionIds.Trim(',').Split(',');
                    arrPermissionsIds = Array.ConvertAll(strIds, s => int.Parse(s));
                }

                var currentRole = db.Roles.Include("Permissions").FirstOrDefault(a => a.RoleID == roleId);
                //var currentList = roles.Permissions.ToList();
                var deletedPermissions = new List<Permission>();
                //remove deleted items
                foreach (var current in currentRole.Permissions)
                {
                    if (arrPermissionsIds == null || (arrPermissionsIds != null && !arrPermissionsIds.Contains(current.PermissionID)))
                    {
                        deletedPermissions.Add(current);
                    }
                }
                foreach(var deletedPermission in deletedPermissions)
                {
                    currentRole.Permissions.Remove(deletedPermission);
                    db.Entry(currentRole).State = EntityState.Modified;
                    db.SaveChanges(byUserId);
                }

                //remove new items
                if (arrPermissionsIds != null)
                {
                    foreach (var permissionsId in arrPermissionsIds)
                    {
                        var current = currentRole.Permissions.FirstOrDefault(a => a.PermissionID == permissionsId);
                        if (current == null)
                        {
                            var item = db.Permissions.FirstOrDefault(a=>a.PermissionID == permissionsId);
                            currentRole.Permissions.Add(item);
                            db.SaveChanges(byUserId);
                        }
                    }
                }

                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

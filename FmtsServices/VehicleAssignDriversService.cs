﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class VehicleAssignDriversService
    {
        private BMOSContext db = new BMOSContext();
        public void UpdateByVehicleIds(string vehicleIds, int driverId, int byUserId)
        {
            try
            {
                int[] vehiclesIds = null;
                if (!String.IsNullOrEmpty(vehicleIds))
                {
                    var strIds = vehicleIds.Trim(',').Split(',');
                    vehiclesIds = Array.ConvertAll(strIds, s => int.Parse(s));
                }

                var currentList = db.VehicleAssignDrivers.Where(a => a.DriverID == driverId).ToList();

                //remove deleted items
                foreach (var current in currentList)
                {
                    if (vehiclesIds == null || (vehiclesIds != null && !vehiclesIds.Contains(current.VehicleID)))
                    {
                        db.Entry(current).State = EntityState.Deleted;
                        db.SaveChanges(byUserId);
                    }
                }
                //remove new items
                if(vehiclesIds != null)
                {
                    foreach (var vehicleId in vehiclesIds)
                    {
                        var item = currentList.FirstOrDefault(a => a.VehicleID == vehicleId);
                        if (item == null)
                        {
                            VehicleAssignDriver vehicleAssignDriver = new VehicleAssignDriver();
                            vehicleAssignDriver.DriverID = driverId;
                            vehicleAssignDriver.VehicleID = vehicleId;
                            vehicleAssignDriver.AssignDate = DateTime.Now;
                            db.VehicleAssignDrivers.Add(vehicleAssignDriver);
                            db.SaveChanges(byUserId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateByDriverIds(string drivers, int vehicleId, int byUserId)
        {
            try
            {
                int[] driversIds = null;
                if (!String.IsNullOrEmpty(drivers))
                {
                    var strIds = drivers.Trim(',').Split(',');
                    driversIds = Array.ConvertAll(strIds, s => int.Parse(s));
                }

                var currentList = db.VehicleAssignDrivers.Where(a => a.VehicleID == vehicleId).ToList();

                //remove deleted items
                if(currentList != null && currentList.Count >0)
                {
                    foreach (var current in currentList)
                    {
                        if (driversIds == null || (driversIds != null && !driversIds.Contains(current.DriverID)))
                        {
                            db.Entry(current).State = EntityState.Deleted;
                        }
                    }
                    db.SaveChanges(byUserId);
                }
                

                //remove new items
                if (driversIds != null)
                {
                    foreach (var driverId in driversIds)
                    {
                        var item = currentList.FirstOrDefault(a => a.DriverID == driverId);
                        if (item == null)
                        {
                            VehicleAssignDriver vehicleAssignDriver = new VehicleAssignDriver();
                            vehicleAssignDriver.DriverID = driverId;
                            vehicleAssignDriver.VehicleID = vehicleId;
                            vehicleAssignDriver.AssignDate = DateTime.Now;
                            db.VehicleAssignDrivers.Add(vehicleAssignDriver);
                            db.SaveChanges(byUserId);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

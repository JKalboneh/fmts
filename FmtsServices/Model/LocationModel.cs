﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class LocationModel : BaseModel
    {
        public int LocationID { get; set; }
        [Required]
        public long? Number { get; set; }
        [Required]
        [DisplayName("Name")]
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(400)]
        public string Description { get; set; }
        [DisplayName("Google Name")]
        [MaxLength(150)]
        public string GoogleName { get; set; }
        [DisplayName("GPS Latitude")]
        public decimal? GPS_Latitude { get; set; }
        [DisplayName("GPS Longitude")]
        public decimal? GPS_Longitude { get; set; }

    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class Enums
    {
        public enum PermissionsEnum
        {
            DashBoardViewPage = 1,
            DashBoardSearch = 2,
            DashBoardExportReport = 3,
            DriversViewPage = 4,
            DriversCreate = 5,
            DriversEdit = 6,
            DriversExportReport = 7,
            VehiclesViewPage = 8,
            VehiclesCreate = 9,
            VehiclesEdit = 10,
            VehiclesExportReport = 11,
            BookingViewPage = 12,
            BookingCreate = 13,
            BookingEdit = 14,
            BookingExportReport = 15,
            ClientViewPage = 16,
            ClientCreate = 17,
            ClientEdit = 18,
            ClientExportReport = 19,
            ReportsViewPage = 20,
            SettingsViewPage = 21,
        }
        public enum PermissionsGroupsEnum
        {
            DashBoard = 1,
            Drivers = 2,
            Vehicles = 3,
            Booking = 4,
            Client = 5,
            Reports = 6,
            Settings = 7
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class DriverDashboardInfoModel
    {
        public double DriverUtilization = 0;
        public List<DriverDashboardModel> DriversInfoList = new List<DriverDashboardModel>();

    }
}

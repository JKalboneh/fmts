﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class NotificationsFilterModel : GridModel
    {
        public string NewValue { set; get; }
        public string ColumnName  { set; get; }
        public string UserName { set; get; }
    }


}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{

    public class BookBaseModel
    {
        
        public int? BookID { get; set; }
        public int BookScheduleID { get; set; }
        public string BookRef { get; set; }
        public string ClientName { get; set; }
        public string CustomerName { get; set; }
        public string DepartmentName { get; set; }
        public string Status { get; set; }
        public DateTime?  TillDate { get; set; }
        public string strTillDate{ get; set; }
    }

}

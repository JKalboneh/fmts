﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class LocationFilterModel : GridModel
    {
        public int LocationID { get; set; }
        public long? Number { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string GoogleName { get; set; }
        public decimal? GPS_Latitude { get; set; }
        
    }


}

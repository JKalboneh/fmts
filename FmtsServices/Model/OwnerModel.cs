﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class OwnerModel
    {
        public int ID { get; set; }
        public string OwnerName { get; set; }
        public string Type { get; set; }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FmtsServices.Model
{
    public class PermissionModel
    {
        public int PermissionID { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
        public int GroupID { get; set; }
        public decimal? Sorting { get; set; }
    }

    public class PermissionGroupsModel
    {
        public int PermissionGroupID { get; set; }
        public string Name { get; set; }
        public IList<PermissionModel> Permissions { get; set; }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{

    public class BookSchedulesHisModel
    {
        public int BookHistoryID { get; set; }
        public string BookRefNumID { get; set; }
        public string Status { get; set; }
    }

}

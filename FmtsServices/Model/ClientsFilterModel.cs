﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class ClientsFilterModel : GridModel
    {
        public string ClientName { set; get; }
        public string PhoneNumber { set; get; }
    }


}

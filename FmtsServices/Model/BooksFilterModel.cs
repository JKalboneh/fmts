﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class BooksFilterModel : GridModel
    {
        public string BookRef { set; get; }
        public string ClientName { set; get; }
        public string CustomerName { set; get; }
        public string DepartmentName { set; get; }
        public string TillDateStr { set; get; }
        public string Status { set; get; }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class BookResult
    {
        public int IDBook { get; set; }
        public int IDBookSch { get; set; }
        public string BookRefNum { get; set; }
        public string SchRefnum { get; set; }
    }

}

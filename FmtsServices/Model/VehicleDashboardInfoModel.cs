﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class VehicleDashboardInfoModel
    {
        public double VehicleUtilization { set; get; }
        public List<VehicleDashboardModel> VehiclesInfoList = new List<VehicleDashboardModel>();

    }
}

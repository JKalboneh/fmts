﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class VehiclesModel : BaseModel
    {
        public int BusyInSameTrip { get; set; }
        public int DriverTagsMissmatch { get; set; }
        public string WorkingDays { get; set; }
        public bool IsVehicalrWorking { get; set; }
        public IEnumerable<int> WorkingDaysList
        {
            get
            {
                return WorkingDays == null || WorkingDays == "" ? null : WorkingDays.Split(',').Select(int.Parse);
            }
        }

        public int VehicalOutTime { get; set; }
        public int ID { get; set; }
        [Required]
        [DisplayName("Number")]
        [MaxLength(100)]
        public string VehicleNum { get; set; }
        public int? SeatCount { get; set; }
        [DisplayName("Color")]
        [MaxLength(50)]
        public string VehicleColor { get; set; }
        [DisplayName("Status")]
        [MaxLength(50)]
        public string VehicleStatus { get; set; }
        [DisplayName("Model")]
        [MaxLength(50)]
        public string VehicleModel { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        [DisplayName("Make Year")]
        [MaxLength(50)]
        public string MakeYear { get; set; }
        [DisplayName("Chasis Number")]
        [MaxLength(50)]
        public string ChasisNumber { get; set; }
        [DisplayName("Make")]
        [MaxLength(50)]
        public string Make { get; set; }
        [DisplayName("Owner")]
        public int? OwnerID { get; set; }
        public string OwnerName { get; set; }
        public int Type { get; set; }

        public IEnumerable<DriverModel> DriverAssign { get; set; }

    }
}

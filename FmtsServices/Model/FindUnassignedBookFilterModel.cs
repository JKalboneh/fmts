﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class FindUnassignedBookFilterModel : GridModel
    {
        public string paraDateTime { set; get; }
        public string paraToTime { set; get; }
        public AttbuteObject paraClientId { set; get; }
        public AttbuteObject paraDriverId { set; get; }
        public AttbuteObject paraVehicaleId { set; get; }
    }


}

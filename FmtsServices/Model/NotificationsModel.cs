﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class NotificationsModel : BaseModel
    {
        public List<NotificationsItemModel> items{ get; set; }
        public int counter { get; set; }
    }
        public class NotificationsItemModel : BaseModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string EventDateUTC { get; set; }
        public string EventType { get; set; }
        public string TableName { get; set; }
        public string RecordId { get; set; }
        public string ColumnName { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
    //   public virtual User User { get; set; }

}
}

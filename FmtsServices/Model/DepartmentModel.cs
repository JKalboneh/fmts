﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class DepartmentModel : BaseModel
    {
        public int DepartmentID { get; set; }
        [Required]
        [DisplayName("Name")]
        [MaxLength(100)]
        public string Name { get; set; }
        public Nullable<int> CustmerID { get; set; }
        [DisplayName("Customer Name")]
        public string CustomerName { get; set; }
    }
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class TagFilterModel : GridModel
    {
        public string TagID { set; get; }
        public string Name { set; get; }
        public int? Type { set; get; }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class BookDashBoardModel
    {
        public int BookScheduleID { get; set; }
        public int BookID { get; set; }
        public string BookRef { get; set; }
        public string ClientName { get; set; }
        public string StartDatetime { get; set; }
        public string StartLocation { get; set; }
        public string EndLocation { get; set; }
        public string EndDate { get; set; }
        public bool Sat { get; set; }
        public bool Sun { get; set; }
        public bool Mon { get; set; }
        public bool Thu { get; set; }
        public bool Wed { get; set; }
        public bool Tue { get; set; }
        public bool Fri { get; set; }

        public string[] SchedDays
        {
            get
            {
                List<string> tempArrau = new List<string>();
                if (Sat)
                    tempArrau.Add("Sat");
                if (Sun)
                    tempArrau.Add("Sun");
                if (Mon)
                    tempArrau.Add("Mon");
                if (Thu)
                    tempArrau.Add("Thu");
                if (Wed)
                    tempArrau.Add("Wed");
                if (Tue)
                    tempArrau.Add("Tue");
                if (Fri)
                    tempArrau.Add("Fri");
                return tempArrau.ToArray();
            }
        }
    }
}

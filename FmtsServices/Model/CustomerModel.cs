﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class CustomerModel:BaseModel
    {

        public int CustomerID { get; set; }
        [Required]
        [DisplayName("Name")]
        [MaxLength(200)]
        public string Name { get; set; }
        [DisplayName("Description")]
        [MaxLength(500)]
        public string Description { get; set; }
        public Nullable<int> ClientID { get; set; }
        
    }
    
}

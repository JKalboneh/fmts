﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class DriverDashboardModel : DriverModel
    {
       
        public int DriverScheduleID { get; set; }
        public int TripCount { get; set; }
        public double FreeTime { get; set; }
        public double TotalHourtripAct { get; set; }
        public double TotalHourTripSch { get; set; }
        public double TotalOutTime { get; set; }
    }
}
   

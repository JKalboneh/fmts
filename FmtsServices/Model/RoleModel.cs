﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FmtsServices.Model
{
    public class RoleModel : BaseModel
    {
        public int RoleID { get; set; }
        [Required]
        [DisplayName("Name")]
        public string RoleName { get; set; }
        public string Description { get; set; }

    }
}


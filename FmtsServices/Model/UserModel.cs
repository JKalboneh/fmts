﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FmtsServices.Model
{
    public class UserModel : BaseModel
    {
        public int UserID { get; set; }
        [Required]
        [DisplayName("Full Name")]
        [MaxLength(150)]
        public string FullName { get; set; }
        [Required]
        [DisplayName("User Name")]
        [MaxLength(100)]
        public string UserName { get; set; }
    [Required]
        [DisplayName("Password")]
        [MaxLength(100)]
        public string Password { get; set; }
        [DisplayName("Address")]
        [MaxLength(200)]
        public string Address { get; set; }
        [DisplayName("Phone Number")]
        [MaxLength(50)]
        public string PhoneNumber { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }

        public IList<RoleModel> Roles { get; set; }

        public IList<PermissionModel> Permissions { get; set; }
    }
}


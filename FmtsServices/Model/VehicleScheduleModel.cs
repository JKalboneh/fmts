﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class VehicleScheduleModel
    {
        public int ID { set; get; }
        public string VehicleNum { set; get; }
        public int VehiclesID { set; get; }
        public string PersonalNumber { set; get; }
        public int DriverID { set; get; }
        
        public string WorkingDays { set; get; }
        public Nullable<System.TimeSpan> WorkingHourFrom { set; get; }
        public Nullable<System.TimeSpan> WorkingHourTo { set; get; }


    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class VehicleDashboardModel : VehiclesModel
    {
        public int TripCount { get; set; }
        public double FreeTime { get; set; }
        public double TotalHourTripAct { get; set; }
        public double TotalHourTripSch { get; set; }
        public double TotalOutTime { get; set; }

    }
}
   

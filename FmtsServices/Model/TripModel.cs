﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class TripModel
    {
        public TripModel()
        {
            EndLocid = new AttbuteObject();
            DriverID = new AttbuteObject();
            VehicleID = new AttbuteObject();
            OverWriteLocID = new AttbuteObject();
            OverWriteLocName = new AttbuteObject();
        }
        public int BookID { get; set; }
        public AttbuteObject EndLocid { get; set; }
        public AttbuteObject DriverID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public double SalikFees { get; set; }
        public double DriverFees { get; set; }
        public double CarFuelCost { get; set; }
        public double ParkingFees { get; set; }
        public double Consumption { get; set; }
        public double Other { get; set; }
        public string OccursOn { get; set; }
        public string Rec_Type { get; set; }
        public string RepeatEvery { get; set; }
        public string TripName { get; set; }

        public AttbuteObject VehicleID { get; set; }
        public int TripID { get; set; }
        public int DriverTrip { get; set; }
        public string DriverTripName { get; set; }
        public AttbuteObject OverWriteLocID { get; set; }
        public AttbuteObject OverWriteLocName { get; set; }
    }
}

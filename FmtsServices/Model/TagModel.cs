﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class TagModel : BaseModel
    {
        public int TagID { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
       
    }
}

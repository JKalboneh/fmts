﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class DriverScheduleModel 
    {
        public int ID { set; get; }
        public string DriverDate { set; get; }
        public string DriverName { set; get; }
        public string PersonalNumber { set; get; }
        public int DriverID { set; get; }
        
        public string WorkingDays { set; get; }
        public Nullable<System.TimeSpan> WorkingHourFrom { set; get; }
        public Nullable<System.TimeSpan> WorkingHourTo { set; get; }
    }


}

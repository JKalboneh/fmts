﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class DriverModel : BaseModel
    {
        public List<KeyValuePair<string, string>> OverAllColor
        {
            get
            {
                var list = new List<KeyValuePair<string, string>>();
                list.Add(new KeyValuePair<string, string>("DriverinVactionColor", DriverinVactionColor.ToString()));
                list.Add(new KeyValuePair<string, string>("InTimeColorCode", InTimeColorCode.ToString()));
                list.Add(new KeyValuePair<string, string>("VehicalTags", VehicalTagsCounter.ToString()));
                list.Add(new KeyValuePair<string, string>("IsDriverWorking", IsDriverWorking.ToString()));

                return list;
            }
        }
        public double ColorSorting
        {
            get
            {
                return VehicalTagsCounter + DriverinVactionColor + InTimeColorCode;
            }
        }
        public double ColorSortingValue { get; set; }

        public int DriverinVactionColor { get; set; }
        public int VehicalTagsCounter { get; set; }
        public string WorkingDays { get; set; }
        public IEnumerable<int> WorkingDaysList
        {
            get
            {
                return WorkingDays==null || WorkingDays ==""? null:WorkingDays.Split(',').Select(int.Parse);
            }
        }
        public bool IsDriverWorking { get; set; }
        public int InTimeColorCode { get; set; }
        public int DriverID { get; set; }
        [Required]
        [DisplayName("Full Name")]
        [MaxLength(100)]
        public string FullName { get; set; }
        [Required]
        [DisplayName("Personal Number")]
        [MaxLength(50)]
        public string PersonalNumber { get; set; }
        [Required]
        [DisplayName("Joining Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime JoiningDate { get; set; }
        public string strJoiningDate { get { return this.JoiningDate.ToString("dd/MM/yyyy"); } }
        [MaxLength(50)]
        public string Nationality { get; set; }
        [DisplayName("Emarites Date Expairy")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EmaritesIdExpairy { get; set; }
        [DisplayName("Passport Number")]
        [MaxLength(50)]
        public string PassportNumber { get; set; }
        [DisplayName("Medical Status")]
        [MaxLength(50)]
        public string MedicalStatus { get; set; }
        [MaxLength(200)]
        public string Address { get; set; }
        [DisplayName("Driver Emarites ID")]
        [MaxLength(50)]
        public string DriverEmaritesID { get; set; }
        [DisplayName("Reference Name")]
        [MaxLength(50)]
        public string ReferenceName { get; set; }
        [MaxLength(200)]
        public string Status { get; set; }

        public List<int> VehicalTagsList { get; set; }

        public IEnumerable<VehiclesModel> VehicleAssign { get; set; }

        public IEnumerable<TagModel> Tags { get; set; }

        public string strVehicles { get; set; }
        public string strTags { get; set; }
    }
}

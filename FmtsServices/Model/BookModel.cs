﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{

    public class BookModel
    {
        public BookModel()
        {
            ClientID = new AttbuteObject();
            StartLoc = new AttbuteObject();
            CustomerID = new AttbuteObject();
            DepartmentID = new AttbuteObject();
            ContactType = new AttbuteObject();
        }
        public int? BookID { get; set; }
        public int ScheduleID { get; set; }
        public string BookRef { get; set; }
        public string SchedulesRefNum { get; set; }
        public AttbuteObject ClientID { get; set; }
        public string ClientName { get; set; }
        public string CustomerName { get; set; }
        public string DepartmentName { get; set; }
        public string Status { get; set; }
        public int CreatedBy { get; set; }
        public string StartDatetime { get; set; }
        public string inputTillDay { get; set; }
        public AttbuteObject StartLoc { get; set; }
        public short? RecType { get; set; }
        public string TillDateStr { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime?  TillDate { get; set; }
        public string[] SchedDays { get; set; }
        public decimal TotalCost { get; set; }
        public AttbuteObject CustomerID { get; set; }
        public AttbuteObject DepartmentID { get; set; }
        public AttbuteObject ContactType { get; set; }
        public IList<TripModel> TripList { get; set; }
        public IList<BookSchedulesHisModel> bookHistory { get; set; }
    }

}

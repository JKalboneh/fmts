﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class VehicleFilterModel : GridModel
    {
        public string VehicleNum { get; set; }
        public int? SeatCount { get; set; }
        public string VehicleColor { get; set; }
        public string VehicleStatus { get; set; }
        public string MakeYear { get; set; }
        public string ChasisNumber { get; set; }
        public string Make { get; set; }
        public int OwnerID { get; set; }
        public string OwnerName { get; set; }
    }


}

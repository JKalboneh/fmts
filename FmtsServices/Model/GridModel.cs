﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class GridModel
    {
        private int _pageSize = 20;
        private int _pageIndex = 1;

        public int pageSize { get { return _pageSize; } set { _pageSize = value; } }
        public int pageIndex { get { return _pageIndex; } set { _pageIndex = value; } }

    }
    

}

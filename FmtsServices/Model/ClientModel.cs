﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class ClientModel : BaseModel
    {
        public int ClientID { get; set; }
        [Required]
        [DisplayName("Client Name")]
        [MaxLength(50)]
        public string ClientName { get; set; }
        [Required]
        [DisplayName("Adress")]
        [MaxLength(200)]
        public string Adress { get; set; }
        public int CreatedBy { get; set; }
        [DisplayName("Phone Numaber")]
        [MaxLength(50)]
        public string PhoneNumaber { get; set; }
    }
}

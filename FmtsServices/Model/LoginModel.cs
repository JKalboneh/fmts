﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FmtsEntity;
using System.ComponentModel.DataAnnotations;

namespace FmtsServices.Model
{
    public class LoginModel
    {
        [Required(ErrorMessage ="Username rquired",AllowEmptyStrings =false)]
        public string Username { set; get; }
        [Required(ErrorMessage = "Password rquired", AllowEmptyStrings = false)]
        public string Password { set; get; }
        public bool RememberMe { set; get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices.Model
{
    public class DriverFilterModel : GridModel
    {
        public string FullName { set; get; }
        public string PersonalNumber { set; get; }
    }


}

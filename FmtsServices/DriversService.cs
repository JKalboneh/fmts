﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class DriversService
    {
        private BMOSContext db = new BMOSContext();

        public List<DriverModel> FindDrivers(DriverFilterModel model, out int count)
        {
            try
            {
                var query = db.Drivers.Include("VehicleAssignDrivers").Include("Tags").AsQueryable();

                #region Filter
                if (!String.IsNullOrEmpty(model.FullName))
                {
                    query = query.Where(a => a.FullName.Contains(model.FullName));
                }
                if (!String.IsNullOrEmpty(model.PersonalNumber))
                {
                    query = query.Where(a => a.PersonalNumber.Contains(model.PersonalNumber));
                }
                #endregion

                var list = query.Select(prepareModel).OrderByDescending(a => a.DriverID).Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);
                count = query.Count();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DriverModel FindByID(int driverId)
        {
            try
            {
                var query = db.Drivers.Include("VehicleAssignDrivers").Include("Tags").Where(a => a.DriverID == driverId).AsQueryable();

                var item = query.Select(prepareModel).FirstOrDefault();

                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int FetchColorType;
        public List<DriverModel> FindDrivers(

            string filter,
            out int count,
            int Type = 1,
            int page = 1,
            int pageSize = 15,
            string parTagsList = "",
            int parVehicleNumid = 0,
            string parStatDate = "",
            string parEndDate = "",
            int parCurrentStopDBId = 0)
        {
            try
            {
                List<DriverModel> aGrenlist = new List<DriverModel>();
                aGrenlist = GetDriverModelList(filter, out count, page, pageSize, parTagsList, parVehicleNumid, parStatDate, parEndDate, parCurrentStopDBId);
                return aGrenlist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<DriverModel> GetDriverModelList(string filter,
        out int count,
        int page = 1,
        int pageSize = 15,
        string parTagsList = "",
        int parVehicleNumid = 0,
        string parStatDate = "",
        string parEndDate = "",
        int parCurrentStopDBId = 0)
        {
            try
            {
                DateTime startDate = new DateTime();
                if (parStatDate != "")
                    startDate = DateTime.ParseExact(parStatDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

                DateTime endDate = new DateTime();
                if (parEndDate != "")
                    endDate = DateTime.ParseExact(parEndDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

                List<string> tageList = new List<string>();
                if (parTagsList != "")
                    tageList = parTagsList.Split(',').ToList();

                List<Tag> aTagVehList = new List<Tag>();
                if (parVehicleNumid > 0)
                {
                    var quryVehicle = db.Tags.AsQueryable().Where(tg => tg.Vehicles.Any(veh => veh.ID == parVehicleNumid));
                    aTagVehList = quryVehicle.ToList();
                }
                
                var query = db.Drivers.AsQueryable();

                query = query.Where(dr=>dr.DriverSchedules.Any(drSch=>(startDate.TimeOfDay>= drSch.WorkingHourFrom && startDate.TimeOfDay<= drSch.WorkingHourTo) &&
                (endDate.TimeOfDay <= drSch.WorkingHourTo && endDate.TimeOfDay  >= drSch.WorkingHourFrom)));


                if (!String.IsNullOrEmpty(filter))
                {
                    if (parTagsList.Length <= 0)
                        query = query.Where(a => a.FullName.Contains(filter));
                    else
                        query = query.Where(a => a.FullName.Contains(filter) &&
                        (a.Tags.Any(t => t.TagID > 0 && tageList.Contains(t.TagID.ToString()))));

                }
                else
                {
                    if (tageList.Count > 0)
                        query = query.Where(a => a.Tags.Any(t => t.TagID > 0 && tageList.Contains(t.TagID.ToString())));
                }
                
                List<int> daysOfDateRange = new List<int>();
                DateTime acurrentdate = startDate;
                while (acurrentdate <= endDate)
                {
                    if (!daysOfDateRange.Contains(acurrentdate.DayOfWeek.GetHashCode()))
                        daysOfDateRange.Add(acurrentdate.DayOfWeek.GetHashCode());
                    acurrentdate = acurrentdate.AddDays(1);
                }
                //IEnumerable<DateTime> days = Enumerable.Range(startDate, endDate);

                var list = query.Select(a => new DriverModel()
                {
                    WorkingDays = a.DriverSchedules.FirstOrDefault() != null ? a.DriverSchedules.Select(drSch => drSch.WorkingDays).FirstOrDefault() : "",
                VehicalTagsCounter = a.Tags.Where(tg => !aTagVehList.Any(tv => tv.TagID == tg.TagID)).Count() > 0 ? 2 : 0,
                    DriverinVactionColor = a.DriverOutDateTimes.Any(ve => (ve.FromDateTime.Value >= startDate && ve.FromDateTime.Value <= endDate) ||
                    (ve.ToDateTime.Value >= startDate && ve.ToDateTime.Value <= endDate)) ? 3 : 0,
                    InTimeColorCode = a.DriverTrips.Any(dt => parCurrentStopDBId > 0 ? !a.DriverTrips.Any(tr => tr.TripID == parCurrentStopDBId) : true &&
                    ((dt.Trip.StartDate.Value <= startDate && dt.Trip.EndDate >= startDate) ||
                            (dt.Trip.StartDate.Value <= endDate && dt.Trip.EndDate >= endDate))) ? 4 : 0,
                    DriverID = a.DriverID,
                    FullName = a.FullName,
                    PersonalNumber = a.PersonalNumber,

                }).OrderBy(a => a.VehicalTagsCounter + a.DriverinVactionColor + a.InTimeColorCode).ToList();//.Skip(pageSize * (page - 1)).Take(pageSize).ToList();

                list.ForEach(a=>a.IsDriverWorking = (daysOfDateRange.Any(d=> a.WorkingDaysList !=null && a.WorkingDaysList.Contains(d))   ));
                var returnlist  = list.Where(a => a.IsDriverWorking).ToList();
                count = returnlist.Count();
                return returnlist;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DriverDashboardInfoModel GetDriverDashboard(int? parDriverId, int? parClientid, DateTime? parStartDateTime, DateTime? parEndDateTime)
        {
            DriverDashboardInfoModel driverDashboardInfoModel = new DriverDashboardInfoModel();
            driverDashboardInfoModel.DriversInfoList = new List<DriverDashboardModel>();
            try
            {

                //driverSchedule for all
                var driverScheduleQuery = (!parDriverId.HasValue || parDriverId.Value <= 0) ?
                        (from dr in db.Drivers
                         join dsch in db.DriverSchedules on dr.DriverID equals dsch.DriverID
                         select new DriverScheduleModel
                         {
                             DriverName = dr.FullName,
                             DriverID = dr.DriverID, // or pc.ProdId
                             ID = dsch.ID,
                             WorkingDays = dsch.WorkingDays,
                             WorkingHourFrom = dsch.WorkingHourFrom,
                             WorkingHourTo = dsch.WorkingHourTo,
                             PersonalNumber = dr.PersonalNumber,
                             DriverDate = dr.JoiningDate == null ? "" : dr.JoiningDate.ToString()
                         }) : (from dr in db.Drivers
                               join dsch in db.DriverSchedules on dr.DriverID equals dsch.DriverID
                               where dr.DriverID == parDriverId
                               select new DriverScheduleModel
                               {
                                   DriverName = dr.FullName,
                                   DriverID = dr.DriverID, // or pc.ProdId
                                   ID = dsch.ID,
                                   WorkingDays = dsch.WorkingDays,
                                   WorkingHourFrom = dsch.WorkingHourFrom,
                                   WorkingHourTo = dsch.WorkingHourTo,
                                   PersonalNumber = dr.PersonalNumber,
                                   DriverDate = dr.JoiningDate == null ? "" : dr.JoiningDate.ToString()
                               });
                //OutDateTimes for all
                var driverOutTimeQuery = db.DriverOutDateTimes.AsQueryable();


                // get Driver Schedules List
                if (parDriverId.HasValue && parDriverId.Value > 0)
                    driverScheduleQuery = driverScheduleQuery.Where(dsch => dsch.DriverID == parDriverId);

                // get Driver Schedules List
                if (parDriverId.HasValue && parDriverId.Value > 0)
                    driverOutTimeQuery = driverOutTimeQuery.Where(dsch => dsch.DriverID == parDriverId);


                IQueryable<Trip> tripList = db.Trips.Include("DriverTrips").AsQueryable();
                // Driver Sch All Driver
                foreach (var itemDriverSch in driverScheduleQuery.ToList())
                {
                    //List<Trip> tripList = tripListorg;

                    DriverDashboardModel driverInfo = new DriverDashboardModel();
                    driverInfo.DriverID = itemDriverSch.DriverID;
                    driverInfo.FullName = itemDriverSch.DriverName;
                    driverInfo.PersonalNumber = itemDriverSch.PersonalNumber;


                    IEnumerable<DriverOutDateTime> outDateTimes = driverOutTimeQuery.Where(t => t.DriverID == itemDriverSch.DriverID);
                    if (parClientid > 0)//error need to fix
                        if (parStartDateTime.HasValue)
                            outDateTimes = outDateTimes.Where(t => t.FromDateTime >= parStartDateTime);
                    if (parEndDateTime.HasValue)
                        outDateTimes = outDateTimes.Where(t => t.ToDateTime <= parEndDateTime);

                    string[] workingdays = itemDriverSch.WorkingDays.Split(',');
                    DateTime aCurrentDateTime = parStartDateTime.Value;
                    int aDaysCounter = 0;
                    while (aCurrentDateTime < parEndDateTime.Value)
                    {
                        if (workingdays.Contains(aCurrentDateTime.DayOfWeek.GetHashCode().ToString()))
                            aDaysCounter++;
                        aCurrentDateTime = aCurrentDateTime.AddDays(1);
                    }
                    System.TimeSpan hourPeriod = itemDriverSch.WorkingHourTo.Value - itemDriverSch.WorkingHourFrom.Value;
                    driverInfo.TotalHourTripSch = aDaysCounter * hourPeriod.TotalHours;
                    var _tripList = tripList.Where(t => t.DriverTrips.Any(dt => dt.DriverID == itemDriverSch.DriverID));
                    if (parClientid > 0)
                        _tripList = _tripList.Where(t => t.BookSchedule.ClientID == parClientid);
                    if (parStartDateTime.HasValue)
                        _tripList = _tripList.Where(t => t.StartDate >= parStartDateTime);
                    if (parEndDateTime.HasValue)
                        _tripList = _tripList.Where(t => t.EndDate <= parEndDateTime);
                    // Get Trip List
                    driverInfo.TripCount = _tripList.Count();// Trip Count


                    IEnumerable<Trip> currentPeriodTrip = _tripList.ToList().Where(tls => (itemDriverSch.WorkingHourFrom.HasValue && tls.StartDate.HasValue &&
                    tls.StartDate.HasValue) &&
                    tls.StartDate.Value.TimeOfDay >= itemDriverSch.WorkingHourFrom.Value &&
                    tls.EndDate.TimeOfDay <= itemDriverSch.WorkingHourTo.Value).ToList();
                    if (currentPeriodTrip != null && currentPeriodTrip.Count() > 0)
                        driverInfo.TotalHourtripAct += Math.Round(currentPeriodTrip.Sum(x => (x.EndDate.TimeOfDay.Subtract(x.StartDate.Value.TimeOfDay).TotalHours)), 2);

                    if (outDateTimes.Count() > 0)
                        driverInfo.TotalOutTime = outDateTimes.Sum(t => (t.ToDateTime.Value.Subtract(t.FromDateTime.Value).TotalHours));
                    //
                    if (driverInfo.TotalHourTripSch > 0)
                    {
                        driverDashboardInfoModel.DriverUtilization += Math.Round((100 * (driverInfo.TotalHourtripAct + driverInfo.TotalOutTime)) / driverInfo.TotalHourTripSch, 1, MidpointRounding.ToEven);
                    }
                    driverInfo.FreeTime = driverInfo.TotalHourTripSch - driverInfo.TotalHourtripAct - driverInfo.TotalOutTime;
                    driverDashboardInfoModel.DriversInfoList.Add(driverInfo);
                }
                driverDashboardInfoModel.DriverUtilization = driverDashboardInfoModel.DriverUtilization / driverScheduleQuery.Count();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //ex.Message
            }
            return driverDashboardInfoModel;
        }

        public void AddDriver(DriverModel model)
        {
            try
            {
                var driver = new Driver();
                driver.FullName = model.FullName;
                driver.PersonalNumber = model.PersonalNumber;
                driver.JoiningDate = model.JoiningDate;
                driver.Nationality = model.Nationality;
                driver.EmaritesIdExpairy = model.EmaritesIdExpairy;
                driver.PassportNumber = model.PassportNumber;
                driver.MedicalStatus = model.MedicalStatus;
                driver.Address = model.Address;
                driver.DriverEmaritesID = model.DriverEmaritesID;
                driver.ReferenceName = model.ReferenceName;
                driver.Status = model.Status;

                db.Drivers.Add(driver);
                db.SaveChanges(model.ByUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateDriver(DriverModel model)
        {
            var driver = db.Drivers.FirstOrDefault(a => a.DriverID == model.DriverID);
            if (driver != null)
            {
                driver.FullName = model.FullName;
                driver.PersonalNumber = model.PersonalNumber;
                driver.JoiningDate = model.JoiningDate;
                driver.Nationality = model.Nationality;
                driver.EmaritesIdExpairy = model.EmaritesIdExpairy;
                driver.PassportNumber = model.PassportNumber;
                driver.MedicalStatus = model.MedicalStatus;
                driver.Address = model.Address;
                driver.DriverEmaritesID = model.DriverEmaritesID;
                driver.ReferenceName = model.ReferenceName;
                driver.Status = model.Status;

                var entry = db.Entry(driver);
                entry.State = EntityState.Modified;
                db.SaveChanges(model.ByUserID);
            }
        }


        private DriverModel prepareModel(Driver driver)
        {
            DriverModel driverModel = new DriverModel();
            driverModel.DriverID = driver.DriverID;
            driverModel.FullName = driver.FullName;
            driverModel.PersonalNumber = driver.PersonalNumber;
            driverModel.JoiningDate = driver.JoiningDate;
            driverModel.Nationality = driver.Nationality;
            driverModel.EmaritesIdExpairy = driver.EmaritesIdExpairy;
            driverModel.PassportNumber = driver.PassportNumber;
            driverModel.MedicalStatus = driver.MedicalStatus;
            driverModel.Address = driver.Address;
            driverModel.DriverEmaritesID = driver.DriverEmaritesID;
            driverModel.ReferenceName = driver.ReferenceName;
            driverModel.Status = driver.Status;
            driverModel.VehicleAssign = driver.VehicleAssignDrivers.Select(x => new VehiclesModel()
            {
                ID = x.VehicleID,
                VehicleNum = x.Vehicle.VehicleNum,
            });
            driverModel.Tags = driver.Tags.Select(x => new TagModel()
            {
                TagID = x.TagID,
                Name = x.Name
            });

            return driverModel;
        }
        public bool DeleteDriver(int driverId, int byUserId)
        {
            try
            {
                var driver = db.Drivers.FirstOrDefault(a => a.DriverID == driverId);
                db.Entry(driver).State = EntityState.Deleted;
                db.SaveChanges(byUserId);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

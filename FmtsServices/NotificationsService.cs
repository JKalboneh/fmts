﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class NotificationsService
    {
        private BMOSContext db = new BMOSContext();

        public NotificationsModel GetNotifications()
        {
            try
            {
                NotificationsModel aNotificationsModel = new NotificationsModel();
                var query = db.AuditLogs.AsQueryable();//.Include("VehicleAssignDrivers").Include("Tags").AsQueryable();
                var list = query.Select(a => new NotificationsItemModel()
                {
                    ColumnName = a.ColumnName,
                    RecordId = a.RecordId,
                    EventType = a.EventType,
                    OriginalValue = a.OriginalValue,
                    NewValue = a.NewValue,
                    ByUserID = a.UserId,
                    UserName = a.UserId.ToString()
                }).OrderByDescending(a => a.RecordId);//.Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);
                aNotificationsModel.counter= query.Count();
                aNotificationsModel.items = list.ToList();

                return aNotificationsModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public NotificationsModel FindNotifications(NotificationsFilterModel model)
        {
            try
            {
                NotificationsModel aNotificationsModel = new NotificationsModel();
                var query = db.AuditLogs.AsQueryable();//.Include("VehicleAssignDrivers").Include("Tags").AsQueryable();

                if(model.NewValue!=null)
                    query = query.Where(q => q.NewValue.Contains(model.NewValue)).OrderByDescending(a => a.RecordId).Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);
                else
                    query = query.OrderByDescending(a => a.RecordId).Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);

                var list = query.Select(a => new NotificationsItemModel()
                {
                    EventDateUTC = a.EventDateUTC.ToString(),
                    TableName = a.TableName,
                    ColumnName = a.ColumnName,
                    RecordId = a.RecordId,
                    EventType = a.EventType,
                    OriginalValue = a.OriginalValue,
                    NewValue = a.NewValue,
                    ByUserID = a.UserId,
                    UserName = a.User.UserName
                }).OrderByDescending(a => a.RecordId);//.Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);
               // aNotificationsModel.counter = query.Count();
                aNotificationsModel.items = list.ToList();

                return aNotificationsModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

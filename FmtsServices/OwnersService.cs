﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class OwnersService
    {
        private BMOSContext db = new BMOSContext();
        
        public List<OwnerModel> FindOwners(string filter, out int count, int page = 1, int pageSize = 15)
        {
            try
            {
                var query = db.Owners.AsQueryable();
                if (!String.IsNullOrEmpty(filter))
                        query = query.Where(a => a.OwnerName.Contains(filter));
                count = query.Count();
                var list = query.Select(a => new OwnerModel()
                {
                    ID = a.ID,
                    OwnerName = a.OwnerName,
                    Type  = a.Type,
                }).OrderBy(a => a.OwnerName).Skip(pageSize * (page - 1)).Take(pageSize).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
}



}

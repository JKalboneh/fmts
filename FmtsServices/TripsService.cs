﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class TripsService
    {
        private BMOSContext db = new BMOSContext();
        public int GetUnassignedTripsCount(FindUnassignedBookFilterModel model)
        {
            try
            {
                var query = db.BookSchedules.AsQueryable();
                DateTime aStartDate = DateTime.ParseExact(model.paraDateTime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                DateTime aToStart = DateTime.ParseExact(model.paraToTime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                query = query.Where(bks => bks.Status == "Active" &&
                    bks.StartDatetime >= aStartDate && bks.TillDate <= aToStart &&
                    bks.Trips.Any(t => !t.VehicleID.HasValue || !t.DriverTrips.Any(dt => dt.DriverID > 0)));
                if (model.paraClientId != null)
                    query.Where(bks => model.paraClientId.id == bks.ClientID);
                if (model.paraDriverId != null)
                    query.Where(bks => bks.Trips.Any(tr => tr.DriverTrips.Any(drt => drt.DriverID == model.paraDriverId.id)));
                if (model.paraVehicaleId != null)
                    query.Where(bks => bks.Trips.Any(tr => tr.VehicleID == model.paraVehicaleId.id));


                var count = query.Count();
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BookDashBoardModel> GetUnassignedBooks(FindUnassignedBookFilterModel model, out int count)
        {
            try
            {
                var query = db.BookSchedules.AsQueryable();
               DateTime aStartDate = DateTime.ParseExact(model.paraDateTime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
               DateTime aToStart = DateTime.ParseExact(model.paraToTime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                query = query.Where(bks => bks.Status == "Active" &&
                    bks.StartDatetime >= aStartDate && bks.TillDate <= aToStart &&
                    bks.Trips.Any(t => !t.VehicleID.HasValue || !t.DriverTrips.Any(dt => dt.DriverID > 0)));
                if(model.paraClientId != null)
                    query.Where(bks => model.paraClientId.id == bks.ClientID);
                if (model.paraDriverId != null)
                    query.Where(bks => bks.Trips.Any(tr => tr.DriverTrips.Any(drt => drt.DriverID == model.paraDriverId.id)));
                if (model.paraVehicaleId != null)
                    query.Where(bks => bks.Trips.Any(tr => tr.VehicleID == model.paraVehicaleId.id));

                    var list = query.Select(a => new BookDashBoardModel()
                {
                        BookScheduleID = a.ID,
                        BookID = a.BookID.Value,
                        BookRef = a.Book.BookRef,
                        ClientName = a.Client.ClientName,
                        StartDatetime = a.StartDatetime.Value.ToString(),
                        EndDate = a.TillDate.Value.ToString(),
                        EndLocation = a.Location.Name
                    }).OrderByDescending(a => a.BookRef).Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);
                //              var count = db.BookSchedules.Where(bks => bks.StartDatetime >= paraDateTime.Value && bks.TillDate <= paraToTime.Value
                //            && (!paraClientId.HasValue || paraClientId.Value== bks.ClientID)
                //     && (!paraDriverId.HasValue || paraDriverId.Value == bks.Trips.Any(tr=>tr.DriverTrips))) ;
                count = query.Count();
                return list.ToList();

                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
    }



}

﻿using FmtsEntity;
using FmtsServices.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FmtsServices
{
    public class VehiclesService
    {
        private BMOSContext db = new BMOSContext();
        public List<VehiclesModel> FindVehicles(string filter, 
            out int count, 
            int page = 1, 
            int pageSize = 15,
            string parTagsList="", 
            int parDriverID = 0, 
            string parStatDate = "", 
            string parEndDate = "", 
            int parCurrentStopDBId = 0)
        {
            try
            {
                DateTime startDate = new DateTime();
                if (parStatDate != "")
                    startDate = DateTime.ParseExact(parStatDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

                DateTime endDate = new DateTime();
                if (parEndDate != "")
                    endDate = DateTime.ParseExact(parEndDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

                List<string> tageList = new List<string>();
                if (parTagsList!="")
                    tageList = parTagsList.Split(',').ToList();
                var query = db.Vehicles.Include("Tags").AsQueryable();

                List<Tag> aTagDriverList = new List<Tag>();
                if (parDriverID > 0)
                {
                    var quryDriverTags = db.Tags.AsQueryable().Where(tg => tg.Drivers.Any(dr => dr.DriverID == parDriverID));
                    aTagDriverList = quryDriverTags.ToList();
                }

                if (!String.IsNullOrEmpty(filter))
                {
                    if(tageList.Count>0)
                        query = query.Where(a => (a.VehicleNum.Contains(filter) || a.Make.Contains(filter)) && a.Tags.Any(t=>t.TagID>0 && tageList.Contains(t.TagID.ToString())));
                    else
                        query = query.Where(a => a.VehicleNum.Contains(filter) || a.Make.Contains(filter));
                }else
                {
                    if (tageList.Count > 0)
                        query = query.Where(a => a.Tags.Any(t => t.TagID > 0 && tageList.Contains(t.TagID.ToString())));
                }

                query = query.Where(v => v.VehicleSchedules.Any(vSch => (startDate.TimeOfDay >= vSch.WorkingHourForm && startDate.TimeOfDay <= vSch.WorkingHourTo) &&
                 (endDate.TimeOfDay <= vSch.WorkingHourTo && endDate.TimeOfDay >= vSch.WorkingHourForm)));

                List<int> daysOfDateRange = new List<int>();
                DateTime acurrentdate = startDate;
                while (acurrentdate <= endDate)
                {
                    if (!daysOfDateRange.Contains(acurrentdate.DayOfWeek.GetHashCode()))
                        daysOfDateRange.Add(acurrentdate.DayOfWeek.GetHashCode());
                    acurrentdate = acurrentdate.AddDays(1);
                }

                count = query.Count();
                var list = query.Select(a => new VehiclesModel()
                {
                    WorkingDays = a.VehicleSchedules.FirstOrDefault() != null ? a.VehicleSchedules.Select(vSch => vSch.WorkingDays).FirstOrDefault() : "",
                    VehicalOutTime = a.VehicleOutDateTimes.Any(ve => (ve.FromDate.Value >= startDate && ve.FromDate.Value <= endDate) ||
                                        (ve.ToDate.Value >= startDate && ve.ToDate.Value <= endDate)) ? 3 : 0,
                    DriverTagsMissmatch = a.Tags.Where(tg => !aTagDriverList.Any(tv => tv.TagID == tg.TagID)).Count() > 0 ? 2 : 0,
                    BusyInSameTrip = a.Trips.Any(t => (parCurrentStopDBId > 0 ? t.TripID != parCurrentStopDBId : true) &&
                    ((t.StartDate.Value <= startDate && t.EndDate >= startDate) ||
                            (t.StartDate.Value <= endDate && t.EndDate >= endDate))) ? 4 : 0,
                    ID = a.ID,
                    VehicleNum = a.VehicleNum,
                    Make = a.Make,
                    SeatCount = a.SeatCount
                }).OrderBy(a => a.VehicalOutTime + a.DriverTagsMissmatch+ a.BusyInSameTrip).ToList();//.Skip(pageSize * (page - 1)).Take(pageSize).ToList();

                list.ForEach(a => a.IsVehicalrWorking = (daysOfDateRange.Any(d => a.WorkingDaysList != null && a.WorkingDaysList.Contains(d))));
                var returnlist = list.Where(a => a.IsVehicalrWorking).ToList();
                count = returnlist.Count();
                return returnlist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<VehiclesModel> FindVehicles(VehicleFilterModel model, out int count)
        {
            try
            {
                var query = db.Vehicles.Include("VehicleAssignDrivers").Include("VehicleOwner").AsQueryable();

                query = FilterByGridModel(query, model);
                count = query.Count();
                var list = query.Select(prepareModel).OrderByDescending(a => a.ID)
                            .Skip(model.pageSize * (model.pageIndex - 1)).Take(model.pageSize);
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public VehiclesModel FindByID(int vehiclerId)
        {
            try
            {
                var query = db.Vehicles.Where(a => a.ID == vehiclerId).AsQueryable();

                var item = query.Select(prepareModel).FirstOrDefault();

                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private IQueryable<Vehicle> FilterByGridModel(IQueryable<Vehicle> query, VehicleFilterModel model)
        {
            if (!String.IsNullOrEmpty(model.VehicleNum))
            {
                query = query.Where(a => a.VehicleNum.Contains(model.VehicleNum));
            }
            if (model.SeatCount.HasValue)
            {
                query = query.Where(a => a.SeatCount == model.SeatCount);
            }
            if (!String.IsNullOrEmpty(model.VehicleColor))
            {
                query = query.Where(a => a.VehicleColor.Contains(model.VehicleColor));
            }
            if (!String.IsNullOrEmpty(model.VehicleStatus))
            {
                query = query.Where(a => a.VehicleStatus.Contains(model.VehicleStatus));
            }
            if (!String.IsNullOrEmpty(model.MakeYear))
            {
                query = query.Where(a => a.MakeYear.Contains(model.MakeYear));
            }
            if (!String.IsNullOrEmpty(model.ChasisNumber))
            {
                query = query.Where(a => a.ChasisNumber.Contains(model.ChasisNumber));
            }
            if (!String.IsNullOrEmpty(model.Make))
            {
                query = query.Where(a => a.Make.Contains(model.Make));
            }
            if (!String.IsNullOrEmpty(model.OwnerName))
            {
                query = query.Where(a => a.VehicleOwner.OwnerName.Contains(model.OwnerName));
            }

            return query;
        }

        

        public VehicleDashboardInfoModel GetVehicleDashboard(int? parDriverId, int? parClientid, int? parVehiclesId, DateTime? parStartDateTime, DateTime? parEndDateTime)
        {
            try
            {
                IQueryable<VehicleScheduleModel> VehiclesScheduleList = (parVehiclesId.HasValue && parVehiclesId.Value>0)?
                   (from veh in db.Vehicles
                   join vsch in db.VehicleSchedules on veh.ID equals vsch.VehicleID
                   where parVehiclesId == vsch.VehicleID
                    select new VehicleScheduleModel
                   {
                       VehicleNum = veh.VehicleNum,
                       ID = vsch.ID,
                       VehiclesID = vsch.VehicleID,
                       WorkingDays = vsch.WorkingDays,
                       WorkingHourFrom = vsch.WorkingHourForm,
                       WorkingHourTo = vsch.WorkingHourTo
                   }): (from veh in db.Vehicles
                        join vsch in db.VehicleSchedules on veh.ID equals vsch.VehicleID
                        select new VehicleScheduleModel
                        {
                            VehicleNum = veh.VehicleNum,
                            ID = vsch.ID,
                            VehiclesID = vsch.VehicleID,
                            WorkingDays = vsch.WorkingDays,
                            WorkingHourFrom = vsch.WorkingHourForm,
                            WorkingHourTo = vsch.WorkingHourTo
                        });

                VehicleDashboardInfoModel vehicleDashboardInfoModel = new VehicleDashboardInfoModel();

                IQueryable<Trip> tripList = db.Trips;
                IQueryable<VehicleOutDateTime> outDateTimesList = db.VehicleOutDateTimes;
                // Vehicles Sch All Vehicles
                foreach (var itemVehiclesSch in VehiclesScheduleList.ToList())
                {
                    VehicleDashboardModel vehiclesInfo = new VehicleDashboardModel();
                    vehiclesInfo.ID = itemVehiclesSch.VehiclesID;
                    vehiclesInfo.VehicleNum = itemVehiclesSch.VehicleNum;

                    // get the Off time date 

                    IQueryable<VehicleOutDateTime> outDateTimesItem = outDateTimesList.Where(t => t.VehicleID == itemVehiclesSch.VehiclesID);
                    if (parClientid > 0)
                        if (parStartDateTime.HasValue)
                            outDateTimesItem.Where(t => t.FromDate >= parStartDateTime);
                    if (parEndDateTime.HasValue)
                        outDateTimesItem.Where(t => t.ToDate <= parEndDateTime);

                    string[] workingdays = itemVehiclesSch.WorkingDays.Split(',');
                    tripList.Where(t => t.Vehicle.ID == itemVehiclesSch.VehiclesID);
                    if (parClientid > 0)
                        tripList.Where(t => t.BookSchedule.ClientID == parClientid);
                    if (parStartDateTime.HasValue)
                        tripList.Where(t => t.StartDate >= parStartDateTime);
                    if (parEndDateTime.HasValue)
                        tripList.Where(t => t.EndDate <= parEndDateTime);

                    // Get Trip List
                    vehiclesInfo.TripCount = tripList.Count();// Trip Count
                    DateTime aCurrentDateTime = parStartDateTime.Value;
                    int aDaysCounter = 0;
                    System.TimeSpan hourPeriod = itemVehiclesSch.WorkingHourTo.Value - itemVehiclesSch.WorkingHourFrom.Value;
                    // Start from start Date to End Date
                    while (aCurrentDateTime < parEndDateTime.Value)
                    {
                        if (workingdays.Contains(aCurrentDateTime.DayOfWeek.GetHashCode().ToString()))
                            aDaysCounter++;
                        aCurrentDateTime = aCurrentDateTime.AddDays(1);
                    }
                    var _tripList = tripList.Where(t => t.VehicleID == itemVehiclesSch.VehiclesID);
                    if (parClientid > 0)
                        _tripList = _tripList.Where(t => t.BookSchedule.ClientID == parClientid);
                    if (parStartDateTime.HasValue)
                        _tripList = _tripList.Where(t => t.StartDate >= parStartDateTime);
                    if (parEndDateTime.HasValue)
                        _tripList = _tripList.Where(t => t.EndDate <= parEndDateTime);
                    // Get Trip List
                    vehiclesInfo.TripCount = _tripList.Count();// Trip Count

                    vehiclesInfo.TotalHourTripSch = aDaysCounter * hourPeriod.TotalHours;
                    IEnumerable<Trip> currentPeriodTrip = _tripList.ToList().Where(tls => (itemVehiclesSch.WorkingHourFrom.HasValue && tls.StartDate.HasValue &&
                                        tls.StartDate.HasValue) &&
                                        tls.StartDate.Value.TimeOfDay >= itemVehiclesSch.WorkingHourFrom.Value &&
                                        tls.EndDate.TimeOfDay <= itemVehiclesSch.WorkingHourTo.Value).ToList();
                    if (currentPeriodTrip != null && currentPeriodTrip.Count() > 0)
                        vehiclesInfo.TotalHourTripAct+= Math.Round(currentPeriodTrip.Sum(x => (x.EndDate.TimeOfDay.Subtract(x.StartDate.Value.TimeOfDay).TotalHours)), 2);

                    if (outDateTimesItem.Count() > 0)
                        vehiclesInfo.TotalOutTime = outDateTimesItem.Sum(t => (t.ToDate.Value.Subtract(t.FromDate.Value).TotalHours));
                    //
                    if (vehiclesInfo.TotalHourTripSch > 0)
                    {
                        vehicleDashboardInfoModel.VehicleUtilization+= Math.Round((100 * (vehiclesInfo.TotalHourTripAct + vehiclesInfo.TotalOutTime)) / vehiclesInfo.TotalHourTripSch, 1, MidpointRounding.ToEven);
                    }
                    vehiclesInfo.FreeTime = vehiclesInfo.TotalHourTripSch - vehiclesInfo.TotalHourTripAct- vehiclesInfo.TotalOutTime;
                    vehicleDashboardInfoModel.VehiclesInfoList.Add(vehiclesInfo);
                }

                return vehicleDashboardInfoModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void AddVehicle(VehiclesModel model)
        {
            try
            {
                var vehicle = new Vehicle();
                
                vehicle.VehicleNum = model.VehicleNum;
                vehicle.SeatCount = model.SeatCount;
                vehicle.VehicleColor = model.VehicleColor;
                vehicle.VehicleStatus = model.VehicleStatus;
                vehicle.MakeYear = model.MakeYear;
                vehicle.ChasisNumber = model.ChasisNumber;
                vehicle.Make = model.Make;
                vehicle.OwnerID = model.OwnerID;
                vehicle.Type = model.Type;
                vehicle.CreatedBy = model.CreatedBy;
                vehicle.VehicleModel = model.VehicleModel;
                db.Vehicles.Add(vehicle);
                db.SaveChanges(model.ByUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateVehicle(VehiclesModel model)
        {
            try
            {
                var vehicle = db.Vehicles.Where(a => a.ID == model.ID).FirstOrDefault();

                if (vehicle != null)
                {
                    vehicle.VehicleNum = model.VehicleNum;
                    vehicle.SeatCount = model.SeatCount;
                    vehicle.VehicleColor = model.VehicleColor;
                    vehicle.VehicleStatus = model.VehicleStatus;
                    vehicle.MakeYear = model.MakeYear;
                    vehicle.ChasisNumber = model.ChasisNumber;
                    vehicle.Make = model.Make;
                    vehicle.OwnerID = model.OwnerID;
                    vehicle.Type = model.Type;
                    vehicle.CreatedBy = model.CreatedBy;
                    vehicle.VehicleModel = model.VehicleModel;
                    var entry = db.Entry(vehicle);
                    entry.State = EntityState.Modified;
                    db.SaveChanges(model.ByUserID);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteVehicle(int vehicleId, int byUserId)
        {
            try
            {
                var vehicle = db.Vehicles.FirstOrDefault(a => a.ID == vehicleId);
                db.Entry(vehicle).State = EntityState.Deleted;
                db.SaveChanges(byUserId);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private VehiclesModel prepareModel(Vehicle vehicle)
        {
            VehiclesModel vehicleModel = new VehiclesModel();

            try
            {
                vehicleModel.ID = vehicle.ID;
                vehicleModel.VehicleNum = vehicle.VehicleNum;
                vehicleModel.SeatCount = vehicle.SeatCount;
                vehicleModel.VehicleColor = vehicle.VehicleColor;
                vehicleModel.VehicleStatus = vehicle.VehicleStatus;
                vehicleModel.MakeYear = vehicle.MakeYear;
                vehicleModel.ChasisNumber = vehicle.ChasisNumber;
                vehicleModel.Make = vehicle.Make;
                vehicleModel.OwnerID = vehicle.OwnerID.HasValue?vehicle.OwnerID.Value:0;
                vehicleModel.Type = vehicle.Type;
                vehicleModel.VehicleModel = vehicle.VehicleModel;
                vehicleModel.DriverAssign = vehicle.VehicleAssignDrivers.Select(x => new DriverModel()
                {
                    DriverID = x.DriverID,
                    FullName = x.Driver.FullName
                });
                vehicleModel.OwnerName = vehicle.VehicleOwner != null ? vehicle.VehicleOwner.OwnerName : "";
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return vehicleModel;
        }
    }



}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.ComponentModel.DataAnnotations;

namespace FmtsEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class BookSchedule
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BookSchedule()
        {
            this.Trips = new HashSet<Trip>();
        }
    
           [Key()]
           public int ID { get; set; }
         public Nullable<int> BookID { get; set; }
         public int ClientID { get; set; }
         public int CreatedBy { get; set; }
         public Nullable<System.DateTime> StartDatetime { get; set; }
         public Nullable<int> StartLoc { get; set; }
         public Nullable<bool> RecType { get; set; }
         public Nullable<System.DateTime> TillDate { get; set; }
         public string Refnum { get; set; }
         public Nullable<int> CustomerID { get; set; }
         public Nullable<int> DepartmentID { get; set; }
         public string ContactType { get; set; }
         public decimal TotalCost { get; set; }
         public Nullable<bool> Sat { get; set; }
         public Nullable<bool> Sun { get; set; }
         public Nullable<bool> Mon { get; set; }
         public Nullable<bool> Tue { get; set; }
         public Nullable<bool> Wed { get; set; }
         public Nullable<bool> Thu { get; set; }
         public Nullable<bool> Fri { get; set; }
         public string Status { get; set; }
    
        public virtual Book Book { get; set; }
        public virtual Client Client { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Department Department { get; set; }
        public virtual Location Location { get; set; }
        public virtual User User { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Trip> Trips { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.ComponentModel.DataAnnotations;

namespace FmtsEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class RouteTrip
    {
           [Key()]
           public int RouteTripID { get; set; }
         public int RouteID { get; set; }
         public int TripID { get; set; }
    
        public virtual Route Route { get; set; }
        public virtual Trip Trip { get; set; }
    }
}

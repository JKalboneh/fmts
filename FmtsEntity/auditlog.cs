//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.ComponentModel.DataAnnotations;

namespace FmtsEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class AuditLog
    {
           [Key()]
           public System.Guid AuditLogId { get; set; }
         public int UserId { get; set; }
         public System.DateTime EventDateUTC { get; set; }
         public string EventType { get; set; }
         public string TableName { get; set; }
         public string RecordId { get; set; }
         public string ColumnName { get; set; }
         public string OriginalValue { get; set; }
         public string NewValue { get; set; }
    
        public virtual User User { get; set; }
    }
}
